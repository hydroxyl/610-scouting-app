function gears_attempted_auton_inc(x) {
	var curVal = parseInt(document.getElementById("gears_attempted_auton").value);
	var success = parseInt(document.getElementById("gears_success_auton").value);
	curVal += x;
	if (curVal > 5) {
		curVal = 5;
	}
	else if(curVal < 0) {
		curVal = 0;
	}

	if(success > curVal){
		gears_success_auton_inc(curVal - success);
	}

	document.getElementById("gears_attempted_auton").value = curVal;
}

function gears_success_auton_inc(x) {
	var curVal = parseInt(document.getElementById("gears_success_auton").value);
	var attempted = parseInt(document.getElementById("gears_attempted_auton").value);
	curVal += x;
	if (curVal > 5) {
		curVal = 5;
	}
	else if(curVal < 0) {
		curVal = 0;
	}

	if(attempted <= curVal) {
		gears_attempted_auton_inc(curVal - attempted);
	}

	document.getElementById("gears_success_auton").value = curVal;
}

function high_goal_shots_auton_inc(x) {
	var curVal = parseInt(document.getElementById("high_goal_shots_auton").value);
	curVal += x;
	if (curVal > 200) {
		curVal = 200;
	}
	else if(curVal < 0) {
		curVal = 0;
	}

	document.getElementById("high_goal_shots_auton").value = curVal;
}

function high_goal_shots_teleop_inc(x) {
	var curVal = parseInt(document.getElementById("high_goal_shots_teleop").value);
	curVal += x;
	if (curVal > 200) {
		curVal = 200;
	}
	else if(curVal < 0) {
		curVal = 0;
	}
	document.getElementById("high_goal_shots_teleop").value = curVal;
}

function gears_attempted_teleop_inc(x) {
	var curVal = parseInt(document.getElementById("gears_attempted_teleop").value);
	var success = parseInt(document.getElementById("gears_success_teleop").value);
	curVal += x;
	if (curVal > 30) {
		curVal = 30;
	}
	else if(curVal < 0) {
		curVal = 0;
	}

	if(success > curVal){
		gears_success_teleop_inc(curVal - success);
	}

	document.getElementById("gears_attempted_teleop").value = curVal;
}

function gears_success_teleop_inc(x) {
	var curVal = parseInt(document.getElementById("gears_success_teleop").value);
	var attempted = parseInt(document.getElementById("gears_attempted_teleop").value);
	curVal += x;
	if (curVal > 30) {
		curVal = 30;
	}
	else if(curVal < 0) {
		curVal = 0;
	}

	if(attempted <= curVal) {
		gears_attempted_teleop_inc(curVal - attempted);
	}

	document.getElementById("gears_success_teleop").value = curVal;
}
