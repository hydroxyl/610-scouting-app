function drawScaleCharts() {
    var data = new google.visualization.arrayToDataTable([
        ['Time', 'Count'],
        ['0', 0]
    ]);
//     <?php
// console.log($analytics['scale_per_match'])
//      ?>



    var options = {
        isStacked: true,
        legend: {
            position: 'none'
        },
        chartArea: {
            width: "85%",
            height: "70%"
        },
        hAxis: {
            title: 'Cycle Time (s)'
        },
        vAxis: {
            title: 'Count'
        }
    };

    var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_scale_1'));

    chart.draw(data, options);


    ////////////////////////////////


    data = new google.visualization.arrayToDataTable([
        ['Match Number', 'Average Cycle Time'],
        <?php $info = json_decode($analytics['scale_per_match'], true); ?>
        @for($i=1; $i<count($info); $i++)
            ['{{$info[$i]["matchNum"]}}', {{$info[$i]['hits']}}],
        @endfor
    ]);

    options = {
        width: document.getElementById('chart_scale_1').offsetWidth,
        height: document.getElementById('chart_scale_1').offsetHeight,
        legend: {
            position: 'none'
        },
        chartArea: {
            width: "85%",
            height: "70%"
        },
        hAxis: {
            title: 'Match Number'
        },
        vAxis: {
            title: 'Average Cycle Time (s)'
        }
    };

    chart = new google.visualization.ColumnChart(
        document.getElementById('chart_scale_2'));

    chart.draw(data, options);
}

function drawSwitchCharts() {
    var data = new google.visualization.arrayToDataTable([
        ['Time', 'Home Count', 'Away Count'],
        ['0', 0, null],
        [1, 0, null]
    ]);

    var options = {
        isStacked: true,
        // legend: {
        //     position: 'none'
        // },
        chartArea: {
            // width: "70%",
            height: "70%"
        },
        hAxis: {
            title: 'Cycle Time (s)'
        },
        vAxis: {
            title: 'Count'
        }
    };

    var chart = new google.visualization.ColumnChart(
      document.getElementById('chart_switch'));

    chart.draw(data, options);
}

function drawExchangeCharts() {
    var data = new google.visualization.arrayToDataTable([
        ['Time', 'Count'],
        ['0', 0]
    ]);

    var options = {
        isStacked: true,
        legend: {
            position: 'none'
        },
        chartArea: {
            width: "90%",
            height: "70%"
        },
        hAxis: {
            title: 'Cycle Time (s)'
        },
        vAxis: {
            title: 'Count'
        }
    };

    var chart = new google.visualization.ColumnChart(
      document.getElementById('chart_exchange'));

    chart.draw(data, options);
}

function drawAutonCharts() {
    var data = new google.visualization.arrayToDataTable([
        ['Match Number', 'Switch', 'Scale'],
        ['7', 1, null],
        [12, 1, null],
        [19, 1, null]
    ]);

    var options = {
        isStacked: true,
        chartArea: {
            // width: "90%",
            height: "70%"
        },
        hAxis: {
            title: 'Match Number'
        },
        vAxis: {
            title: 'Cubes Scored'
        }
    };

    var chart = new google.visualization.ColumnChart(
      document.getElementById('chart_auton'));

    chart.draw(data, options);
}

function drawHangCharts() {
    var data = new google.visualization.arrayToDataTable([
        ['Match Number', 'Centre Rung', 'Carried/Assisted'],
        ['7', 3.147, null],
        [12, 1.856, null],
        [19, null, 4.363],
        [25, 1.725, null]
    ]);

    var options = {
        isStacked: true,
        chartArea: {
            height: "70%"
        },
        hAxis: {
            title: 'Match Number',
        },
        vAxis: {
            title: 'Time (s)'
        }
    };

    var chart = new google.visualization.ColumnChart(
      document.getElementById('chart_hang'));

    chart.draw(data, options);
}
