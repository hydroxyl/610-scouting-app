var aGearsAttemptValue;
var aGearsSuccessValue;
var aHighGoalsValue;
var tGearsAttemptValue;
var tGearsSuccessValue;
var tHighGoalsValue;
var tHangValue;
var teamNumValue;
var matchNumValue;
var matchToModify;
var curMatchNum;
var pComments;


function countChars() {
	var charCount = document.getElementById("comment_box").value.length;
	if (charCount >= 999) {
		document.getElementById("char_count").style = "color:red";
	} else if (charCount >= 800) {
		document.getElementById("char_count").style = "color:#F4FA58";
	} else {
		document.getElementById("char_count").style = "color:black";
	}
	document.getElementById("char_count").innerHTML = 999-charCount;
	pComments = document.getElementById("comment_box").value;
	localStorage.setItem(matchNumValue + ".comments", pComments);
}

function loadData(matchToModify) {
	//Team Number info
	teamNumValue = localStorage.getItem(matchToModify + ".teamNum");
	document.getElementById("team_num").value = teamNumValue;

	//Number values
	aGearsAttemptValue = localStorage.getItem(matchToModify + ".aGearsAttempt");
	if (aGearsAttemptValue) {
		console.log("Loading auton gears attempted");
		document.getElementById("gears_attempted_auton").value = aGearsAttemptValue;
	} else if (aGearsAttemptValue == "0") {
		console.log("Loading auton gears attempted");
		document.getElementById("gears_attempted_auton").value = aGearsAttemptValue;
	}
	aGearsSuccessValue = localStorage.getItem(matchToModify + ".aGearsSuccess");
	if (aGearsSuccessValue) {
		console.log("Loading auton gears success");
		document.getElementById("gears_success_auton").value = aGearsSuccessValue;
	} else if (aGearsSuccessValue == "0") {
		console.log("Loading auton gears success");
		document.getElementById("gears_success_auton").value = aGearsSuccessValue;
	}
	aHighGoalsValue = localStorage.getItem(matchToModify + ".aHighGoals");
	if (aHighGoalsValue) {
		console.log("Loading auton high goals");
		document.getElementById("high_goal_shots_auton").value = aHighGoalsValue;
	} else if (aHighGoalsValue == "0") {
		console.log("Loading auton high goals");
		document.getElementById("high_goal_shots_auton").value = aHighGoalsValue;
	}
	tGearsAttemptValue = localStorage.getItem(matchToModify + ".tGearsAttempt");
	if (tGearsAttemptValue) {
		console.log("Loading teleop gears attempted");
		document.getElementById("gears_attempted_teleop").value = tGearsAttemptValue;
	} else if (tGearsAttemptValue == "0") {
		console.log("Loading teleop gears attempted");
		document.getElementById("gears_attempted_teleop").value = tGearsAttemptValue;
	}
	tGearsSuccessValue = localStorage.getItem(matchToModify + ".tGearsSuccess");
	if (tGearsSuccessValue) {
		console.log("Loading teleop gears success");
		document.getElementById("gears_success_teleop").value = tGearsSuccessValue;
	} else if (tGearsSuccessValue == "0") {
		console.log("Loading teleop gears success");
		document.getElementById("gears_success_teleop").value = tGearsSuccessValue;
	}
	tHighGoalsValue = localStorage.getItem(matchToModify + ".tHighGoals");
	if (tHighGoalsValue) {
		console.log("Loading teleop high goals");
		document.getElementById("high_goal_shots_teleop").value = tHighGoalsValue;
	} else if (tHighGoalsValue == "0") {
		console.log("Loading teleop high goals");
		document.getElementById("high_goal_shots_teleop").value = tHighGoalsValue;
	}
	tHangValue = localStorage.getItem(matchToModify + ".tHang");
	if (tHangValue) {
		console.log("Loading teleop hang");
		document.getElementById("hang_time").value = tHangValue;
	} else if (tHighGoalsValue == "0") {
		console.log("Loading teleop hang");
		document.getElementById("hang_time").value = tHangValue;
	}

	//Radio buttons
	var autonPos = document.getElementsByName("starting_pos_auton");
	var aPosValue = localStorage.getItem(matchToModify + ".aPos");
	for (var i=0; i<autonPos.length; i++) {
		if (autonPos[i].value == aPosValue) {
			autonPos[i].checked = true;
			console.log("Loading starting position");
		}
	}
	var autonLow = document.getElementsByName("low_goal_auton");
	var aLowGoalValue = localStorage.getItem(matchToModify + ".aLowGoal");
	for (var i=0; i<autonLow.length; i++) {
		if (autonLow[i].value == aLowGoalValue) {
			autonLow[i].checked = true;
			console.log("Loading low goal auton");
		}
	}
	var tHangSuccessful = document.getElementsByName("hang");
	var tHangSuccessValue = localStorage.getItem(matchToModify + ".tHangSuccess");
	for (var i=0; i<tHangSuccessful.length; i++) {
		if (tHangSuccessful[i].value == tHangSuccessValue) {
			tHangSuccessful[i].checked = true;
			console.log("Loading hang success");
		}
	}
	var tHangPosition = document.getElementsByName("hang_pos");
	var tHangPosValue = localStorage.getItem(matchToModify + "tHangPos");
	for (var i=0; i<tHangPosition.length; i++) {
		if (tHangPosition[i].value == tHangPosValue) {
			tHangPosition[i].checked = true;
			console.log("Loading hang position");
		}
	}
	var tDefenseBot = document.getElementsByName("played_defense");
	var tDefenseValue = localStorage.getItem(matchToModify + ".tDefense");
	for (var i=0; i<tDefenseBot.length; i++) {
		if (tDefenseBot[i].value == tDefenseValue) {
			tDefenseBot[i].checked = true;
			console.log("Loading defense played");
		}
	}
	var pPressureReached = document.getElementsByName("pressure");
	var pPressureValue = localStorage.getItem(matchToModify + ".pPressure");
	for (var i=0; i<pPressureReached.length; i++) {
		if (pPressureReached[i].value == pPressureValue) {
			pPressureReached[i].checked = true;
			console.log("Loading pressure");
		}
	}
	var pShootingOnly = document.getElementsByName("pressure");
	var pShootingValue = localStorage.getItem(matchToModify + ".pShooting");
	for (var i=0; i<pShootingOnly.length; i++) {
		if (pShootingOnly[i].value == pShootingValue) {
			pShootingOnly[i].checked = true;
			console.log("Loading shooter bot");
		}
	}
	var pDefenseBot = document.getElementsByName("defended");
	var pDefenseValue = localStorage.getItem(matchToModify + ".pDefense");
	for (var i=0; i<pDefenseBot.length; i++) {
		if (pDefenseBot[i].value == pDefenseValue) {
			pDefenseBot[i].checked = true;
			console.log("Loading defended against");
		}
	}
	var pReplayMatch = document.getElementsByName("replay");
	var pReplayValue = localStorage.getItem(matchToModify + ".pReplay");
	for (var i=0; i<pReplayMatch.length; i++) {
		if (pReplayMatch[i].value == pReplayValue) {
			pReplayMatch[i].checked = true;
			console.log("Loading replay match");
		}
	}

	//Comments
	pComments = localStorage.getItem(matchToModify + ".comments");
	if (pComments) {
		document.getElementById("comment_box").value = pComments;
		console.log("Loading comments");
	}
}

function setToDefault() {
	//Number values
	document.getElementById("gears_attempted_auton").value = 0;
	document.getElementById("gears_success_auton").value = 0;
	document.getElementById("high_goal_shots_auton").value = 0;
	document.getElementById("gears_attempted_teleop").value = 0;
	document.getElementById("gears_success_teleop").value = 0;
	document.getElementById("high_goal_shots_teleop").value = 0;
	document.getElementById("hang_time").value = 0;

	document.getElementById("comment_box").value = "";
}

window.onload = function() {

	matchNumValue = localStorage.getItem("preMatchNum");
	curMatchNum = document.getElementById("match_num").value;

	if (matchNumValue) {
		console.log("There have been matches scouted before.");
		if (matchNumValue != curMatchNum) {
			console.log("Different match from last time.");
			var userWipe = confirm("Do you want to wipe the data from match " + matchNumValue + "? There is unsaved information at the moment. Click OK to erase it, click Cancel to keep it.");
			if (userWipe) {
				console.log("User wiped information.");
				wipeData(matchNumValue);
			} else {
				console.log("User did not wipe information.");
			}
			setToDefault();
		} else {
			console.log("Loading information from last match.");
		}
		loadData(curMatchNum);
	} else {
		console.log("No matches have been scouted so far, the values will be set to default.")
		setToDefault();
	}
}

window.onbeforeunload = function() {
	//Match and team info
	matchNumValue = document.getElementById("match_num").value;
	localStorage.setItem("preMatchNum", matchNumValue);
	teamNumValue = document.getElementById("team_num").value;
	localStorage.setItem(matchNumValue + ".teamNum", teamNumValue);

	//Number values
	aGearsAttemptValue = document.getElementById("gears_attempted_auton").value;
	localStorage.setItem(matchNumValue + ".aGearsAttempt", aGearsAttemptValue);
	aGearsSuccessValue = document.getElementById("gears_success_auton").value;
	localStorage.setItem(matchNumValue + ".aGearsSuccess", aGearsSuccessValue);
	aHighGoalsValue = document.getElementById("high_goal_shots_auton").value;
	localStorage.setItem(matchNumValue + ".aHighGoals", aHighGoalsValue);
	tGearsAttemptValue = document.getElementById("gears_attempted_teleop").value;
	localStorage.setItem(matchNumValue + ".tGearsAttempt", tGearsAttemptValue);
	tGearsSuccessValue = document.getElementById("gears_success_teleop").value;
	localStorage.setItem(matchNumValue + ".tGearsSuccess", tGearsSuccessValue);
	tHighGoalsValue = document.getElementById("high_goal_shots_teleop").value;
	localStorage.setItem(matchNumValue + ".tHighGoals", tHighGoalsValue);

	//Comments
	pComments = document.getElementById(matchNumValue + "comments").value;
	localStorage.setItem(matchNumValue + ".comments", pComments);
}
//Radio buttons
$(document).ready(function(){
	$('input[name="starting_pos_auton"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.aPos', $(this).val());
	});
	$('input[name="low_goal_auton"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.aLowGoal', $(this).val());
	});
	$('input[name="hang"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.tHangSuccess', $(this).val());
	});
	$('input[name="hang_pos"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.tHangPos', $(this).val());
	});
	$('input[name="played_defense"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.tDefense', $(this).val());
	});
	$('input[name="pressure"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.pPressure', $(this).val());
	});
	$('input[name="only_shooting"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.pShooting', $(this).val());
	});
	$('input[name="defended"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.pDefense', $(this).val());
	});
	$('input[name="replay"]').on('change', function() {
		localStorage.setItem(matchNumValue + '.pReplay', $(this).val());
	});
});