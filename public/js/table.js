var ascAAGA = 1;
var ascAAGS = 1;
var ascAAGR = 1;
var ascAAHG = 1;
var ascATGA = 1;
var ascATGS = 1;
var ascATGR = 1;
var ascATHG = 1;
var ascAH = 1;
var ascAAS = 1;
var ascSAGA = 1;
var ascSAGS = 1;
var ascSAGR = 1;
var ascSAHG = 1;
var ascSTGA = 1;
var ascSTGS = 1;
var ascSTGR = 1;
var ascSTHG = 1;
var ascSH = 1;
var ascSAS = 1;

function sortTable(col, asc){
    var tbody = document.getElementById("tableBody");
    var rows = tbody.rows, rlen = rows.length, arr = new Array(), i, j, cells, clen;
    // fill the array with values from the table
    for(i = 0; i < rlen; i++){
        cells = rows[i].cells;
        clen = cells.length;
        arr[i] = new Array();
        for(j = 0; j < clen; j++){
            arr[i][j] = cells[j].innerHTML;
        }
    }
    // sort the array by the specified column number (col) and order (asc)
    arr.sort(function(a, b){
        return (a[col] == b[col]) ? 0 : ((a[col] > b[col]) ? asc : -1*asc);
    });
    for (i = 0; i < rlen; i++) {
        rows[i].innerHTML = "<td>" + arr[i].join("</td><td>") + "</td>";
        document.getElementById("firstCol").className = "headCol";
    }
}