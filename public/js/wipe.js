function wipeData(matchToModify) {
	//Wiping local storage

	//Number values
	localStorage.removeItem(matchToModify + ".aGearsAttempt");
	localStorage.removeItem(matchToModify + ".aGearsSuccess");
	localStorage.removeItem(matchToModify + ".aHighGoals");
	localStorage.removeItem(matchToModify + ".tGearsAttempt");
	localStorage.removeItem(matchToModify + ".tGearsSuccess");
	localStorage.removeItem(matchToModify + ".tHang");
	localStorage.removeItem(matchToModify + ".tHighGoals");

	//Radio buttons
	localStorage.removeItem(matchToModify + ".aPos");
	localStorage.removeItem(matchToModify + ".aLowGoal");
	localStorage.removeItem(matchToModify + ".tHangSuccess");
	localStorage.removeItem(matchToModify + ".tHangPos");
	localStorage.removeItem(matchToModify + ".tDefense");
	localStorage.removeItem(matchToModify + ".pShooting");
	localStorage.removeItem(matchToModify + ".pPressure");
	localStorage.removeItem(matchToModify + ".pDefense");
	localStorage.removeItem(matchToModify + ".pReplay");
	localStorage.removeItem(matchToModify + ".pYellow");

	//Drodown menus
	localStorage.removeItem(matchToModify + ".teamNum");
	localStorage.removeItem(matchToModify + ".aLowGoal");
	localStorage.removeItem(matchToModify + ".tLowGoal");
	localStorage.removeItem(matchToModify + ".pHang");

	localStorage.removeItem(matchToModify + ".comments");

	console.log("Wiping information for match " + matchToModify);
}