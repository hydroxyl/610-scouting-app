function adjustAGears() {
	document.getElementById("aGearsValue").innerHTML = document.getElementById("aGearsSlide").value;
	console.log("Adjusting auton gears to: " + document.getElementById("aGearsSlide").value);
}

function adjustTGears() {
	document.getElementById("tGearsValue").innerHTML = document.getElementById("tGearsSlide").value;
	console.log("Adjusting teleop gears to: " + document.getElementById("tGearsSlide").value);
}

function adjustAHighGoal() {
	document.getElementById("aHighGoalValue").innerHTML = document.getElementById("aHighGoalSlide").value;
	console.log("Adjusting auton high goal to: " + document.getElementById("aHighGoalSlide").value);
}

function adjustTHighGoal() {
	document.getElementById("tHighGoalValue").innerHTML = document.getElementById("tHighGoalSlide").value;
	console.log("Adjusting teleop high goal to: " + document.getElementById("tHighGoalSlide").value);
}

function adjustHang() {
	document.getElementById("hangValue").innerHTML = document.getElementById("hangSlide").value;
	console.log("Adjusting hang to: " + document.getElementById("hangSlide").value);
}

window.onload = function() {
	document.getElementById("aGearsValue").innerHTML = document.getElementById("aGearsSlide").value;
	document.getElementById("tGearsValue").innerHTML = document.getElementById("tGearsSlide").value;
	document.getElementById("aHighGoalValue").innerHTML = document.getElementById("aHighGoalSlide").value;
	document.getElementById("tHighGoalValue").innerHTML = document.getElementById("tHighGoalSlide").value;
	document.getElementById("hangValue").innerHTML = document.getElementById("hangSlide").value;
}