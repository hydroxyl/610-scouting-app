@extends('layouts/main')
@section('content')
<table class="table">
   <thead>
      <tr>
         <th>ID</th>
         <th>Scout ID</th>
         <th>Match Num</th>
         <th>Team Num</th>
         <th>Time Created</th>
         <th>Delete</th>
      </tr>
   </thead>
   <tbody>
      @foreach($matches as $match)
      <tr>
         <td><a href="{{route('admin.matchdata', $match->id)}}">{{$match->id}}</a></td>
         <td>{{$match->scout_id}}</td>
         <td>{{json_decode($match->data, true)['match_num']}}</td>
         <td>{{json_decode($match->data, true)['team_num']}}</td>
         <td>{{$match->created_at}}</td>
         <td><a href="{{route('admin.deletematch', $match->id)}}">Delete</a></td>
      </tr>
      @endforeach
   </tbody>
</table>
@stop
