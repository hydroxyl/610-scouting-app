@extends('layouts/main')
@section('content')
<h1>Current Event: {{$currentEvent}} </h1>
<hr>
<h3>
Enter the current match number:
<h3>
<form action="{{ route('form.callback') }}" method="post">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <div class="input-group">
      <input type="text" name="matchNum" class="form-control form-control-lg" placeholder="Match number">
      <span class="input-group-btn">
      <button class="btn btn-primary btn-lg" type="submit">Next</button>
      </span>
   </div>
</form>
@stop
