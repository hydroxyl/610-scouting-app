@extends('layouts/main')
@section('content')
<link rel="stylesheet" href="{{ url('css/dropzone.css') }}">
<script src="{{url('js/dropzone.js')}}"></script>

<script>
Dropzone.options.uploadWidget = {
  url: '{{ route('team.upload.callback', $teamdata['key']) }}',
  paramName: 'photo',
  maxFilesize: 50, // MB
  maxFiles: 10,
  dictDefaultMessage: 'Please drag and drop a robot photo to upload or click to select one.',
  acceptedFiles: 'image/*',
};
</script>
<form id="uploadWidget" action="{{ route('team.upload.callback', $teamdata['key']) }}" class="dropzone">
  <input type="hidden" name="team" value="{{$teamdata['key']}}">
</form>
@stop
