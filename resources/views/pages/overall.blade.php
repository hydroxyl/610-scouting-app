@extends('layouts/main')
@section('content')
@if(!isset($error))
<script src="{{ url('js/sliders.js') }}"></script>
<div class="card">
   <div class="card-block">
      <h3>"610 Number" Weights:</h3>
      <hr>
      <form action="{{ route('admin.overall') }}" method="get">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <p>Auton Gears: <span id="aGearsValue">1</span></p>
         <input type="range" min="0" max="5" id="aGearsSlide" onchange="adjustAGears()" @if(filter_var($request->auton_gears, FILTER_SANITIZE_NUMBER_INT) != null) value="{{filter_var($request->auton_gears, FILTER_SANITIZE_NUMBER_INT)}}" @else value="1" @endif name="auton_gears"><br>
         <p>Teleop Gears: <span id="tGearsValue">1</span></p>
         <input type="range" min="0" max="5" id="tGearsSlide" onchange="adjustTGears()" @if(filter_var($request->teleop_gears, FILTER_SANITIZE_NUMBER_INT) != null) value="{{filter_var($request->teleop_gears, FILTER_SANITIZE_NUMBER_INT)}}" @else value="1" @endif name="teleop_gears"><br>
         <p>Auton High Goal: <span id="aHighGoalValue">1</span></p>
         <input type="range" min="0" max="5" id="aHighGoalSlide" onchange="adjustAHighGoal()" @if(filter_var($request->auton_high_goal, FILTER_SANITIZE_NUMBER_INT) != null) value="{{filter_var($request->auton_high_goal, FILTER_SANITIZE_NUMBER_INT)}}" @else value="1" @endif name="auton_high_goal"><br>
         <p>Teleop High Goal: <span id="tHighGoalValue">1</span></p>
         <input type="range" min="0" max="5" id="tHighGoalSlide" onchange="adjustTHighGoal()" @if(filter_var($request->teleop_high_goal, FILTER_SANITIZE_NUMBER_INT) != null) value="{{filter_var($request->teleop_high_goal, FILTER_SANITIZE_NUMBER_INT)}}" @else value="1" @endif name="teleop_high_goal"><br>
         <p>Hang: <span id="hangValue">1</span></p>
         <input type="range" min="0" max="5" id="hangSlide" onchange="adjustHang()" @if(filter_var($request->hang, FILTER_SANITIZE_NUMBER_INT) != null) value="{{filter_var($request->hang, FILTER_SANITIZE_NUMBER_INT)}}" @else value="1" @endif name="hang"><br>
         <input class="btn btn-success" style="float: right;" type="submit" value="Recalculate 610 Number">
      </form>
   </div>
</div>
<script src="{{ url('js/table.js') }}"></script>
<table class="table table-responsive" id="sortableTable">
   <thead>
      <tr>
         <th>Team #</th>
         <!-- Sample sort table code
            onclick="sortTable(1, ascAAGA); ascAAGA *= -1; ascAAGS=1; ascAAGR=1; ascAAHG=1; ascATGA=1; ascATGS=1; ascATGR=1; ascATHG=1; ascAH=1; ascAAS=1; ascSAGA=1; ascSAGS=1; ascSAGR=1; ascSAHG=1; ascSTGA=1; ascSTGS=1; ascSTGR=1; ascSTHG=1; ascSH=1; ascSAS=1;" -->
         <th>"610" Ranking #</th>
         <th>Avg Auton Gear Attempted</th>
         <th>Avg Auton Gear Success</th>
         <th>Avg Auton Gear Accuracy</th>
         <th>Avg Auton High Goal Shots</th>
         <th>Avg Teleop Gear Attempted</th>
         <th>Avg Teleop Gear Success</th>
         <th>Avg Teleop Gear Accuracy</th>
         <th>Avg Teleop High Goal Shots</th>
         <th>Avg Hang</th>
         <!--<th>Avg Alliance Score</th>-->
      </tr>
   </thead>
   <tbody id="tableBody">

      @if(count($rankingNumbers) > 0)

      @foreach($rankingNumbers as $key => $value)
      <tr>
         <th scope="row" class="headcol" id="firstcol"><a href="{{route('admin.team', $analytics[$key]->id)}}">{{substr($analytics[$key]->id, 3)}}</a></td>
         <td class="table-warning">{{number_format($value, 2, '.', '')}}</td>
         <td class="table-warning">{{number_format($analytics[$key]->gears_attempted_auton_avg, 2, '.', '')}} ({{number_format($analytics[$key]->gears_attempted_auton_sd, 2, '.', '')}})</td>
         <td class="table-warning">{{number_format($analytics[$key]->gears_success_auton_avg, 2, '.', '')}} ({{number_format($analytics[$key]->gears_success_auton_sd, 2, '.', '')}})</td>
         <td class="table-warning">{{number_format($analytics[$key]->gear_accuracy_auton_avg, 2, '.', '')}}% ({{number_format($analytics[$key]->gear_accuracy_auton_sd, 2, '.', '')}}%)</td>
         <td class="table-warning">{{number_format($analytics[$key]->high_goal_shots_auton_avg, 2, '.', '')}} ({{number_format($analytics[$key]->high_goal_shots_auton_sd, 2, '.', '')}})</td>
         <td class="table-warning">{{number_format($analytics[$key]->gears_attempted_teleop_avg, 2, '.', '')}} ({{number_format($analytics[$key]->gears_attempted_teleop_sd, 2, '.', '')}})</td>
         <td class="table-warning">{{number_format($analytics[$key]->gears_success_teleop_avg, 2, '.', '')}} ({{number_format($analytics[$key]->gears_success_teleop_sd, 2, '.', '')}})</td>
         <td class="table-warning">{{number_format($analytics[$key]->gear_accuracy_teleop_avg, 2, '.', '')}}% ({{number_format($analytics[$key]->gear_accuracy_teleop_sd, 2, '.', '')}}%)</td>
         <td class="table-warning">{{number_format($analytics[$key]->high_goal_shots_teleop_avg, 2, '.', '')}} ({{number_format($analytics[$key]->high_goal_shots_teleop_sd, 2, '.', '')}})</td>
         <td class="table-warning">{{number_format($analytics[$key]->hang_avg, 2, '.', '')}} ({{number_format($analytics[$key]->hang_sd, 2, '.', '')}})</td>
         <!--<td class="table-warning">{{number_format($analytics[$key]->alliance_score_avg, 2, '.', '')}} ({{number_format($analytics[$key]->alliance_score_sd, 2, '.', '')}})</td>-->
         <!-- <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td>
            <td class="table-info"></td> -->
      </tr>
      @endforeach
      @else
      Sorry, there are no analytics at the moment.
      @endif
   </tbody>
</table>
</div>
</div>
</div>
@else
{{$error}}
@endif
@stop
