@extends('layouts/main')
@section('content')
<!--TODO: refactor the edit page to include the correct version -->
<script src="{{url('js/buttons.js')}}"></script>
<form action="{{ route('edit.callback') }}" method="post">
   @if ($error_message !=null)
       <span>{{$error_message}}</span>
   @else
       Old ID: <input id="oldID" name="old_id" value="{{$oldID}}" readonly/><br/>
       Scout ID: <input id="scoutID" name="scout_id" value="{{$scoutID}}" readonly/><br/>
       Event ID: <input id="eventID" name="event_id" value="{{$eventID}}" readonly/><br/>
       Team ID: <input id="teamID" name="team_id" value="{{$teamID}}"/><br/><br/>
       @foreach ($lines as $index => $line)
           @if ($line['type']=='hidden')
                <input id="{{$line['name']}}" name="{{$line['long_name']}}" value="{{$line['value']}}" hidden="true"/>
           @else
               <div>
                @for ($i=0;$i<(int)($line['level']);$i++)
                    &nbsp;&nbsp&nbsp;&nbsp;
                @endfor
                @if ($line['type']=='display')
                     <span>{{$line['name']}}</span>
                 @elseif ($line['type']=='edit')
                    <span>{{$line['name']}}:</span>
                    <input id="{{$line['name']}}" name="{{$line['long_name']}}" value="{{$line['value']}}"/>
                 @endif
               </div>
           @endif
       @endforeach
   @endif
    <button type="submit">Submit</button>

</form>
@stop
