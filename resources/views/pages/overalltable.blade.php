@extends('layouts/main')
@section('content')
<table class="table table-responsive" id="sortableTable">
	<thead>
		<tr>
			<th>Team #</th>
			<th onclick="sortTable(1);">610 Number</th>
			<?php $colNum = 2; ?>
			@foreach($headers as $header)
			<th onclick="sortTable(<?php echo $colNum ?>);">{{$header['label']}}</th>
			<?php $colNum++; ?>
			@endforeach
		</tr>
	</thead>
	<tbody id="tableBody">
		@foreach($dataTable as $teamNum=>$teamData)
		<tr>
			<td scope="row" class="headcol" id="headcol"><a href="{{route('admin.team', $teamNum)}}">{{$teamNum}}</a></td>
			<td>{{ round($number610[$teamNum]) }}</td>
			@foreach($headers as $header)
			<td>{{round($teamData[$header['id']], 2)}}</td>
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>

<script type="text/javascript">
	var asc = 1;

	function sortTable(col) {
		var tbody = document.getElementById('tableBody');
		var rows = tbody.rows;
		var switching = true;
		var shouldSwitch;

		while(switching) {
			switching = false;
			rows = tbody.getElementsByTagName("TR");
			for (i=0; i<(rows.length-1); i++) {
				shouldSwitch = false;
				var x = rows[i].getElementsByTagName("TD")[col];
				var y = rows[i+1].getElementsByTagName("TD")[col];
				if (x.innerHTML*asc > y.innerHTML*asc) {
					shouldSwitch = true;
					break;
				}
			}
			if (shouldSwitch) {
				rows[i].parentNode.insertBefore(rows[i+1], rows[i]);
				switching = true;
			}
		}

		asc *= -1;
	}
</script>
@stop
