@extends('layouts/main')
@section('content')
<div class="card-deck">
   <div class="card card-outline-info">
      <center>
         <h3 class="card-title">Blue</h3>
      </center>
      <div class="list-group">
         <div class="list-group-item flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
               <a href="{{route('admin.team',$matchData->blue1)}}">
                  <h5 class="mb-1">{{substr($matchData->blue1, 3)}}</h5>
               </a>
            </div>
            @if(isset($teamData['blue1']))
            <table class="table">
               <tbody>
                  <tr>
                     <td>Auton Gears</td>
                     <td>{{$teamData['blue1']->gears_success_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop Gears</td>
                     <td>{{$teamData['blue1']->gears_success_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Auton High Goal Shots</td>
                     <td>{{$teamData['blue1']->high_goal_shots_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop High Goal Shots</td>
                     <td>{{$teamData['blue1']->high_goal_shots_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Hang</td>
                     <td>{{$teamData['blue1']->hang}}</td>
                  </tr>
               </tbody>
            </table>
            @else
            <p class="mb-1">Sorry, we dont have match data for this team.</p>
            @endif
            <h3> Stats </h3>
            <table class="table">
              <tbody>
                <tr>
                  <td>Gears Success Teleop Avg</td>
                  <td>{{number_format($analytics['blue1']->gears_success_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Gears Success Auton Avg</td>
                  <td>{{number_format($analytics['blue1']->gears_success_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Auton Avg</td>
                  <td>{{number_format($analytics['blue1']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Teleop Avg</td>
                  <td>{{number_format($analytics['blue1']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Hang Avg</td>
                  <td>{{number_format(100* $analytics['blue1']->hang_avg, 2, '.', '')}}%</td>
                </tr>
              </tbody>
            </table>
         </div>
         <div class="list-group-item flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
               <a href="{{route('admin.team',$matchData->blue2)}}">
                  <h5 class="mb-1">{{substr($matchData->blue2, 3)}}</h5>
               </a>
            </div>
            @if(isset($teamData['blue2']))
            <table class="table">
               <tbody>
                  <tr>
                     <td>Auton Gears</td>
                     <td>{{$teamData['blue2']->gears_success_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop Gears</td>
                     <td>{{$teamData['blue2']->gears_success_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Auton High Goal Shots</td>
                     <td>{{$teamData['blue2']->high_goal_shots_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop High Goal Shots</td>
                     <td>{{$teamData['blue2']->high_goal_shots_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Hang</td>
                     <td>{{$teamData['blue2']->hang}}</td>
                  </tr>
               </tbody>
            </table>
            @else
            <p class="mb-1">Sorry, we dont have match data for this team.</p>
            @endif
            <h3> Stats </h3>
            <table class="table">
              <tbody>
                <tr>
                  <td>Gears Success Teleop Avg</td>
                  <td>{{number_format($analytics['blue2']->gears_success_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Gears Success Auton Avg</td>
                  <td>{{number_format($analytics['blue2']->gears_success_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Auton Avg</td>
                  <td>{{number_format($analytics['blue2']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Teleop Avg</td>
                  <td>{{number_format($analytics['blue2']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Hang Avg</td>
                  <td>{{number_format(100* $analytics['blue2']->hang_avg, 2, '.', '')}}%</td>
                </tr>
              </tbody>
            </table>
         </div>
         <div class="list-group-item flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
               <a href="{{route('admin.team',$matchData->blue3)}}">
                  <h5 class="mb-1">{{substr($matchData->blue3, 3)}}</h5>
               </a>
            </div>
            @if(isset($teamData['blue3']))
            <table class="table">
               <tbody>
                  <tr>
                     <td>Auton Gears</td>
                     <td>{{$teamData['blue3']->gears_success_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop Gears</td>
                     <td>{{$teamData['blue3']->gears_success_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Auton High Goal Shots</td>
                     <td>{{$teamData['blue3']->high_goal_shots_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop High Goal Shots</td>
                     <td>{{$teamData['blue3']->high_goal_shots_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Hang</td>
                     <td>{{$teamData['blue3']->hang}}</td>
                  </tr>
               </tbody>
            </table>
            @else
            <p class="mb-1">Sorry, we dont have match data for this team.</p>
            @endif
            <h3> Stats </h3>
            <table class="table">
              <tbody>
                <tr>
                  <td>Gears Success Teleop Avg</td>
                  <td>{{number_format($analytics['blue3']->gears_success_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Gears Success Auton Avg</td>
                  <td>{{number_format($analytics['blue3']->gears_success_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Auton Avg</td>
                  <td>{{number_format($analytics['blue3']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Teleop Avg</td>
                  <td>{{number_format($analytics['blue3']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Hang Avg</td>
                  <td>{{number_format(100* $analytics['blue3']->hang_avg, 2, '.', '')}}%</td>
                </tr>
              </tbody>
            </table>
         </div>
      </div>
      <div class="list-group-item flex-column align-items-start">
         <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Total</h5>
         </div>
         <table class="table">
            <tbody>
               <tr>
                  <td>Auton Gears</td>
                  <td>{{$total['blue_auton_gears']}}</td>
               </tr>
               <tr>
                  <td>Teleop Gears</td>
                  <td>{{$total['blue_teleop_gears']}}</td>
               </tr>
               <tr>
                  <td>Auton High Goal Shots</td>
                  <td>{{$total['blue_auton_highgoals']}}</td>
               </tr>
               <tr>
                  <td>Teleop High Goal Shots</td>
                  <td>{{$total['blue_teleop_highgoals']}}</td>
               </tr>
            </tbody>
         </table>
         <span></span>
         <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">"Predicted" Total</h5>
         </div>
         <table class="table">
            <tbody>
               <tr>
                  <td>Auton Gears</td>
                  <td>{{number_format($analytics['blue3']->gears_success_auton_avg + $analytics['blue2']->gears_success_auton_avg + $analytics['blue1']->gears_success_auton_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Teleop Gears</td>
                  <td>{{number_format($analytics['blue3']->gears_success_teleop_avg + $analytics['blue2']->gears_success_teleop_avg + $analytics['blue1']->gears_success_teleop_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Auton High Goal Shots</td>
                  <td>{{number_format($analytics['blue3']->high_goal_shots_auton_avg + $analytics['blue2']->high_goal_shots_auton_avg + $analytics['blue1']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Teleop High Goal Shots</td>
                  <td>{{number_format($analytics['blue3']->high_goal_shots_teleop_avg + $analytics['blue2']->high_goal_shots_teleop_avg + $analytics['blue1']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Hangs</td>
                  <td>{{number_format($analytics['blue3']->hang_avg + $analytics['blue2']->hang_avg + $analytics['blue1']->hang_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Total kPa</td>
                  <td>{{number_format($analytics['blue3']->high_goal_shots_auton_avg + $analytics['blue2']->high_goal_shots_auton_avg + $analytics['blue1']->high_goal_shots_auton_avg + ($analytics['blue3']->high_goal_shots_teleop_avg + $analytics['blue2']->high_goal_shots_teleop_avg + $analytics['blue1']->high_goal_shots_teleop_avg)/3, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Total Score</td>
                  <td>{{number_format($analytics['blue3']->high_goal_shots_auton_avg + $analytics['blue2']->high_goal_shots_auton_avg + $analytics['blue1']->high_goal_shots_auton_avg
                      + ($analytics['blue3']->high_goal_shots_teleop_avg + $analytics['blue2']->high_goal_shots_teleop_avg + $analytics['blue1']->high_goal_shots_teleop_avg)/3
                      , 0, '.', '')
                      + $total['blue_gear_score']
                      + number_format($analytics['blue3']->hang_avg + $analytics['blue2']->hang_avg + $analytics['blue1']->hang_avg + 0.5, 0, '.', '') * 50
                      + 15
                      }}</td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
   <div class="card card-outline-danger">
      <center>
         <h3 class="card-title">Red</h3>
      </center>
      <div class="list-group">
         <div class="list-group-item flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
               <a href="{{route('admin.team',$matchData->red1)}}">
                  <h5 class="mb-1">{{substr($matchData->red1, 3)}}</h5>
               </a>
            </div>
            @if(isset($teamData['red1']))
            <table class="table">
               <tbody>
                  <tr>
                     <td>Auton Gears</td>
                     <td>{{$teamData['red1']->gears_success_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop Gears</td>
                     <td>{{$teamData['red1']->gears_success_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Auton High Goal Shots</td>
                     <td>{{$teamData['red1']->high_goal_shots_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop High Goal Shots</td>
                     <td>{{$teamData['red1']->high_goal_shots_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Hang</td>
                     <td>{{$teamData['red1']->hang}}</td>
                  </tr>
               </tbody>
            </table>
            @else
            <p class="mb-1">Sorry, we dont have match data for this team.</p>
            @endif
            <h3> Stats </h3>
            <table class="table">
              <tbody>
                <tr>
                  <td>Gears Success Teleop Avg</td>
                  <td>{{number_format($analytics['red1']->gears_success_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Gears Success Auton Avg</td>
                  <td>{{number_format($analytics['red1']->gears_success_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Auton Avg</td>
                  <td>{{number_format($analytics['red1']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Teleop Avg</td>
                  <td>{{number_format($analytics['red1']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Hang Avg</td>
                  <td>{{number_format(100* $analytics['red1']->hang_avg, 2, '.', '')}}%</td>
                </tr>
              </tbody>
            </table>
         </div>
         <div class="list-group-item flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
               <a href="{{route('admin.team',$matchData->red2)}}">
                  <h5 class="mb-1">{{substr($matchData->red2, 3)}}</h5>
               </a>
            </div>
            @if(isset($teamData['red2']))
            <table class="table">
               <tbody>
                  <tr>
                     <td>Auton Gears</td>
                     <td>{{$teamData['red2']->gears_success_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop Gears</td>
                     <td>{{$teamData['red2']->gears_success_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Auton High Goal Shots</td>
                     <td>{{$teamData['red2']->high_goal_shots_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop High Goal Shots</td>
                     <td>{{$teamData['red2']->high_goal_shots_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Hang</td>
                     <td>{{$teamData['red2']->hang}}</td>
                  </tr>
               </tbody>
            </table>
            @else
            <p class="mb-1">Sorry, we dont have match data for this team.</p>
            @endif
            <h3> Stats </h3>
            <table class="table">
              <tbody>
                <tr>
                  <td>Gears Success Teleop Avg</td>
                  <td>{{number_format($analytics['red2']->gears_success_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Gears Success Auton Avg</td>
                  <td>{{number_format($analytics['red2']->gears_success_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Auton Avg</td>
                  <td>{{number_format($analytics['red2']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Teleop Avg</td>
                  <td>{{number_format($analytics['red2']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Hang Avg</td>
                  <td>{{number_format(100* $analytics['red2']->hang_avg, 2, '.', '')}}%</td>
                </tr>
              </tbody>
            </table>
         </div>
         <div class="list-group-item flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
               <a href="{{route('admin.team',$matchData->red3)}}">
                  <h5 class="mb-1">{{substr($matchData->red3, 3)}}</h5>
               </a>
            </div>
            @if(isset($teamData['red3']))
            <table class="table">
               <tbody>
                  <tr>
                     <td>Auton Gears</td>
                     <td>{{$teamData['red3']->gears_success_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop Gears</td>
                     <td>{{$teamData['red3']->gears_success_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Auton High Goal Shots</td>
                     <td>{{$teamData['red3']->high_goal_shots_auton}}</td>
                  </tr>
                  <tr>
                     <td>Teleop High Goal Shots</td>
                     <td>{{$teamData['red3']->high_goal_shots_teleop}}</td>
                  </tr>
                  <tr>
                     <td>Hang</td>
                     <td>{{$teamData['red3']->hang}}</td>
                  </tr>
               </tbody>
            </table>
            @else
            <p class="mb-1">Sorry, we dont have match data for this team.</p>
            @endif
            <h3> Stats </h3>
            <table class="table">
              <tbody>
                <tr>
                  <td>Gears Success Teleop Avg</td>
                  <td>{{number_format($analytics['red3']->gears_success_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Gears Success Auton Avg</td>
                  <td>{{number_format($analytics['red3']->gears_success_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Auton Avg</td>
                  <td>{{number_format($analytics['red3']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>High Goal Shots Teleop Avg</td>
                  <td>{{number_format($analytics['red3']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
                </tr>
                <tr>
                  <td>Hang Avg</td>
                  <td>{{number_format(100* $analytics['red3']->hang_avg, 2, '.', '')}}%</td>
                </tr>
              </tbody>
            </table>
         </div>
      </div>
      <div class="list-group-item flex-column align-items-start">
         <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Total</h5>
         </div>
         <table class="table">
            <tbody>
               <tr>
                  <td>Auton Gears</td>
                  <td>{{$total['red_auton_gears']}}</td>
               </tr>
               <tr>
                  <td>Teleop Gears</td>
                  <td>{{$total['red_teleop_gears']}}</td>
               </tr>
               <tr>
                  <td>Auton High Goal Shots</td>
                  <td>{{$total['red_auton_highgoals']}}</td>
               </tr>
               <tr>
                  <td>Teleop High Goal Shots</td>
                  <td>{{$total['red_teleop_highgoals']}}</td>
               </tr>
            </tbody>
         </table>
         <span></span>
         <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">"Predicted" Total</h5>
         </div>
         <table class="table">
            <tbody>
               <tr>
                  <td>Auton Gears</td>
                  <td>{{number_format($analytics['red3']->gears_success_auton_avg + $analytics['red2']->gears_success_auton_avg + $analytics['red1']->gears_success_auton_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Teleop Gears</td>
                  <td>{{number_format($analytics['red3']->gears_success_teleop_avg + $analytics['red2']->gears_success_teleop_avg + $analytics['red1']->gears_success_teleop_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Auton High Goal Shots</td>
                  <td>{{number_format($analytics['red3']->high_goal_shots_auton_avg + $analytics['red2']->high_goal_shots_auton_avg + $analytics['red1']->high_goal_shots_auton_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Teleop High Goal Shots</td>
                  <td>{{number_format($analytics['red3']->high_goal_shots_teleop_avg + $analytics['red2']->high_goal_shots_teleop_avg + $analytics['red1']->high_goal_shots_teleop_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Hangs</td>
                  <td>{{number_format($analytics['red3']->hang_avg + $analytics['red2']->hang_avg + $analytics['red1']->hang_avg, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Total kPa</td>
                  <td>{{number_format($analytics['red3']->high_goal_shots_auton_avg + $analytics['red2']->high_goal_shots_auton_avg + $analytics['red1']->high_goal_shots_auton_avg + ($analytics['red3']->high_goal_shots_teleop_avg + $analytics['red2']->high_goal_shots_teleop_avg + $analytics['red1']->high_goal_shots_teleop_avg)/3, 2, '.', '')}}</td>
               </tr>
               <tr>
                  <td>Total Score</td>
                  <td>{{number_format($analytics['red3']->high_goal_shots_auton_avg + $analytics['red2']->high_goal_shots_auton_avg + $analytics['red1']->high_goal_shots_auton_avg
                      + ($analytics['red3']->high_goal_shots_teleop_avg + $analytics['red2']->high_goal_shots_teleop_avg + $analytics['red1']->high_goal_shots_teleop_avg)/3
                      , 0, '.', '')
                      + $total['red_gear_score']
                      + number_format($analytics['red3']->hang_avg + $analytics['red2']->hang_avg + $analytics['red1']->hang_avg + 0.5, 0, '.', '') * 50
                      + 15
                      }}</td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>
@stop
