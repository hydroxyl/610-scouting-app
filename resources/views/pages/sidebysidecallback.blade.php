@extends('layouts/main')
@section('content')
<style>
   mark {
   background-color: yellow;
   color: black;
   }

   .better-dark {
       background-color: #e0e261;
       font-weight: 500;
       color: #303030;
   }

   .better {
      font-weight: 500;
      background-color: #f4eb42;
   }

   h3 {
    margin-top: 5px;
   }
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      <?php /*Run foreach chart loop, call all charts' draw function*/ ?>
      @foreach($displayGroups as $group)
      @if($group['type']=='charts')
      @foreach($group['charts'] as $chart)
        draw{{$chart['id']}}1();
        draw{{$chart['id']}}2();
      @endforeach
      @endif
      @endforeach
    }

    window.onresize = function() {
      drawChart();
    }

    function showCharts() {
      document.getElementById("charts1").removeAttribute("hidden");
      document.getElementById("charts2").removeAttribute("hidden");
      document.getElementById("chartsButton").setAttribute("onclick", "hideCharts();");
      document.getElementById("chartsButton").innerHTML = "Hide Charts";
      console.log("Showing charts");
    }

    function hideCharts() {
      document.getElementById("charts1").hidden = "true";
      document.getElementById("charts2").hidden = "true";
      document.getElementById("chartsButton").setAttribute("onclick", "showCharts();");
      document.getElementById("chartsButton").innerHTML = "Show Charts";
      console.log("Hiding charts");
    }
  </script>
<div>
    <button type="button" class="btn btn-outline-success" style="display:inline-block" id="chartsButton" onclick="hideCharts()">Hide Charts</button>
</div>
<br>
<div id="comparison" class="card-deck">
  <div class="card">
    <h3 class="card-title">&nbsp;Team 1: <a href="{{route('admin.team', $team1Num)}}">{{$team1Num}}</a></h3>
    @foreach($displayGroups as $group)
    <h5>&nbsp;{{$group['label']}}</h5>
    @if($group['type'] == 'numberTable')
      <table class="table table-hover">
        <tbody>
        @foreach($group['fields'] as $field)
          <tr>
            <td>{{$field['label']}}</td>
            <td><span @if(($field['better']=='gt'&&$team1Data[$field['id']]>$team2Data[$field['id']])||($field['better']=='lt'&&$team1Data[$field['id']]<$team2Data[$field['id']])) class="better" @endif>{{number_format($team1Data[$field['id']],2)}}</span></td>
          </tr>
        @endforeach
        </tbody>
      </table>
    @elseif($group['type'] == 'charts')
    <div id="charts1">
      @foreach($group['charts'] as $chart)
        <script type="text/javascript">
          <?php $info = json_decode($team1Data[$chart['id']], true); ?>
          function draw{{$chart['id']}}1() {
            @if($chart['type']=='perMatch' || $chart['type'] == 'timeHisto')
            var {{$chart['id'].'Data'}} = new google.visualization.DataTable();
            {{$chart['id'].'Data'}}.addColumn('string', 'Match Number');
            {{$chart['id'].'Data'}}.addColumn('number', 'value');
            {{$chart['id'].'Data'}}.addRows([
            @for($i=1; $i<count($info); $i++)
            @if($chart['type'] == 'perMatch')
              ['{{$info[$i]["matchNum"]}}', {{$info[$i]['hits']}}],
            @elseif($chart['type'] == 'timeHisto')
              ['{{$i-1}}', {{$info[$i][$i-1]}}],
            @endif
            @endfor
            ]);
            @elseif($chart['type'] == 'hangPerMatch')
            var {{$chart['id'].'Data'}} = new google.visualization.arrayToDataTable([
              ['Match Number', 'Centre Rung', 'Assissted', 'Carrier'],
              @for($i=1; $i<count($info); $i++)
                @if($info[$i]['hangPos']=='Center Rung')
                ['{{$info[$i]["matchNum"]}}',{{$info[$i]["hangTime"]}},0,0],
                @elseif($info[$i]['hangPos']=='Carried/Assisted')
                ['{{$info[$i]["matchNum"]}}',0,{{$info[$i]["hangTime"]}},0],
                @elseif($info[$i]['hangPos']=='Carrier')
                ['{{$info[$i]["matchNum"]}}',0,0,{{$info[$i]["hangTime"]}}],
                @else
                ['{{$info[$i]["matchNum"]}}',0,0,0],
                @endif
              @endfor
            ]);
            @elseif($chart['type'] == 'autoPerMatch')
            var {{$chart['id'].'Data'}} = new google.visualization.arrayToDataTable([
              ['Match Number', 'Scale', 'Switch', 'Exchange'],
              @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}',{{$info[$i]["scl"]}},{{$info[$i]["hsw"]}},{{$info[$i]["xch"]}}],
              @endfor
            ]);
            @elseif($chart['type'] == 'autoPickupPerMatch')
            var {{$chart['id'].'Data'}} = new google.visualization.arrayToDataTable([
              ['Match Number', 'Count'],
              @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}',{{$info[$i]["numCubes"]}}],
              @endfor
            ]);
            @endif
            var {{$chart['id']}}Options = {
              // backgroundColor: '#202020',
              @if($chart['type']=='perMatch'||$chart['type']=='timeHisto')legend: { position: "none"},
              @elseif($chart['type']=='hangPerMatch')legend: {textStyle:{/*color:'#CCC'*/}},@endif
              title: '{{$chart["title"]}}',
              titleTextStyle: {
                // color:'#CCC'
              },
              hAxis: {
                title: '{{$chart["hAxis"]["title"]}}',
                titleTextStyle: {
                  // color:'#CCC'
                },
                textStyle: {
                  // color:'#CCC'
                }@if($chart['type']=='timeHisto'),
                viewWindow: {
                  min: 0,
                  max: 30
                }
                @endif
              },
              vAxis: {
                title: '{{$chart["vAxis"]["title"]}}',
                format: 'short',
                viewWindow: {
                  min: 0
                },
                titleTextStyle: {
                  // color:'#CCC'
                },
                gridlines: {
                  // color: '#CCC'
                },
                textStyle: {
                  // color:'#CCC'
                }
              }@if($chart['type']=='hangPerMatch'||$chart['type']=='autoPerMatch'),
              isStacked: true
              @endif
            };
            
            var {{$chart['id'].'Chart'}} = new google.visualization.ColumnChart(document.getElementById('{{$chart["id"]."1ChartArea"}}'));
            {{$chart['id'].'Chart'}}.draw({{$chart['id'].'Data'}}, {{$chart['id'].'Options'}});
          } 
        </script>
        <div id='{{$chart["id"]."1ChartArea"}}'></div>
      @endforeach
    </div>
    @endif
    <hr>
    @endforeach
  </div>
  <div class="card">
    <h3 class="card-title">&nbsp;Team 2: <a href="{{route('admin.team', $team2Num)}}">{{$team2Num}}</a></h3>
    @foreach($displayGroups as $group)
    <h5>&nbsp;{{$group['label']}}</h5>
    @if($group['type'] == 'numberTable')
    <table class="table table-hover">
      <tbody>
      @foreach($group['fields'] as $field)
        <tr>
          <td>{{$field['label']}}</td>
          <td><span @if(($field['better']=='gt'&&$team2Data[$field['id']]>$team1Data[$field['id']])||($field['better']=='lt'&&$team2Data[$field['id']]<$team1Data[$field['id']])) class="better" @endif>{{number_format($team2Data[$field['id']],2)}}</span></td>
        </tr>
      @endforeach
      </tbody>
    </table>
    @elseif($group['type'] == 'charts')
    <div id="charts2">
      @foreach($group['charts'] as $chart)
        <script type="text/javascript">
          <?php $info = json_decode($team2Data[$chart['id']], true); ?>
          function draw{{$chart['id']}}2() {
            @if($chart['type']=='perMatch' || $chart['type'] == 'timeHisto')
            var {{$chart['id'].'Data'}} = new google.visualization.DataTable();
            {{$chart['id'].'Data'}}.addColumn('string', 'Match Number');
            {{$chart['id'].'Data'}}.addColumn('number', 'value');
            {{$chart['id'].'Data'}}.addRows([
            @for($i=1; $i<count($info); $i++)
            @if($chart['type'] == 'perMatch')
              ['{{$info[$i]["matchNum"]}}', {{$info[$i]['hits']}}],
            @elseif($chart['type'] == 'timeHisto')
              ['{{$i-1}}', {{$info[$i][$i-1]}}],
            @endif
            @endfor
            ]);
            @elseif($chart['type'] == 'hangPerMatch')
            var {{$chart['id'].'Data'}} = new google.visualization.arrayToDataTable([
              ['Match Number', 'Centre Rung', 'Assissted', 'Carrier'],
              @for($i=1; $i<count($info); $i++)
                @if($info[$i]['hangPos']=='Center Rung')
                ['{{$info[$i]["matchNum"]}}',{{$info[$i]["hangTime"]}},0,0],
                @elseif($info[$i]['hangPos']=='Carried/Assisted')
                ['{{$info[$i]["matchNum"]}}',0,{{$info[$i]["hangTime"]}},0],
                @elseif($info[$i]['hangPos']=='Carrier')
                ['{{$info[$i]["matchNum"]}}',0,0,{{$info[$i]["hangTime"]}}],
                @else
                ['{{$info[$i]["matchNum"]}}',0,0,0],
                @endif
              @endfor
            ]);
            @elseif($chart['type'] == 'autoPerMatch')
            var {{$chart['id'].'Data'}} = new google.visualization.arrayToDataTable([
              ['Match Number', 'Scale', 'Switch', 'Exchange'],
              @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}',{{$info[$i]["scl"]}},{{$info[$i]["hsw"]}},{{$info[$i]["xch"]}}],
              @endfor
            ]);
            @elseif($chart['type'] == 'autoPickupPerMatch')
            var {{$chart['id'].'Data'}} = new google.visualization.arrayToDataTable([
              ['Match Number', 'Count'],
              @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}',{{$info[$i]["numCubes"]}}],
              @endfor
            ]);
            @endif
            var {{$chart['id']}}Options = {
              // backgroundColor: '#202020',
              @if($chart['type']=='perMatch'||$chart['type']=='timeHisto')legend: { position: "none"},
              @elseif($chart['type']=='hangPerMatch')legend: {textStyle:{/*color:'#CCC'*/}},@endif
              title: '{{$chart["title"]}}',
              titleTextStyle: {
                // color:'#CCC'
              },
              hAxis: {
                title: '{{$chart["hAxis"]["title"]}}',
                titleTextStyle: {
                  // color:'#CCC'
                },
                textStyle: {
                  // color:'#CCC'
                }@if($chart['type']=='timeHisto'),
                viewWindow: {
                  min: 0,
                  max: 30
                }
                @endif
              },
              vAxis: {
                title: '{{$chart["vAxis"]["title"]}}',
                format: 'short',
                viewWindow: {
                  min: 0
                },
                titleTextStyle: {
                  // color:'#CCC'
                },
                gridlines: {
                  // color: '#CCC'
                },
                textStyle: {
                  // color:'#CCC'
                }
              }@if($chart['type']=='hangPerMatch'||$chart['type']=='autoPerMatch'),
              isStacked: true
              @endif
            };
            
            var {{$chart['id'].'Chart'}} = new google.visualization.ColumnChart(document.getElementById('{{$chart["id"]."2ChartArea"}}'));
            {{$chart['id'].'Chart'}}.draw({{$chart['id'].'Data'}}, {{$chart['id'].'Options'}});
          } 
        </script>
        <div id='{{$chart["id"]."2ChartArea"}}'></div>
      @endforeach
      </div>
    @endif
    <hr>
    @endforeach
  </div>
</script>

@stop
