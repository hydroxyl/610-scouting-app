@extends('layouts/main')
@section('content')

<a href ="{{route('admin.team', $id)}}"> <i class="fa fa-angle-left"></i>Back </a><br><br>

<div class="card-deck">
@foreach($photos as $photo)



<div class="card" style="width: 20rem;">
  <img class="card-img-top" height="360" src="{{url($photo->path)}}">
  <div class="card-block">
    <p class="card-text">{{$photo->scout_email}}</p>
    <a href="{{route('admin.team.delete', $photo->id)}}" class="btn btn-primary">Delete</a>
  </div>
</div>
<br>

@endforeach
</div>

@stop
