@extends('layouts/main')
@section('content')

<table class="table table-hover">
   <thead>
      <tr>
         <th>ID</th>
         <th>Is Current Event</th>
         <th>Event ID</th>
         <th>Created At</th>
         <th>Updated At</th>
         <th>Set As Current Event</th>
         <th>Update</th>
         <th>Delete</th>
      </tr>
   </thead>
   <tbody>
      @foreach($events as $event)
      <tr>
         <td>{{$event->id}}</td>
         <td>@if($event->is_current_event == true) <i class="fa fa-check" aria-hidden="true"></i> @else <i class="fa fa-times" aria-hidden="true"></i> @endif </td>
         <td>{{$event->event}}</td>
         <td>{{$event->created_at}}</td>
         <td>{{$event->updated_at}}</td>
         <td><a href="{{route('admin.events.setcurrent', $event->id)}}">Set</a></td>
         <td><a href="{{route('admin.events.update', $event->id)}}">Update</a></td>
         <td><a href="{{route('admin.events.delete', $event->id)}}">Delete</a></td>
      </tr>
      @endforeach
   </tbody>
</table>

  <hr>

  <a class="btn btn-outline-success" style="float: right;" href="{{route ('admin.events.new')}}" role="button">New</a>
@stop
