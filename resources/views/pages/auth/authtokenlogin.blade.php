<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="{{ url('favicon.ico') }}">
      <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
      <link rel="stylesheet" href="{{ url('css/custom.css') }}">
      <link rel="stylesheet" href="{{ url('css/font-awesome.css') }}">
      <script src="{{url('js/wipe.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/js/tether.min.js"></script>
      <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
      <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
      <script src="{{ url('js/bootstrap.js') }}"></script>
      <script src="{{ url('js/collapse.js') }}"></script>
      <title>Team 610 Scouting</title>
   </head>
   <body>
      <div class="container centeredcontainer">
         <div class="col-md-3">
         </div>
         <div class="col-md-6">
            <center>
               <img src="{{ url('img/logo128.png') }}">
               <div class="card centercard">
                  <div class="card-header">
                     Please enter the provided token below
                  </div>
                  <div class="card-block" class="nounderline">
                     @if(Session::has('error'))
                     <div class="alert alert-danger">
                        {{Session::get('error') }}
                     </div>
                     @endif
                     @if (count($errors) > 0)
                     <div class="alert alert-danger">
                        <ul>
                           @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                     </div>
                     @endif
                     <form action="{{ route('accesstokens.login') }}" method="post">
                        <div class="input-group">
                           <span class="input-group-addon" id="basic-addon3">Token</span>
                           <input type="text" class="form-control" name="token" aria-describedby="basic-addon3">
                        </div>
                        <br>
                        <input class="btn btn-outline-success" style="float: right;" type="submit" id="submitBtn" value="Login">
                     </form>
                  </div>
               </div>
            </center>
         </div>
      </div>
   </body>
</html>
