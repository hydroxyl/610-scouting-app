<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="{{ url('favicon.ico') }}">
      <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
      <link rel="stylesheet" href="{{ url('css/custom.css') }}">
      <link rel="stylesheet" href="{{ url('css/font-awesome.css') }}">
      <script src="{{url('js/wipe.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/js/tether.min.js"></script>
      <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
      <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
      <script src="{{ url('js/bootstrap.js') }}"></script>
      <title>Team 610 Scouting</title>
   </head>
   <body>
      <div class="container centeredcontainer">
         <div class="col-md-3">
         </div>
         <div class="col-md-6">
            <center>
              @if(Session::has('error'))
              <div class="alert alert-danger">
                 {{Session::get('error') }}
              </div>
              <br>
              @endif
               <img src="{{ url('img/logo128.png') }}">
               <div class="card centercard">
                  <div class="card-header">
                     Please login with One of the following providers
                  </div>
                  <div class="card-block" class="nounderline">
                     <a href="{{ route('login.redirect') }}" style="font-size:0;">
                     <img src="{{ url('img/signinwithgoogle.png') }}" height ="70">
                     </a>
                     <br>
                     <br>
                     <a href="{{ route('login.withaccesstoken') }}" style="font-size:0;">
                     <img src="{{ url('img/inverse-AccessToken2.png') }}" height = "70">
                     </a>
                  </div>
               </div>
            </center>
         </div>
      </div>
   </body>
</html>
