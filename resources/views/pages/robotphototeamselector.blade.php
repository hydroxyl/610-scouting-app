@extends('layouts/main')
@section('content')
<h3>
Enter team number:
<h3>
<form action="{{ route('robotphotos.select.callback') }}" method="post">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <div class="input-group">
      <input type="text" name="team" class="form-control form-control-lg" placeholder="Team Number">
      <span class="input-group-btn">
      <button class="btn btn-primary btn-lg" type="submit">Next</button>
      </span>
   </div>
   <br>
   <h3>Completion</h3>
   <br>
   <table class="table table-hover">
      <tbody>
         @foreach($teams as $team)
         <tr>
            <td>{{substr($team['key'], 3)}}</td>
            <td>
            @if($status[$team['key']] == 1)
            <i class="fa fa-check" aria-hidden="true"></i>
            @else
            <i class="fa fa-times" aria-hidden="true"></i>
            @endif
            </td>
         </tr>
         @endforeach
      </tbody>
   </table>
</form>
@stop
