@extends('layouts/main')
@section('content')
@if(count($teams1) > 0)
<?php
// TODO: reimplement search
// <form class="form-inline my-2 my-lg-0" action="{{ route('admin.teamsearch') }}" method="post">
//    <input class="form-control mr-sm-2 pull-xs-right" type="text" name="team" placeholder="Team Number">
//    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
// </form>
// <hr>
?>
<table class="table">
   <tbody>
      @for($x = 0; $x <= count($teams1); $x++)
      <tr>
         @if(isset($teams1[$x]))
         <td><a href="{{route('admin.team', $teams1[$x]['team_number'])}}">{{$teams1[$x]['team_number']}}</a></td>
         @endif
         @if(isset($teams2[$x]))
         <td><a href="{{route('admin.team', $teams2[$x]['team_number'])}}">{{$teams2[$x]['team_number']}}</a></td>
         @endif
         @if(isset($teams3[$x]))
         <td><a href="{{route('admin.team', $teams3[$x]['team_number'])}}">{{$teams3[$x]['team_number']}}</a></td>
         @endif
      </tr>
      @endfor
   </tbody>
</table>
@else
There is no team data in our database :(
@endif
@stop
