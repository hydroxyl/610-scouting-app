@extends('layouts/main')
@section('content')

@if(isset($error))
<p>{{$error}}</p>
@else
<table class="table table-hover">
   <thead>
      <tr>
         <th>Match Number</th>
         <th>Match ID</th>
         <th>Red 1</th>
         <th>Red 2</th>
         <th>Red 3</th>
         <th>Blue 1</th>
         <th>Blue 2</th>
         <th>Blue 3</th>
      </tr>
   </thead>
   <tbody>
      @foreach($matches as $match)
      <tr>
         <th scope="row"><a href='{{'match/'.$match->match_number}}'>{{$match->match_number}}</a></th>
         <td>{{$match->key}}</td>
         <td>{{substr($match->alliances->red->team_keys[0], 3)}}</td>
         <td>{{substr($match->alliances->red->team_keys[1], 3)}}</td>
         <td>{{substr($match->alliances->red->team_keys[2], 3)}}</td>
         <td>{{substr($match->alliances->blue->team_keys[0], 3)}}</td>
         <td>{{substr($match->alliances->blue->team_keys[1], 3)}}</td>
         <td>{{substr($match->alliances->blue->team_keys[2], 3)}}</td>
      </tr>
      @endforeach
   </tbody>
</table>

@endif
@stop
