@extends('layouts/main')
@section('content')
<style>
   mark {
   background-color: yellow;
   color: black;
   }

	.red {
		background-color: #f55b5b;
	}

	.blue {
		background-color: #5ba0f5;
	}

   .better-dark {
       background-color: #e0e261;
       font-weight: 500;
       color: #303030;
   }

   .better {
      font-weight: 500;
      background-color: #f4eb42;
   }

   h3 {
    margin-top: 5px;
   }
</style>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Position</th>
			<th>Team #</th>
			<th>Auto Scored</th>
			<th>Average Cycles</th>
			<th>Scale Cycles</th>
			<th>Home Switch Cycles</th>
			<th>Away Switch Cycles</th>
			<th>Exchange Cycles</th>
			<th>Hang Percentage</th>

		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="red">Red 1</td>
			<td>{{$teamList['red1']}}</td>
			<?php
			foreach($dataTable[$teamList['red1']] as $data) {
				echo "<td>$data</td>";
			}
			?>
		</tr>
		<tr>
			<td class="red">Red 2</td>
			<td>{{$teamList['red2']}}</td>
			<?php
			foreach($dataTable[$teamList['red2']] as $data) {
				echo "<td>$data</td>";
			}
			?>
		</tr>
		<tr>
			<td class="red">Red 3</td>
			<td>{{$teamList['red3']}}</td>
			<?php
			foreach($dataTable[$teamList['red3']] as $data) {
				echo "<td>$data</td>";
			}
			?>
		</tr>
		<tr>
			<th style="color: white;">-</th>
		</tr>
		<tr>
			<td class="blue">Blue 1</td>
			<td>{{$teamList['blue1']}}</td>
			<?php
			foreach($dataTable[$teamList['blue1']] as $data) {
				echo "<td>$data</td>";
			}
			?>
		</tr>
		<tr>
			<td class="blue">Blue 2</td>
			<td>{{$teamList['blue2']}}</td>
			<?php
			foreach($dataTable[$teamList['blue2']] as $data) {
				echo "<td>$data</td>";
			}
			?>
		</tr>
		<tr>
			<td class="blue">Blue 3</td>
			<td>{{$teamList['blue3']}}</td>
			<?php
			foreach($dataTable[$teamList['blue3']] as $data) {
				echo "<td>$data</td>";
			}
			?>
		</tr>
	</tbody>
</table>
<script>
	window.onload = function () {
		var elem = document.getElementsByTagName('td');

		//            1  2  3  4  5  6  7
		var thresh = [1, 9, 6, 3, 3, 5, 0.8];
		//Auto Scored
		for (var i = 0; i < elem.length; i++) {
			if ( i % 9 > 1 && Number(elem[i].innerHTML) >= thresh[i % 9 - 2] ) {
				elem[i].classList += ' better';
			}
		}
		// for (var i = 0; i < elem.length; i++) {
		// 	if (i % 9 > 1) {
		// 		var percent = 0.9 * Number(elem[i].innerHTML) / thresh[i%9];
		// 		var g = Math.round(255 * percent);
		// 		elem[i].style.backgroundColor = 'rgba(90, 255, 90, ' + percent + ')';
		// 	}
		// 	// if ( i % 9 > 1 && Number(elem[i].innerHTML) >= maxs[i % 9] && maxs[i % 9] > 0) {
		// 	// 	elem[i].classList += ' better';
		// 	// 	// elem[i].style.backgroundColor = 'rgba(0, 0, 255, 0.5)';
		// 	// }
		// }
	}
</script>
@stop
