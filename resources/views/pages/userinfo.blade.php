@extends('layouts/main')
@section('content')
<div class="card">
  <div class="card-block">
    <h3><u>User Info</u></h3>
    <table class="table table-responsive">
      <tbody>
        <tr>
        <td>
          <td>First Name</td>
          <td>{{$userData->first_name}}</td>
        </tr>
        <td>
          <td>Last Name</td>
          <td>{{$userData->last_name}}</td>
        </tr>
        <td>
          <td>Email</td>
          <td>{{$userData->email}}</td>
        </tr>
        <td>
          <td>Login Count</td>
          <td>{{$userData->login_count}}</td>
        </tr>
        <td>
          <td>Matches Scouted</td>
          <td>{{count($matchData)}}</td>
        </tr>
      </tbody>
    </table>
    <br>
    <h3 id="matchcomments"><u>Match Comments</u></h3>
    <br>
    <table class="table table-responsive">
       <thead>
          <tr>
             <th>Match #</th>
             <th>Comments</th>
          </tr>
       </thead>
       <tbody>
          @foreach($matchData as $match)
          <tr>
             <td>{{json_decode($match->data, true)['match_num']}}</td>
             <td>{{json_decode($match->data, true)['p_comments']}}</td>
          </tr>
          @endforeach
       </tbody>
    </table>
    <br>

        <h3><u>Match Data</u></h3>
        <table class="table table-responsive">
           <thead>
              <tr>
                 <th>Match #</th>
                 <th>Team</th>
                 <th>Edit</th>
              </tr>
           </thead>
           <tbody>
              @foreach($matchData as $match)
              <tr>
                 <td><a href="{{route('admin.matchdata', $match->id)}}">{{json_decode($match->data, true)['match_num']}}</a></td>
                 <td>{{substr($match->team_id, 3)}}</td>
                 <td><a href="{{route('edit', $match->id)}}">Edit</a></td>
              </tr>
              @endforeach
           </tbody>
        </table>
<br>

    <h3 id="robotphotos"><u>Robot Photos</u></h3>
    <br>
    <br>
    @if(count($robotPhotos) >= 1)
    @foreach($robotPhotos as $photo)
    <div class="card">
      <a href="{{url($photo->path)}}">
       <img src="{{url($photo->path)}}" class="img-thumbnail">
     </a>
       <br>
       <p class="card-text">Uploaded by {{$photo->scout_email}} <a href ={{route('admin.team.delete', $photo->id)}}>Delete</a></p>
       <p class="card-text"><small class="text-muted">Uploaded at {{$photo->created_at}}</small></p>
    </div>
    <br>
    @endforeach
    @else
    <p id="robotphotos">Sorry, we dont have any photos from this scout.</p>
    @endif
    <br>
  </div>
</div>
@stop
