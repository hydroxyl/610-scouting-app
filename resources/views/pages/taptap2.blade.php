@extends('pages/inputform')
@section('tap-tap')
<style>
	.cycle-times-popup {
		display: grid;
		grid-template-columns: 30.5% 19.5% 19.5% 30.5%;
		grid-template-rows: 50% 50%;
		margin:auto;
		background-image: url('img/LayoutandMarkingDiagram.png');
		background-size: 100% 100%;
	}

	.cycle-zone {
		border-style: solid;
		border-width: thin;
		border-color: blue;
		text-align: center;
		vertical-align: middle;
	}

	.big-button {
		height: 50px;
		margin-top: 5px;
		margin-bottom: 5px;
	}

	.small-button {
		height: 35px;
		margin-bottom: 10px;
	}

	#button-row {
		align-self: center;
	}
</style>

<button class="small-button btn btn-danger" type="button" onclick="kill()">Kill Cycle</button>&nbsp;&nbsp;<button class="small-button btn btn-danger" type="button" onclick="markLast()">Mark Last</button>
<div class="cycle-times-popup">
	@for($zone=1; $zone<=8; $zone++)
		<div class="cycle-zone" id="zone{{$zone}}" onclick="start({{$zone}});"></div>
	@endfor
</div>
<?php //TODO add another button for second layer scale scoring ?>
<br>
<span id="timer"><b>0.0 seconds</b></span>
<div id="button-row">
	<button class="big-button btn btn-success" type="button" onclick="success('Xch')">Exchange</button>&nbsp;&nbsp;
	<button class="big-button btn btn-success" type="button" onclick="success('HSw')">Home Switch</button>&nbsp;&nbsp;
	<button class="big-button btn btn-success" type="button" onclick="success('Scl')">Scale</button>&nbsp;&nbsp;
	<button class="big-button btn btn-success" type="button" onclick="success('ASw')">Away Switch</button>&nbsp;&nbsp;<br>
	<button class="big-button btn btn-danger" type="button" onclick="fail('Xch')">Exchange</button>&nbsp;&nbsp;
	<button class="big-button btn btn-danger" type="button" onclick="fail('HSw')">Home Switch</button>&nbsp;&nbsp;
	<button class="big-button btn btn-danger" type="button" onclick="fail('Scl')">Scale</button>&nbsp;&nbsp;
	<button class="big-button btn btn-danger" type="button" onclick="fail('ASw')">Away Switch</button>&nbsp;&nbsp;
	<button class="big-button btn btn-danger" type="button" onclick="fail('Drp')">Cube Dropped</button>&nbsp;&nbsp;
</div>
<br>
<div id="cycle-table" style="display:inline-block;">
	<table class="table">
		<thead>
			<tr>
				<th>#</th><th>&#127988;</th>
			</tr>
		</thead>
		<tbody id="cycleTableBody">
		</tbody>
	</table>
</div>

<textarea name="cycle_times" id="cycle_times" style="font-family:monospace;" wrap="soft" hidden readonly>[{}]</textarea>

<script type="text/javascript">
	var inCycle=false;
	var startTime;
	var startZone;
	var scoreZone;
	var endTime;
	var timer;
	var lapCnt=0;
	var cycleCnt=1;
	var totalLapCnt=0;

	function start(aZone) {
		if (inCycle==false) {
			startZone = aZone;
			document.getElementById("zone"+startZone).style="background-color:red; opacity:0.5";
			var d = new Date();
			startTime = d.getTime();
			startTimer();
			inCycle = true;
			lapCnt++;
			totalLapCnt++;
		}
	}

	function addRow() {
		document.getElementById("cycleTableBody").innerHTML = document.getElementById("cycleTableBody").innerHTML + '<tr id="cycleRow'+totalLapCnt+'""><td>'+totalLapCnt+'</td><td>&#10008;</td></tr>';
	}

	function endCycle() {
		document.getElementById("zone"+startZone).style="";
		var d= new Date();
		endTime = d.getTime();
		endTimer();
		inCycle = false;
	}

	function kill() {
		if (inCycle == true) {
			endCycle();
			lapCnt--;
			totalLapCnt--;
		}
	}

	function success(aZone) {
		if (inCycle==true) {
			scoreZone = aZone;
			endCycle();
			var text = document.getElementById("cycle_times").innerHTML;
			document.getElementById("cycle_times").innerHTML = text.substring(0,text.length-1)+',{"cycleNum":'+cycleCnt+',"lapNum":'+lapCnt+',"startZone":"'+startZone+'","scoreZone":"'+scoreZone+'","startTime":'+startTime+',"endTime":'+endTime+',"duration":'+(endTime-startTime)/1000+',"score":"true","mark":"false"}]';
			cycleCnt++;
			lapCnt=0;
			addRow();
		}
	}

	function fail(aZone) {
		if (inCycle==true) {
			scoreZone = aZone;
			endCycle();
			var text = document.getElementById("cycle_times").innerHTML;
			document.getElementById("cycle_times").innerHTML = text.substring(0,text.length-1)+',{"cycleNum":'+cycleCnt+',"lapNum":'+lapCnt+',"startZone":"'+startZone+'","scoreZone":"'+scoreZone+'","startTime":'+startTime+',"endTime":'+endTime+',"duration":'+(endTime-startTime)/1000+',"score":"false","mark":"false"}]';
			addRow();
		}
	}

	function markLast() {
		if (inCycle==false) {
			var cycleJSON = JSON.parse(document.getElementById("cycle_times").innerHTML);
			if(cycleJSON[cycleJSON.length-1].mark == "false") {
				cycleJSON[cycleJSON.length-1].mark = "true";
				markRow(true);
			} else {
				cycleJSON[cycleJSON.length-1].mark = "false";
				markRow(false);
			}
			document.getElementById("cycle_times").innerHTML = JSON.stringify(cycleJSON);
		}
	}

	function markRow(toCheck) {
		if(toCheck) {
			document.getElementById("cycleRow"+totalLapCnt).innerHTML = '<td>'+totalLapCnt+'</td><td>&#10004;</td>';
		} else {
			document.getElementById("cycleRow"+totalLapCnt).innerHTML = '<td>'+totalLapCnt+'</td><td>&#10008;</td>';
		}
	}

	function startTimer() {
		timer = window.setInterval(function() {
			var d = new Date();
			var curTime = d.getTime();
			var duration = (Math.round((curTime-startTime)/100)/10).toFixed(1);
			document.getElementById('timer').innerHTML = "<b>"+duration+" seconds</b>";
		}, 100);
	}

	function endTimer() {
		window.clearInterval(timer);
		document.getElementById('timer').innerHTML = "<b>0.0 seconds</b>";
	}
</script>
@stop