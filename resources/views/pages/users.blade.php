@extends('layouts/main')
@section('content')
<table class="table table-hover">
   <thead>
      <tr>
         <th>ID</th>
         <th>First Name</th>
         <th>Last Name</th>
         <th>Role</th>
         <th>Edit</th>
      </tr>
   </thead>
   <tbody>
      @foreach($users as $user)
      <tr>
         <th scope="row"><a href="{{route('admin.userinfo', $user->id)}}">{{$user->id}}</a></th>
         <td>{{$user->first_name}}</td>
         <td>{{$user->last_name}}</td>
         <td>{{$user->role}}</td>
         <td><a href="{{route('user', $user->id)}}">Edit</a></td>
      </tr>
      @endforeach
   </tbody>
</table>
@stop
