@extends('layouts/main')
@section('content')
<style>
   robotIMG {
   width:  auto;
   height: auto;
   }
   img {
     image-orientation: from-image;
   }
</style>
<link rel="stylesheet" href="{{ url('css/team.css') }}">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
           <h3>Expert Scout: <mark id="main">{{$exp}}</mark></h3> <a href="{{route('admin.team.photos', $teamData['key'])}}" style="float: right;">Robot Photos</a>
           <br>

             <a href="{{route('admin.teammatches.id', $teamData['key'])}}">Click Here to see all of the matches that this team is playing.</a>
             <br>
             <br>
            @if(!(count($teamMatches) <= 0))
            <p>There is more than one match.</p>
            @foreach($analytics as $group)
                  @if ($group == 'averages')
                        <p>{{$group['name']}} Average</p>
                  @else if ($group == 'totals')
                        <p>{{$group['name']}} Total</p>
                  @endif
            @endforeach
            @else
            <p>Sorry, we dont have any matches for this team for graphs. </p>
            @endif

            <?php
            //TODO fix analytics page
            // <h3 id="overall"><u>Overall</U></h3>
            //   <br>
            //   <div class="card">
            //    <div class="card-header">
            //       Robot Overall
            //    </div>
            //    <div class="card-block">
            //       <center>
            //          <canvas id="canvas" width=500 height=500 style="margin:10px"></canvas><br>
            //          <p style="display:inline-block" ><a class="text-success">Green:</a> Avg - sd</p><br>
            //          <p style="display:inline-block"><a class="text-warning">Yellow:</a> Avg</p><br>
            //          <p style="display:inline-block"><a class="text-danger">Red:</a> Avg + sd</p>
            //       </center>
            //    </div>
            //   </div>
            //   <br>
            //   <div class="card-deck">
            //    <div class="card text-center">
            //     <div class="card-block">
            //        <h4 class="card-title">Total Matches Scouted</h4>
            //        <hr>
            //        <p class="card-text">{{App\MatchData::where('team_id', $teamData['key'])->count()}}</p>
            //     </div>
            //   </div>
            //    =<div class="card text-center">
            //      <div class="card-block">
            //        <h4 class="card-title">Total Gears Scored</h4>
            //        <hr>
            //        <p class="card-text">{{App\MatchData::where('team_id', $teamData['key'])->sum('gears_success_auton') + App\MatchData::where('team_id', $teamData['key'])->sum('gears_success_teleop')}}</p>
            //      </div>
            //    </div>
            //    <div class="card text-center">
            //      <div class="card-block">
            //        <h4 class="card-title">Total High Goals Scored</h4>
            //        <hr>
            //        <p class="card-text">{{App\MatchData::where('team_id', $teamData['key'])->sum('high_goal_shots_auton') + App\MatchData::where('team_id', $teamData['key'])->sum('high_goal_shots_teleop')}}</p>
            //      </div>
            //    </div>
            //    <div class="card text-center">
            //      <div class="card-block">
            //        <h4 class="card-title">Total Time Spent Climbing</h4>
            //        <hr>
            //        <p class="card-text">{{App\MatchData::where('team_id', $teamData['key'])->sum('hang_time')}}s</p>
            //      </div>
            //    </div>
            //  </div>
            //  <br>
            // <h3><u>Gears</U></h3>
            // <div class="card">
            //    <div class="card-header">
            //       Total Gears Success
            //    </div>
            //    <div class="card-block">
            //      <center>
            //       <div id="total-gear-chart" class="chart"></div>
            //     </center>
            //    </div>
            // </div>
            // <br>
            // <div class="card">
            //    <div class="card-header">
            //       Auton Gears Success
            //    </div>
            //    <div class="card-block">
            //      <center>
            //       <div id="auto-gear-chart" class="chart"></div>
            //     </center>
            //    </div>
            // </div>
            // <br>
            // <div class="card">
            //    <div class="card-header">
            //       Teleop Gears Success
            //    </div>
            //    <div class="card-block">
            //      <center>
            //       <div id="teleop-gear-chart" class="chart"></div>
            //     </center>
            //    </div>
            // </div>
            // <h3><u>Balls</U></h3>
            // <div class="card">
            //    <div class="card-header">
            //       Auton High Goal Success
            //    </div>
            //    <div class="card-block">
            //      <center>
            //       <div id="auto-high-goal-chart" class="chart"></div>
            //     </center>
            //    </div>
            // </div>
            // <br>
            // <div class="card">
            //    <div class="card-header">
            //       Teleop High Goal Success
            //    </div>
            //    <div class="card-block">
            //      <center>
            //       <div id="teleop-high-goal-chart" class="chart"></div>
            //     </center>
            //    </div>
            // </div>
            // <h3><u>Hang</U></h3>
            // <div class="card">
            //    <div class="card-header">
            //       Hang Time
            //    </div>
            //    <div class="card-block">
            //      <center>
            //       <div id="hang-time-chart" class="chart"></div>
            //     </center>
            //    </div>
            //    </div>
            //    <h3><u>Auton Starting Position Stats</U></h3>
            //    <div class="card">
            //       <div class="card-header">
            //          Starting Position Accuracy
            //       </div>
            //       <div class="card-block">
            //         <center>
            //          <div id="starting-pos-chart" class="chart"></div>
            //        </center>
            //       </div>
            //    </div>
            //    <br>
            //    <div class="card">
            //       <div class="card-header">
            //          Hang Position Accuracy
            //       </div>
            //       <div class="card-block">
            //         <center>
            //          <div id="hang-pos-chart" class="chart"></div>
            //        </center>
            //       </div>
            //    </div>
               ?>


            
            <?php
//             <br>
//             <h3 id="rawstatistics"><u>Raw statistics</u></h3>
//             <br>
//             <style>
//             .borderless td, .borderless th {
//                 border: none;
//             }

//             </style>
//             <div class="card">
//               <div class="card-header">
//                  Raw Statistics
//               </div>
//               <div class="card-block">
//                 <p>*Values in brackets are the standard deviation.</p>
//               <div class="card-deck">
//                 <div class="card text-center">
//                   <div class="card-block">
//                     <i class="fa fa-bullseye fa-5x" aria-hidden="true"></i>
//                     <h4>Accuracy Tings</h4>
//                     <hr>
//                     <center>
//                     <table class="borderless">
//                       <tbody>
//                         <tr>
//                           <td>Gear Accuracy Auton</td>
//                           <td>{{number_format($teamAnalytics->gear_accuracy_auton_avg * 100, 2, '.', '')}}%</td>
//                         </tr>
//                         <tr>
//                           <td>Gear Accuracy Teleop</td>
//                           <td>{{number_format($teamAnalytics->gear_accuracy_teleop_avg * 100, 2, '.', '')}}%</td>
//                         </tr>
//                       </tbody>
//                     </table>
//                   </center>
//                   </div>
//                 </div>

//                 <div class="card text-center">
//                   <div class="card-block">
//                     <i class="fa fa-cog fa-5x" aria-hidden="true"></i>
//                     <h4>Gear Tings</h4>
//                     <hr>
//                     <center>
//                     <table class="borderless">
//                       <tbody>
//                         <tr>
//                           <td>Average Gears Success Auton</td>
//                           <td>{{number_format($teamAnalytics->gears_success_auton_avg, 2, '.', '')}} ({{number_format($teamAnalytics->gears_success_auton_sd, 2, '.', '')}})</td>
//                         </tr>
//                         <tr>
//                           <td>Average Gears Attempted Auton</td>
//                           <td>{{number_format($teamAnalytics->gears_attempted_auton_avg, 2, '.', '')}} ({{number_format($teamAnalytics->gears_attempted_auton_sd, 2, '.', '')}})</td>
//                         </tr>
//                         <tr>
//                           <td>Average Gears Success Teleop</td>
//                           <td>{{number_format($teamAnalytics->gears_success_teleop_avg, 2, '.', '')}} ({{number_format($teamAnalytics->gears_success_teleop_sd, 2, '.', '')}})</td>
//                         </tr>
//                         <tr>
//                           <td>Average Gears Attempted Teleop</td>
//                           <td>{{number_format($teamAnalytics->gears_attempted_teleop_avg, 2, '.', '')}} ({{number_format($teamAnalytics->gears_attempted_teleop_sd, 2, '.', '')}})</td>
//                         </tr>

//                       </tbody>
//                     </table>
//                   </center>
//                   </div>
//                 </div>

//                 <div class="card text-center">
//                   <div class="card-block">
//                     <i class="fa fa-futbol-o fa-5x" aria-hidden="true"></i>
//                     <h4>Ball Tings</h4>
//                     <hr>
//                     <center>
//                     <table class="borderless">
//                       <tbody>
//                         <tr>
//                           <td>Average Balls Success Auton</td>
//                           <td>{{number_format($teamAnalytics->high_goal_shots_auton_avg, 2, '.', '')}} ({{number_format($teamAnalytics->high_goal_shots_auton_sd, 2, '.', '')}})</td>
//                         </tr>
//                         <tr>
//                           <td>Average Balls Attempted Teleop</td>
//                           <td>{{number_format($teamAnalytics->high_goal_shots_teleop_avg, 2, '.', '')}} ({{number_format($teamAnalytics->high_goal_shots_teleop_sd, 2, '.', '')}})</td>
//                         </tr>

//                       </tbody>
//                     </table>
//                   </center>
//                   </div>
//                 </div>

//                 <div class="card text-center">
//                   <div class="card-block">
//                     <img height="80" src="https://maxcdn.icons8.com/Share/icon/Healthcare//suicide_risk1600.png"></img>
//                     <h4>Rope Tings</h4>
//                     <hr>
//                     <center>
//                     <table class="borderless">
//                       <tbody>
//                         <tr>
//                           <td>Hang "Accuracy"</td>
//                           <td>{{number_format($teamAnalytics->hang_avg * 100, 2, '.', '')}}%</td>
//                         </tr>
//                         <tr>
//                           <td>Average Hang Time</td>
//                           <td>{{number_format($teamAnalytics->hang_time_avg, 2, '.', '')}}s ({{number_format($teamAnalytics->hang_time_sd, 2, '.', '')}}s)</td>
//                         </tr>

//                       </tbody>
//                     </table>
//                   </center>
//                   </div>
//                 </div>

//               </div>
//               </div>
//             </div>
//             <br>
//             <h3 id="matchcomments"><u>Match Comments</u></h3>
//             <br>
//             <table class="table table-responsive">
//                <thead>
//                   <tr>
//                      <th>Match #</th>
//                      <th>Comments</th>
//                      <th>Scout</th>
//                   </tr>
//                </thead>
//                <tbody>
//                   @foreach($teamMatches as $match)
//                   <tr>
//                      <td>{{substr($match->match_id, 2)}}</td>
//                      <td>{{$match->comments}}</td>
//                      <td>{{$match->scout_email}}</td>
//                   </tr>
//                   @endforeach
//                </tbody>
//             </table>
//             <br>
//             <h3 id="matchdata"><u>Match Data</u></h3>
//             <br>
//             <button type="button" class="btn btn-outline-success" onclick="disableZeros()">Disable Zeros</button>
//             <br>
//             <br>
//             <table id="stats-table" class="stat-table">
//                <tr>
//                   <th>Data</th>
//                   <th>Auton Gears</th>
//                   <th>Auton High Goals</th>
//                   <th>Teleop Gears</th>
//                   <th>Teleop High Goals</th>
//                   <th>Hang</th>
//                   <th>Raw Link</th>
//                </tr>
//                <tr id="averages">
//                   <th>Average</th>
//                   <th>{{number_format($teamAnalytics->gears_success_auton_avg, 2, '.', '')}}</th>
//                   <th>{{number_format($teamAnalytics->high_goal_shots_auton_avg, 2, '.', '')}}</th>
//                   <th>{{number_format($teamAnalytics->gears_success_teleop_avg, 2, '.', '')}}</th>
//                   <th>{{number_format($teamAnalytics->high_goal_shots_teleop_avg, 2, '.', '')}}</th>
//                   <th>{{number_format($teamAnalytics->hang_time_avg, 2, '.', '')}}</th>
//                </tr>
//                <tr class="data-row">
//                   <th>Max</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                </tr>
//                <tr class="data-row">
//                   <th>Min</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                   <th>Your code sucks</th>
//                </tr>
//                @foreach($teamMatches as $match)
//                <tr class="data-row">
//                   <th onclick="showTeams" class="left-column">Match {{ substr($match->match_id, 2) }}</th>
//                   <td>{{$match->gears_success_auton}}</td>
//                   <td>{{$match->high_goal_shots_auton}}</td>
//                   <td>{{$match->gears_success_teleop}}</td>
//                   <td>{{$match->high_goal_shots_teleop}}</td>
//                   <td>{{$match->hang_time}}</td>
//                   <td><a href="{{route('admin.matchdata', $match->id)}}">Raw</a></td>
//                </tr>
//                @endforeach
//             </table>

//          </div>
//       </div>
//    </div>
// </div>
// </div>
// </div>


// <!-----------------------------------------------Graphs Script---------------------------------->


// <script type="text/javascript">
//    google.charts.load('current', {
//        'packages': ['corechart']
//    });
//    google.charts.setOnLoadCallback(drawVisualization);

//    function drawVisualization() {

//      //Start Auton Gears************************************************************************************************
//      var data = google.visualization.arrayToDataTable([
//    ['Match', '', { role: 'style' }, ''],
//    @foreach($teamMatches as $match)
//    ['{{substr($match->match_id, 2)}}', {{$match->gears_success_auton}}, 'stroke-color: #000000; stroke-width: 2; fill-color: #FFFF00', {{$teamAnalytics->gears_success_auton_avg}}],
//    @endforeach
//    ]);

//    var options = {
//    legend: 'left',
//    title: null,
//    vAxis: {
//        title: 'Gears'
//    },
//    hAxis: {
//        title: 'Match'
//    },
//    chartArea: {
//        left: 70,
//        right: 10, // !!! works !!!
//        bottom: 50, // !!! works !!!
//        top: 20,
//        width: "100%",
//        height: "100%"
//    },
//    seriesType: 'bars',
//    series: {
//        1: {
//            type: 'line'
//        }
//    }
//    };

//    var chart = new google.visualization.ComboChart(document.getElementById('auto-gear-chart'));
//    chart.draw(data, options);
//      //End Auton Gears************************************************************************************************
//      //Start of total Gears
//      var data = google.visualization.arrayToDataTable([
//    ['Match', '', { role: 'style' }],
//    @foreach($teamMatches as $match)
//    ['{{substr($match->match_id, 2)}}', {{$match->gears_success_auton + $match->gears_success_teleop}},  'stroke-color: #000000; stroke-width: 2; fill-color: #FFFF00'],
//    @endforeach
//    ]);

//    var options = {
//    legend: 'left',
//    title: null,
//    vAxis: {
//        title: 'Gears'
//    },
//    hAxis: {
//        title: 'Match'
//    },
//    chartArea: {
//        left: 70,
//        right: 10, // !!! works !!!
//        bottom: 50, // !!! works !!!
//        top: 20,
//        width: "100%",
//        height: "100%"
//    },
//    seriesType: 'bars',
//    series: {
//        1: {
//            type: 'line'
//        }
//    }
//    };

//    var chart = new google.visualization.ComboChart(document.getElementById('total-gear-chart'));
//    chart.draw(data, options);
//      //end of total gears
//      //start of Auton High Goal Success
//      var data = google.visualization.arrayToDataTable([
//      ['Match', '', { role: 'style' }, ''],

//      @foreach($teamMatches as $match)
//      ['{{substr($match->match_id, 2)}}', {{$match->high_goal_shots_auton}}, 'stroke-color: #000000; stroke-width: 2; fill-color: #2EFE2E', {{$teamAnalytics->high_goal_shots_auton_avg}}],
//      @endforeach
//      ]);

//      var options = {
//      legend: 'left',
//      title: null,
//      vAxis: {
//        title: 'Balls'
//      },
//      hAxis: {
//        title: 'Match'
//      },
//      chartArea: {
//        left: 70,
//        right: 10, // !!! works !!!
//        bottom: 50, // !!! works !!!
//        top: 20,
//        width: "100%",
//        height: "100%"
//      },
//      seriesType: 'bars',
//      series: {
//        1: {
//            type: 'line'
//        }
//      }
//      };

//      var chart = new google.visualization.ComboChart(document.getElementById('auto-high-goal-chart'));
//      chart.draw(data, options);

//      //end of Auton High Goal Success
//      //start of teleop high goals
//      var data = google.visualization.arrayToDataTable([
//      ['Match', '', { role: 'style' }, ''],

//      @foreach($teamMatches as $match)
//      ['{{substr($match->match_id, 2)}}', {{$match->high_goal_shots_teleop}}, 'stroke-color: #000000; stroke-width: 2; fill-color: #2EFE2E', {{$teamAnalytics->high_goal_shots_teleop_avg}}],
//      @endforeach
//      ]);

//      var options = {
//      legend: 'left',
//      title: null,
//      vAxis: {
//        title: 'Balls'
//      },
//      hAxis: {
//        title: 'Match'
//      },
//      chartArea: {
//        left: 70,
//        right: 10, // !!! works !!!
//        bottom: 50, // !!! works !!!
//        top: 20,
//        width: "100%",
//        height: "100%"
//      },
//      seriesType: 'bars',
//      series: {
//        1: {
//            type: 'line'
//        }
//      }
//      };

//      var chart = new google.visualization.ComboChart(document.getElementById('teleop-high-goal-chart'));
//      chart.draw(data, options);
//      //end of teleop high goals
//      //start of hang time
//      var data = google.visualization.arrayToDataTable([
//      ['Match', '', ''],

//      @foreach($teamMatches as $match)
//      ['{{substr($match->match_id, 2)}}', {{$match->hang_time}} , {{$teamAnalytics->hang_time_avg}}],
//      @endforeach
//      ]);

//      var options = {
//      legend: 'left',
//      title: null,
//      vAxis: {
//        title: 'Seconds'
//      },
//      hAxis: {
//        title: 'Match'
//      },
//      chartArea: {
//        left: 70,
//        right: 10, // !!! works !!!
//        bottom: 50, // !!! works !!!
//        top: 20,
//        width: "100%",
//        height: "100%"
//      },
//      seriesType: 'bars',
//      series: {
//        1: {
//            type: 'line'
//        }
//      }
//      };

//      var chart = new google.visualization.ComboChart(document.getElementById('hang-time-chart'));
//      chart.draw(data, options);
//      //end of hang time

//      var data = google.visualization.arrayToDataTable([
//    ['Match', '', { role: 'style' }, ''],
//    @foreach($teamMatches as $match)
//    ['{{substr($match->match_id, 2)}}', {{$match->gears_success_teleop}}, 'stroke-color: #000000; stroke-width: 2; fill-color: #FFFF00', {{$teamAnalytics->gears_success_teleop_avg}}],
//    @endforeach
//    ]);

//    var options = {
//    legend: 'left',
//    title: null,
//    vAxis: {
//        title: 'Gears'
//    },
//    hAxis: {
//        title: 'Match'
//    },
//    chartArea: {
//        left: 70,
//        right: 10, // !!! works !!!
//        bottom: 50, // !!! works !!!
//        top: 20,
//        width: "100%",
//        height: "100%"
//    },
//    seriesType: 'bars',
//    series: {
//        1: {
//            type: 'line'
//        }
//    }
//    };

//    var chart = new google.visualization.ComboChart(document.getElementById('teleop-gear-chart'));
//    chart.draw(data, options);



//    var data = google.visualization.arrayToDataTable([
//    ['Match', '', { role: 'style' }],
//    ['Human Player', {{$teamAnalytics->human_player_success_rate * 100}}, 'fill-color: #B71C1C'],
//    ['Center', {{$teamAnalytics->center_success_rate * 100}}, 'fill-color: #0D47A1'],
//    ['Boiler', {{$teamAnalytics->boiler_success_rate * 100}}, 'fill-color: #1B5E20'],
//    ['Other', {{$teamAnalytics->other_success_rate * 100}}, 'fill-color: #F57F17'],
//    ]);

//    var options = {
//    legend: 'left',
//    title: null,
//    vAxis: {
//      title: 'Accuracy(%)'
//    },
//    hAxis: {
//      title: 'Position'
//    },
//    chartArea: {
//      left: 70,
//      right: 10, // !!! works !!!
//      bottom: 50, // !!! works !!!
//      top: 20,
//      width: "100%",
//      height: "100%"
//    },
//    seriesType: 'bars',
//    series: {
//      1: {
//          type: 'line'
//      }
//    }
//    };

//    var chart = new google.visualization.ComboChart(document.getElementById('starting-pos-chart'));
//    chart.draw(data, options);


//    var data = google.visualization.arrayToDataTable([
//    ['Match', '', { role: 'style' }],
//    ['Human Player', {{$teamAnalytics->human_player_success_rate_hang * 100}}, 'fill-color: #B71C1C'],
//    ['Center', {{$teamAnalytics->center_success_rate_hang * 100}}, 'fill-color: #0D47A1'],
//    ['Boiler', {{$teamAnalytics->boiler_success_rate_hang * 100}}, 'fill-color: #1B5E20'],
//    ['Did Not Hang', {{$teamAnalytics->dnh_success_rate_hang * 100}}, 'fill-color: #F57F17'],
//    ]);

//    var options = {
//    legend: 'left',
//    title: null,
//    vAxis: {
//      title: 'Accuracy(%)'
//    },
//    hAxis: {
//      title: 'Position'
//    },
//    chartArea: {
//      left: 70,
//      right: 10, // !!! works !!!
//      bottom: 50, // !!! works !!!
//      top: 20,
//      width: "100%",
//      height: "100%"
//    },
//    seriesType: 'bars',
//    series: {
//      1: {
//          type: 'line'
//      }
//    }
//    };

//    var chart = new google.visualization.ComboChart(document.getElementById('hang-pos-chart'));
//    chart.draw(data, options);



//      //Start of pentagon chart
//     var canvas=document.getElementById("canvas");
//     var cxt=canvas.getContext("2d");


//     // pentagon
//     var radius = (canvas.clientHeight - 70)/2;
//     var cx = canvas.clientHeight/2, cy = canvas.clientHeight/2;
//     //rank for each category here
//     var hang = {{$pentagonChart['hang']}}, tGears = {{$pentagonChart['tGears']}}, tFuel = {{$pentagonChart['tFuel']}}, aGears = {{$pentagonChart['aGears']}}, aFuel = {{$pentagonChart['aFuel']}};
//     //points for stats
//     var points = [
//     {x:cx, y:cy - (hang/5)*radius},
//     {x:cx + Math.cos(0.3142) * radius * (tGears/5), y:cy - Math.sin(0.3142) * radius * (tGears/5)},
//     {x:cx + Math.sin(0.6283) * radius * (aGears/5), y:cy + Math.cos(0.6283) * radius * (aGears/5)},
//     {x:cx - Math.sin(0.6283) * radius * (tFuel/5), y:cy + Math.cos(0.6283) * radius * (tFuel/5)},
//     {x:cx - Math.cos(0.3142) * radius * (aFuel/5), y:cy - Math.sin(0.3142) * radius * (aFuel/5)},
//     {x:cx, y:cy - (hang/5)*radius}
//     ];

//     console.log(hang);
//     console.log(tGears);
//     console.log(aGears);
//     console.log(tFuel);
//     console.log(aFuel);
//     console.log("----");

//     //rank with average + standard deviation
//     hang = {{$pentagonChart['hang_add']}};
//     tGears = {{$pentagonChart['tGears_add']}};
//     tFuel = {{$pentagonChart['tFuel_add']}};
//     aGears = {{$pentagonChart['aGears_add']}};
//     aFuel = {{$pentagonChart['aFuel_add']}};

//     console.log(hang);
//     console.log(tGears);
//     console.log(aGears);
//     console.log(tFuel);
//     console.log(aFuel);
//     console.log("----");

//     var tempPoints = [
//     {x:cx, y:cy - (hang/5)*radius},
//     {x:cx + Math.cos(0.3142) * radius * (tGears/5), y:cy - Math.sin(0.3142) * radius * (tGears/5)},
//     {x:cx + Math.sin(0.6283) * radius * (aGears/5), y:cy + Math.cos(0.6283) * radius * (aGears/5)},
//     {x:cx - Math.sin(0.6283) * radius * (tFuel/5), y:cy + Math.cos(0.6283) * radius * (tFuel/5)},
//     {x:cx - Math.cos(0.3142) * radius * (aFuel/5), y:cy - Math.sin(0.3142) * radius * (aFuel/5)},
//     {x:cx, y:cy - (hang/5)*radius}
//     ];
//     cxt.beginPath();
//     for (var i = 0; i < points.length;i++) {
//       cxt.lineTo (tempPoints[i].x, tempPoints[i].y);
//     }

//     //avg + sd colour
//     cxt.fillStyle = "#D32F2F"
//     cxt.globalAlpha = 1;
//     cxt.fill();
//     cxt.globalAlpha = 0.65;
//     cxt.stroke();
//     cxt.closePath();

//     //max pentagon
//     var points2 = [
//     {x:cx, y:cy - radius},
//     {x:cx + Math.cos(0.3142) * radius, y:cy - Math.sin(0.3142) * radius},
//     {x:cx + Math.sin(0.6283) * radius, y:cy + Math.cos(0.6283) * radius},
//     {x:cx - Math.sin(0.6283) * radius, y:cy + Math.cos(0.6283) * radius},
//     {x:cx - Math.cos(0.3142) * radius, y:cy - Math.sin(0.3142) * radius},
//     {x:cx, y:cy - radius}
//     ];

//     cxt.beginPath();

//     for (var i = 0; i < points.length;i++) {
//       cxt.lineTo (points2[i].x, points2[i].y);
//         // ctx.fillStyle = "green";
//     }
//     cxt.fillStyle = "black";
//     cxt.globalAlpha = 1;
//     cxt.fillText("Hang", points2[0].x - 10, points2[0].y-3);
//     cxt.fillText("T-Gears", points2[1].x, points2[1].y);
//     cxt.fillText("A-Gears", points2[2].x, points2[2].y + 10);
//     cxt.fillText("T-Fuel", points2[3].x - 20, points2[3].y + 10);
//     cxt.fillText("A-Fuel", points2[4].x - 32, points2[4].y);

//     cxt.strokeStyle = "#000000";
//     cxt.lineWidth = 1;
//     cxt.stroke();
//     cxt.closePath();

//    cxt.beginPath();

//     for (var i = 0; i < points.length;i++) {
//       cxt.lineTo (points[i].x, points[i].y);
//     }

//     cxt.closePath();

//     //average colour
//     cxt.fillStyle = "#FFEB3B";
//     // cxt.globalAlpha = 0.75;
//     cxt.lineWidth = 1;
//     cxt.globalAlpha = 1;
//     cxt.fill();
//     cxt.globalAlpha = 0.65;
//     cxt.stroke();
//     cxt.fillStyle = "black";
//     for (var i = 0; i < points.length;i++) {
//       cxt.fillRect(points[i].x-2, points[i].y-2, 4, 4);
//     }

//     //rank with average - standard deviation
//     hang = {{$pentagonChart['hang_sub']}};
//     tGears = {{$pentagonChart['tGears_sub']}};
//     tFuel = {{$pentagonChart['tFuel_sub']}};
//     aGears = {{$pentagonChart['aGears_sub']}};
//     aFuel = {{$pentagonChart['aFuel_sub']}};

//     console.log(hang);
//     console.log(tGears);
//     console.log(aGears);
//     console.log(tFuel);
//     console.log(aFuel);
//     console.log("----");

//     tempPoints = [
//     {x:cx, y:cy - (hang/5)*radius},
//     {x:cx + Math.cos(0.3142) * radius * (tGears/5), y:cy - Math.sin(0.3142) * radius * (tGears/5)},
//     {x:cx + Math.sin(0.6283) * radius * (aGears/5), y:cy + Math.cos(0.6283) * radius * (aGears/5)},
//     {x:cx - Math.sin(0.6283) * radius * (tFuel/5), y:cy + Math.cos(0.6283) * radius * (tFuel/5)},
//     {x:cx - Math.cos(0.3142) * radius * (aFuel/5), y:cy - Math.sin(0.3142) * radius * (aFuel/5)},
//     {x:cx, y:cy - (hang/5)*radius}
//     ];

//     cxt.beginPath();

//     //avg - sd colour
//     cxt.fillStyle = "#43A047"
//     for (var i = 0; i < points.length;i++) {
//       cxt.lineTo (tempPoints[i].x, tempPoints[i].y);
//     }
//     // cxt.globalAlpha = 0.4;
//     cxt.globalAlpha = 1;
//     cxt.fill();
//     cxt.globalAlpha = 0.65;
//     cxt.stroke();
//     cxt.closePath();

//     //pentagon rings
//     for (var i = 1; i <= 4; i++) {
//         var tempPoints = [
//         {x:cx, y:cy - (i/5)*radius},
//         {x:cx + Math.cos(0.3142) * radius * (i/5), y:cy - Math.sin(0.3142) * radius * (i/5)},
//         {x:cx + Math.sin(0.6283) * radius * (i/5), y:cy + Math.cos(0.6283) * radius * (i/5)},
//         {x:cx - Math.sin(0.6283) * radius * (i/5), y:cy + Math.cos(0.6283) * radius * (i/5)},
//         {x:cx - Math.cos(0.3142) * radius * (i/5), y:cy - Math.sin(0.3142) * radius * (i/5)},
//         {x:cx, y:cy - (i/5)*radius}
//         ];
//         cxt.beginPath();

//         for (var j = 0; j < points.length;j++) {
//           cxt.lineTo (tempPoints[j].x, tempPoints[j].y);
//         }
//         cxt.strokeStyle = "#000000";
//         cxt.globalAlpha = 0.3;
//         cxt.lineWidth = 1;
//         cxt.stroke();
//         cxt.closePath();
//     }
//      //end


//    }
// </script>






// <!-----------------------------------------------Table Script---------------------------------->



// <script>
//    function showTeams() {

//    }

//    function disableZeros() {
//     //    console.log("Clicked");
//        var rows = document.getElementById('stats-table').rows;
//        var numRows = rows.length;
//        for (var i = 4; i < numRows; i++) {
//            for (var j = 1; j < 6; j++) {
//                if(rows[i].cells[j].innerHTML === '0' && !rows[i].cells[j].classList.contains('disabled')) {
//                    rows[i].cells[j].classList = 'disabled';
//                 //    console.log(rows[i].cells[j] + " is now disabled");
//                }
//                else {
//                 //    console.log(rows[i].cells[j] + " is either not 0 or already disabled");
//                }
//            }
//        }
//        calcAvg();
//        getMinMax();
//    }

//    var index = 0;

//    $('.stat-table td').click(function(){
//        index = this.cellIndex;
//    })
//    $('.stat-table th').click(function(){
//        index = 0;
//    })

//    //toggle disabled
//    $('.stat-table tr').click(function(){
//        var rIndex = this.rowIndex
//        var td = document.getElementById('stats-table').rows[rIndex].cells[index];
//        var row = document.getElementById('stats-table').rows[rIndex].cells;
//        if(rIndex > 3) {
//            if(index === 0) {
//                var good = true;
//                for (var i = 1; i < row.length; i++) {
//                    if(row[i].classList.contains('disabled')) {
//                        good = false;
//                        break;
//                    }
//                }
//                console.log(good);
//                if (good) {
//                    for (var i = 1; i < row.length; i++) {
//                        row[i].classList = 'disabled';
//                    }
//                }
//                else {
//                    for (var i = 1; i < row.length; i++) {
//                        row[i].classList = '';
//                    }
//                }
//            }
//            else {
//                if(td.classList.contains('disabled')) {
//                    td.classList = '';
//                }
//                else {
//                    td.classList = 'disabled';
//                }
//            }
//        }
//        calcAvg();
//        getMinMax();
//    })

//    window.onload = function loadStuff() {
//        getMinMax();
//        colorScale();
//    }

//    function colorScale() {
//        var avgs = document.getElementById('averages').cells;
//        var rows = document.getElementsByClassName('data-row');
//        var min = rows[1].cells;
//        var max = rows[0].cells;
//        for (var i = 2; i < rows.length; i++) {
//            for (var j = 1; j < rows[i].cells.length - 1; j++) {
//                var r, g, b, data;
//                var percent = (rows[i].cells[j].innerHTML - min[j].innerHTML) / (max[j].innerHTML - min[j].innerHTML);
//                if (percent <= 0.5) {
//                    r = 255;
//                    g = Math.trunc((percent / 0.5) * 255);
//                } else {
//                    r = Math.trunc((1 - (percent - 0.5) / 0.5) * 255);
//                    g = 255;
//                }
//                b = 0;
//                if (rows[i].cells[j].innerHTML > avgs[j].innerHTML) {

//                }
//                rows[i].cells[j].style.backgroundColor = 'rgba(' + r + ',' + g + ',' + b + ',' + 0.4 + ')';
//            }
//        }
//    }

//    function getMinMax() {
//        var min = [99, 99, 99, 99, 99, 99];
//        var max = [0, 0, 0, 0, 0, 0];
//        var rows = document.getElementsByClassName('data-row');
//        for (var i = 2; i < rows.length; i++) {
//            for (var j = 1; j < rows[i].cells.length; j++) {
//                if (!rows[i].cells[j].classList.contains('disabled')) {
//                    max[j] = Math.max(max[j], rows[i].cells[j].innerHTML);
//                    min[j] = Math.min(min[j], rows[i].cells[j].innerHTML);
//                }
//            }
//        }
//        for (var i = 1; i < max.length - 1; i++) {
//            rows[0].cells[i].innerHTML = max[i];
//            rows[1].cells[i].innerHTML = min[i];
//        }
//    }

//    function calcAvg() {
//        var avg = [0, 0, 0, 0, 0, 0];
//        var count = [0, 0, 0, 0, 0, 0];
//        var rows = document.getElementsByClassName('data-row');
//        var avgs = document.getElementById('averages').cells;
//        for (var i = 2; i < rows.length; i++) {
//            for (var j = 1; j < rows[i].cells.length - 1; j++) {
//                if (!rows[i].cells[j].classList.contains('disabled')) {
//                    avg[j] += parseInt(rows[i].cells[j].innerHTML);
//                    count[j]++;
//                }
//            }
//        }
//        for (var i = 1; i < avg.length; i++) {
//            avg[i] = (avg[i] / count[i]).toFixed(2);
//            avgs[i].innerHTML = avg[i];
//        }
//    }
// </script>


?>




@stop
