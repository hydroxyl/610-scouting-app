@extends('layouts/main')
@section('content')
@if(isset($error))
<p>{{$error}}</p>
@else
<h5>Please enter each team followed by a comma and a space IE "610, 1114".</h5>
<hr>
<form action="{{ route('admin.expertscoutingsettings.callback') }}" method="post">
  @foreach($users as $user)

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">{{$user->display_name}}</label>
    <div class="col-10">
      <input class="form-control" type="text" value="@if(isset($data->{$user->id})) {{implode(", ", $data->{$user->id})}} @endif" name="id.{{$user->id}}">
    </div>
  </div>

  @endforeach
  <input class="btn btn-outline-success" style="float: right;" type="submit" id="submitBtn" value="Save">
</form>
@endif
@stop
