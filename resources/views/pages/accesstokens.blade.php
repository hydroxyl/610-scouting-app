@extends('layouts/main')
@section('content')
<a href="{{route('admin.accesstokens.new')}}">
<input class="btn btn-outline-success" style="float: right;" value="New">
<br><br>
</a>
<table class="table table-hover">
   <thead>
      <tr>
         <th>ID</th>
         <th>First Name</th>
         <th>Last Name</th>
         <th>Role</th>
         <th>Token</th>
         <th>Creator (User ID)</th>
         <th>Expiry</th>
         <th>Delete</th>
      </tr>
   </thead>
   <tbody>
      @foreach($tokens as $token)
      @if(\Carbon\Carbon::now()->lt(\Carbon\Carbon::parse($token->expiry)))
      <tr>
         <th scope="row">{{$token->id}}</th>
         <td>{{$token->first_name}}</td>
         <td>{{$token->last_name}}</td>
         <td>{{$token->role}}</td>
         <td>{{$token->token}}</td>
         <td>{{$token->google_id}}</td>
         <td>{{$token->expiry}}</td>
         <td><a href="{{route('admin.accesstokens.delete', $token->id)}}">Delete</a></td>
      </tr>
      @endif
      @endforeach
   </tbody>
</table>
@stop
