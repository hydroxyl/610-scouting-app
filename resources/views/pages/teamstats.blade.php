@extends('layouts/main')
@section('content')
<style>
	robotIMG {
		width:  auto;
		height: auto;
	}
	img {
		image-orientation: from-image;
	}
</style>
<!--link rel="stylesheet" href="{{ url('css/team.css') }}"-->
<?php /*
<h3>Expert Scout: <mark id="main">{{$exp}}</mark></h3> */ ?>

<h2><a style="color: black" href="{{route ('admin.teamtest', substr_replace($title, '', 0, 5))}}"><strong>{{$title}}</strong></a></h2>
<p>{{count($matchList)}} matches found.</p>
@if(count($matchList) > 0)
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {packages: ['corechart']});
		google.charts.setOnLoadCallback(drawChart);

		function drawChart() {
			<?php /*Run foreach chart loop, call all charts' draw function*/ ?>
			@foreach($displayGroups as $group)
			@if($group['type']=='charts')
			@foreach($group['charts'] as $chart)
				draw{{$chart['id']}}();
			@endforeach
			@endif
			@endforeach
		}

		window.onresize = function() {
			drawChart();
		}
	</script>
	<div>
		<?php
			/*foreach($displayGroups as $group) {
				$id = $group['id'];
				$label = $group['label'];
				echo "<a href='#$id'>$label</a><br>";
			}*/
		?>
	</div>
	@foreach($displayGroups as $group)
	<div class="card" id="$group['id']">
		<div class="card-header">
			{{$group['label']}}
		</div>
	@if($group['type'] == 'overallTable')
		<div class="card-block">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Exchange</th>
					<th>Home Switch</th>
					<th>Away Switch</th>
					<th>Scale</th>
					<th>Hang</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php
						if($analytics['exchange_acc']!=-1) {
							$acc = round($analytics['exchange_acc'],4)*100;
							$time = round($analytics['avg_exchange_time'],2);
							echo "<td>$acc% @ $time s</td>";
						} else {
							echo "<td>N/A</td>";
						}

						if($analytics['home_switch_acc']!=-1) {
							$acc = round($analytics['home_switch_acc'],4)*100;
							$time = round($analytics['avg_home_switch_time'],2);
							echo "<td>$acc% @ $time s</td>";
						} else {
							echo "<td>N/A</td>";
						}

						if($analytics['away_switch_acc']!=-1) {
							$acc = round($analytics['away_switch_acc'],4)*100;
							$time = round($analytics['avg_away_switch_time'],2);
							echo "<td>$acc% @ $time s</td>";
						} else {
							echo "<td>N/A</td>";
						}

						if($analytics['scale_acc']!=-1) {
							$acc = round($analytics['scale_acc'],4)*100;
							$time = round($analytics['avg_scale_time'],2);
							echo "<td>$acc% @ $time s</td>";
						} else {
							echo "<td>N/A</td>";
						}
					?>
					<td>{{round(($analytics['num_center_rung_hangs']+$analytics['num_carrier_hangs']+$analytics['num_assissted_hangs'])/count($matchList),4)*100}}% @ {{round($analytics['active_hang_avg'],2)}} s</td>
				</tr>
			</tbody>
		</table>
		</div>
	@elseif($group['type'] == 'charts')
		<div class="card-block">
		@foreach($group['charts'] as $chart)
			<script type="text/javascript">
				<?php $info = json_decode($analytics[$chart['id']], true); ?>
				function draw{{$chart['id']}}() {
					@if($chart['type']=='perMatch' || $chart['type'] == 'timeHisto')
					var {{$chart['id'].'Data'}} = new google.visualization.DataTable();
					{{$chart['id'].'Data'}}.addColumn('string', 'Match Number');
					{{$chart['id'].'Data'}}.addColumn('number', 'value');
					{{$chart['id'].'Data'}}.addRows([
					@for($i=1; $i<count($info); $i++)
					@if($chart['type'] == 'perMatch')
						['{{$info[$i]["matchNum"]}}', {{$info[$i]['hits']}}],
					@elseif($chart['type'] == 'timeHisto')
						['{{$i-1}}', {{$info[$i][$i-1]}}],
					@endif
					@endfor
					]);
					@elseif($chart['type'] == 'hangPerMatch')
					var {{$chart['id'].'Data'}} = new google.visualization.arrayToDataTable([
						['Match Number', 'Centre Rung', 'Assissted'],
						@for($i=1; $i<count($info); $i++)
							@if($info[$i]['hangPos']=='Center Rung'||$info[$i]['hangPos']=='Side')
							['{{$info[$i]["matchNum"]}}',{{$info[$i]["hangTime"]}},0],
							@elseif($info[$i]['hangPos']=='Carried/Assisted')
							['{{$info[$i]["matchNum"]}}',0,{{$info[$i]["hangTime"]}}],
							@else
							['{{$info[$i]["matchNum"]}}',0,0],
							@endif
						@endfor
					]);
					@endif
					var {{$chart['id']}}Options = {
						// backgroundColor: '#202020',
						@if($chart['type']=='perMatch'||$chart['type']=='timeHisto')legend: { position: "none"},
						@elseif($chart['type']=='hangPerMatch')legend: {textStyle:{/*color:'#CCC'*/}},@endif
						title: '{{$chart["title"]}}',
						titleTextStyle: {
							// color:'#CCC'
						},
						hAxis: {
							title: '{{$chart["hAxis"]["title"]}}',
							titleTextStyle: {
								// color:'#CCC'
							},
							textStyle: {
								// color:'#CCC'
							}@if($chart['type']=='timeHisto'),
							viewWindow: {
								min: 0,
								max: 30
							}
							@endif
						},
						vAxis: {
							title: '{{$chart["vAxis"]["title"]}}',
							format: 'short',
							viewWindow: {
								min: 0
							},
							titleTextStyle: {
								// color:'#CCC'
							},
							gridlines: {
								// color: '#CCC'
							},
							textStyle: {
								// color:'#CCC'
							}
						}@if($chart['type']=='hangPerMatch'),
						isStacked: true
						@endif
					};

					var {{$chart['id'].'Chart'}} = new google.visualization.ColumnChart(document.getElementById('{{$chart["id"]."ChartArea"}}'));
					{{$chart['id'].'Chart'}}.draw({{$chart['id'].'Data'}}, {{$chart['id'].'Options'}});
				}
			</script>
			<div id='{{$chart["id"]."ChartArea"}}'></div>
		@endforeach
		</div>
	@elseif($group['type'] == 'numberTable')
		<div class="card-block">
		<table class="table table-hover table-responsive">
			<tbody>
			@foreach($group['fields'] as $field)
				<tr>
					<td>{{$field['label']}}</td>
					<td>{{number_format($analytics[$field['id']], 3)}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		</div>
    @elseif($group['type'] == 'heatmap')
    	<div class="card-block" id="auton-heatmap">
    	<?php $gridNumRows=5; $gridNumCols=5; $info=json_decode($analytics[$group['fields'][0]['id']],true); ?>
			<style>
				#cropped-field-diagram {
					display: grid;
					grid-template-columns: <?php for($i=0; $i<$gridNumCols; $i++){echo (100/$gridNumCols).'% ';} ?>;
					grid-template-rows: <?php for($i=0; $i<$gridNumRows; $i++){echo (100/$gridNumRows).'% ';} ?>;
					margin:auto;
					background-image: url('../img/inverse-cropped-field-diagram.png');
					background-size: 100% 100%;
				}
			</style>
			<script type="text/javascript">
				$(document).ready(function() {
					var aWidth = 0.5*$('#auton-heatmap').width();
					document.getElementById("cropped-field-diagram").style="width:"+aWidth+"; height:"+aWidth;
				});
				$(window).resize(function() {
					var aWidth = 0.5*$('#auton-heatmap').width();
					document.getElementById("cropped-field-diagram").style="width:"+aWidth+"; height:"+aWidth;
				});
			</script>
			<div id="cropped-field-diagram">
				@for($row=1; $row<=$gridNumRows; $row++)
					@for($col=1; $col<=$gridNumCols; $col++)
						<?php
							$count=0;
							for($i=1; $i<count($info); $i++) {
								if($info[$i]['row'] == $row && $info[$i]['col'] == $col) {
									$count = $info[$i]['count'];
								}
							}
						?>
						<!--div id="grid_r{{$row}}c{{$col}}" style="border-style: dotted; border-width: thin; background-color: rgba(255,0,0,{{$count/count($matchList)}})"></div-->
						@if($count!=0)
						<div id="grid_r{{$row}}c{{$col}}" style="border-style: dotted; border-width: thin; background-color: rgba({{round(195*$count/count($matchList)+60)}},0,0,0.8)"></div>
						@else
						<div id="grid_r{{$row}}c{{$col}}" style="border-style: dotted; border-width: thin; background-color: rgba(0,0,0,0)"></div>
						@endif
					@endfor
				@endfor
			</div>
    	</div>
		@elseif($group['type']=="comments")
		@foreach($group['fields'] as $field)
			<div class="card-block">
				<table class="table table-hover table-responsive">
					<thead>
						<tr>
							<th>Match #</th>
							<!--th>Scout Name</th-->
							<th>Comments</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$info = json_decode($analytics[$field['id']], true);
						#<td>".$info[$i]['scout']."</td>
						for($i=1; $i<count($info); $i++) {
							echo "<tr><td>".$info[$i]['matchNum']."</td><td>".$info[$i]['comments']."</td></tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		@endforeach
    @endif
    </div>
    <br>
	@endforeach
	</script>
@else
	<p>No matches found</p>
@endif

@stop
