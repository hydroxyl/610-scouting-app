@extends('layouts/main')
@section('content')
<script>
	<?php
		//TODO: save for timeout case
		/*On view loading, check if matches have already been scouted
		 *If so, then check if the current match is the same as the previous matches
		 *If not, ask the user if they would like to clear the previous data
		 */
	?>
	window.onload = function() {
		var matchNumValue = localStorage.getItem("preMatchNum");
		var curMatchNum = document.getElementById("match_num").value;

		if (matchNumValue) {
			console.log("There have been matches scouted before.");
			if (matchNumValue != curMatchNum) {
				console.log("Different match from last time.");
			} else {
				console.log("Loading information from last match.");
				loadData(curMatchNum);
			}
		} else {
			console.log("No matches have been scouted so far, the values will be set to default.");
			localStorage.setItem("preMatchNum", matchNumValue);
		}
	}

	<?php /*Load the data for the match **matchNum** onto all fields*/ ?>
	function loadData(matchNum) {
		<?php /*TODO: add stuff for teamNum*/ ?>
		var matchNumValue = localStorage.getItem("preMatchNum");
		localStorage.setItem("preMatchNum", matchNumValue);
		@foreach($fields['groups'] as $section)
		@foreach($section['fields'] as $field)
			@if($field['inputType'] == "numberButton"||$field['inputType'] == "numberType"||$field['inputType'] == "text")
				var inputValue = localStorage.getItem(matchNum+".{{$field['id']}}");
				if (inputValue) {
					console.log("Loading item {{$field['id']}}");
					document.getElementById("{{$field['id']}}").value = inputValue;
				}
			@elseif($field['inputType'] == "radio")
				var radioList = document.getElementsByName("{{$field['id']}}");
				var radioValue = localStorage.getItem(matchNum + ".{{$field['id']}}");
				for (var i=0; i<radioList.length; i++) {
					if (radioList[i].value == radioValue) {
						radioList[i].checked = true;
						console.log("Loading radio {{$field['id']}}");
					}
				}
			@endif
		@endforeach
		@endforeach
	}

	<?php /*On unload, save everything*/ ?>
	window.onbeforeunload = function() {
		var matchNumValue = document.getElementById("match_num").value;
		@foreach($fields['groups'] as $section)
		@foreach($section['fields'] as $field)
			@if($field['inputType']=="numberButton"||$field['inputType']=="numberType"||$field['inputType']=="text")
				console.log("Saving item {{$field['id']}}");
				var inputValue = document.getElementById("{{$field['id']}}").value;
				localStorage.setItem(matchNumValue + ".{{$field['id']}}", inputValue);
			@elseif($field['inputType'] == "radio")
				console.log("Saving radio {{$field['id']}}");
				var radioList = document.getElementsByName("{{$field['id']}}");
				var radioValue;
				for (var i=0; i<radioList.length; i++) {
					if (radioList[i].checked) {
						radioValue = radioList[i].value;
					}
				}
				localStorage.setItem(matchNumValue+".{{$field['id']}}", radioValue);
			@endif
		@endforeach
		@endforeach
	}

	<?php /*Button and character count code*/ ?>
	@foreach($fields['groups'] as $section)
	@foreach($section['fields'] as $field)
		@if($field['inputType'] == 'numberButton')
			function inc_{{$field['id']}}(x) {
				var curVal = parseInt(document.getElementById("{{$field['id']}}").value);
				curVal+=x;
				if (curVal > {{$field['maxValue']}}) {
					curVal = {{$field['maxValue']}};
				} else if (curVal < {{$field['minValue']}}) {
					curVal = {{$field['minValue']}};
				}
				document.getElementById("{{$field['id']}}").value = curVal;
			}
		@elseif($field['inputType'] == 'text')
			function countChars{{$field['id']}} () {
				var charCount = document.getElementById("{{$field['id']}}").value.length;
				if (charCount >= {{$field['maxLength']}}) {
					document.getElementById("char_count_{{$field['id']}}").style = "color:red";
					// window.alert("You have hit the limit: "+charCount);
				} else if (charCount >= {{$field['maxLength']}}*9/10) {
					document.getElementById("char_count_{{$field['id']}}").style = "color:#F4FA58";
				} else {
					document.getElementById("char_count_{{$field['id']}}").style = "color:#202020";
				}
				document.getElementById("char_count_{{$field['id']}}").innerHTML = 999-charCount;
			}
		@endif
	@endforeach
	@endforeach

	<?php /*"Clear" local storage*/ ?>
	function clearStorage() {
		var a = -1;
		localStorage.setItem("preMatchNum", a);
	}
</script>

<form action="{{ route('submitForm') }}" method="post" onsubmit="" id="form">
<input type="hidden" name="match_num" id="match_num" value="{{$matchNum}}">
	<!--***********************Team Select***********************-->
	<div class="card">
		<div class="card-header">
			Team Select **Please do this first!**
		</div>
		<div class="card-block">
			<select name="team_num" id="team_num" required>
	          <option class="btn-group-vertical" value="{{substr($red1, 3)}}">Red 1: {{substr($red1, 3)}}</option>
	          <option class="btn-group-vertical" value="{{substr($red2, 3)}}">Red 2: {{substr($red2, 3)}}</option>
	          <option class="btn-group-vertical" value="{{substr($red3, 3)}}">Red 3: {{substr($red3, 3)}}</option>
	          <option class="btn-group-vertical" value="{{substr($blue1, 3)}}">Blue 1: {{substr($blue1, 3)}}</option>
	          <option class="btn-group-vertical" value="{{substr($blue2, 3)}}">Blue 2: {{substr($blue2, 3)}}</option>
	          <option class="btn-group-vertical" value="{{substr($blue3, 3)}}">Blue 3: {{substr($blue3, 3)}}</option>
	       </select>
		</div>
	</div>
	<br>

	@foreach($fields['groups'] as $section)
	<div class="card">
		<div class="card-header">
			{{$section['group_label']}}
		</div>
		<div class="card-block">
		@foreach($section['fields'] as $field)
			<hr>
			<p>{{$field['label']}}</p>
			@if($field['inputType'] == 'numberButton')
			<?php /*TODO: Define a new input type beyond numberButton (i.e. slow, one-at-a-time cycles and fast dumps) */ ?>
				@if($field['maxValue']-$field['minValue'] <= 30)
					<button type="button" class="btn btn-danger" id="minusone_{{$field['id']}}" onclick="inc_{{$field['id']}}(-1)">-1</button>
					<input type="number" name="{{$field['id']}}" id="{{$field['id']}}" value="{{$field['defaultValue']}}" min="{{$field['minValue']}}" max="{{$field['maxValue']}}" readonly>
					<button type="button" class="btn btn-success" id="plusone_{{$field['id']}}" onclick="inc_{{$field['id']}}(1)">+1</button>
					<br>
				@else
					<button type="button" class="btn btn-danger" id="minusten_{{$field['id']}}" onclick="inc_{{$field['id']}}(-10)">-10</button>
					<button type="button" class="btn btn-danger" id="minusfive_{{$field['id']}}" onclick="inc_{{$field['id']}}(-5)">-5</button>
					<button type="button" class="btn btn-danger" id="minusone_{{$field['id']}}" onclick="inc_{{$field['id']}}(-1)">-1</button>
					<input type="number" name="{{$field['id']}}" id="{{$field['id']}}" value="{{$field['defaultValue']}}" min="{{$field['minValue']}}" max="{{$field['maxValue']}}" readonly>
					<button type="button" class="btn btn-success" id="plusone_{{$field['id']}}" onclick="inc_{{$field['id']}}(1)">+1</button>
					<button type="button" class="btn btn-success" id="plusfive_{{$field['id']}}" onclick="inc_{{$field['id']}}(5)">+5</button>
					<button type="button" class="btn btn-success" id="plusten_{{$field['id']}}" onclick="inc_{{$field['id']}}(10)">+10</button>
					<br>
				@endif
			@elseif($field['inputType'] == 'numberType')
				<input type="number" name="{{$field['id']}}" id="{{$field['id']}}" value="{{$field['defaultValue']}}" min="{{$field['minValue']}}" max="{{$field['maxValue']}}"><br>
			@elseif($field['inputType'] == 'radio')
				<fieldset id="{{$field['id']}}">
				@foreach ($field['choices'] as $choice)
					<label><input type="radio" value="{{$choice}}" name="{{$field['id']}}"> {{$choice}}</label><br>
				@endforeach
				</fieldset><br>
			@elseif($field['inputType'] == 'text')
				<textarea name="{{$field['id']}}" id="{{$field['id']}}" rows="{{$field['rows']}}" cols="{{$field['cols']}}" maxlength="{{$field['maxLength']}}" placeholder="{{$field['placeholder']}}" oninput="countChars{{$field['id']}}()"></textarea><br>
				Characters remaining: <span id="char_count_{{$field['id']}}">{{$field['maxLength']}}</span><br>
			@elseif($field['inputType'] == 'cycleTimes')
				<!--button class="my_popup_open">Open cycle times</button-->
				<div id="my_popup">
					@yield('tap-tap')
					<!--button class="my_popup_close">Close</button-->
					<script src="https://cdn.rawgit.com/vast-engineering/jquery-popup-overlay/1.7.13/jquery.popupoverlay.js"></script>
					<!--script>
					    $(document).ready(function() {
						    // Initialize the plugin
							$('#my_popup').popup();
						});
					</script-->
				</div>
			@elseif($field['inputType'] == 'timer')
				<script>
					var startTime;
					var endTime;
					var hangTimer;
					var hangStatus=0;

					function startHangTimer() {
						if(hangStatus==0) {
							var startDate = new Date();
							startTime = startDate.getTime();
							hangTimer = window.setInterval(function() {
								var d = new Date();
								var curTime = d.getTime();
								var duration = (Math.round((curTime-startTime)/100)/10).toFixed(1);
								document.getElementById('hangTimer').innerHTML = "<b>"+duration+" seconds</b>";
							}, 100);
							hangStatus=1;
						}
					}

					function endHangTimer() {
						if(hangStatus==1) {
							var endDate = new Date();
							endTime = endDate.getTime();
							window.clearInterval(hangTimer);
							document.getElementById("t_active_hang").value=(endTime-startTime)/1000;
							hangStatus=2;
						}
					}
				</script>
				<input type="number" id="t_active_hang" name="t_active_hang" value=0 step="0.001" readonly>&nbsp;&nbsp;<span id="hangTimer"><b>0.0 seconds</b></span><br><br>
	    		<button class="big-button btn btn-success" type="button" onclick="startHangTimer()">Start</button>&nbsp;&nbsp;
	    		<button class="big-button btn btn-danger" type="button" onclick="endHangTimer()">End</button>
			@elseif($field['inputType'] == 'autonPathing')
				<?php $gridNumRows=5; $gridNumCols=10; ?>
				<style>
					#field-diagram {
						display: grid;
						grid-template-columns: <?php for($i=0; $i<$gridNumCols; $i++){echo (100/$gridNumCols).'% ';} ?>;
						grid-template-rows: <?php for($i=0; $i<$gridNumRows; $i++){echo (100/$gridNumRows).'% ';} ?>;
						margin:auto;
						background-image: url('img/LayoutandMarkingDiagram.png');
						background-size: 100% 100%;
					}
				</style>
				<div class="field-grid-taptap">
					<div id="field-diagram">
						@for($row=1; $row<=$gridNumRows; $row++)
							@for($col=1; $col<=$gridNumCols; $col++)
								<div id="grid_r{{$row}}c{{$col}}" onclick="gridClicked({{$row}}, {{$col}})" style="border-style: dotted; border-width: thin; opacity: 0.5"></div>
							@endfor
						@endfor
					</div>
				</div>
				<textarea name="a_auton_path" id="a_auton_path" style="font-family:monospace;" wrap="soft" hidden readonly>[{}]</textarea>
				
				<script type="text/javascript">
					var gridState = [];
					var gridClickCount = 0;
					for(var row=0; row< <?php echo $gridNumRows ?>; row++) {
						var arrRow = [];
						for(var col=0; col< <?php echo $gridNumCols ?>; col++) {
							arrRow.push(0);
						}
						gridState.push(arrRow);
					}
					function gridClicked(row, col) {
						if(gridState[row-1][col-1] == 0) {
							document.getElementById("grid_r"+row+"c"+col).style="border-style: dotted; border-width: thin; background-color: rgba(255,0,0,0.5);";
							gridState[row-1][col-1] = 1;
							gridClickCount++;
							document.getElementById("grid_r"+row+"c"+col).innerHTML="<div style='text-align:center;'>"+gridClickCount+"</div>";
							var text = document.getElementById("a_auton_path").innerHTML;
							document.getElementById("a_auton_path").innerHTML = text.substring(0, text.length-1)+',{"tapNum":'+gridClickCount+',"row":'+row+',"col":'+col+',"keep":"true"'+'}]';
						} else if (gridState[row-1][col-1] == 2) {
							document.getElementById("grid_r"+row+"c"+col).style="border-style: dotted; border-width: thin; background-color: rgba(255,0,0,0.5);";
							gridState[row-1][col-1] = 1;
							if(document.getElementById("grid_r"+row+"c"+col).innerHTML=="") {
								gridClickCount++;
								document.getElementById("grid_r"+row+"c"+col).innerHTML="<div style='text-align:center;'>"+gridClickCount+"</div>";
							}

							var aPathString = document.getElementById("a_auton_path").innerHTML;
							var aPathJSON = JSON.parse(aPathString);
							for (var i=1; i<aPathJSON.length; i++) {
								if(aPathJSON[i].row==row&&aPathJSON[i].col==col) {
									aPathJSON[i].keep="true";
									aPathJSON[i].tapNum=gridClickCount;
								}
							}
							document.getElementById("a_auton_path").innerHTML = JSON.stringify(aPathJSON);
						} else {
							document.getElementById("grid_r"+row+"c"+col).style="border-style: dotted; border-width: thin; background-color: rgba(211, 211, 211, 0.5)";
							gridState[row-1][col-1] = 2;
							document.getElementById("grid_r"+row+"c"+col).innerHTML="";
							var aPathString = document.getElementById("a_auton_path").innerHTML;
							var aPathJSON = JSON.parse(aPathString);
							for (var i=1; i<aPathJSON.length; i++) {
								if(aPathJSON[i].row==row&&aPathJSON[i].col==col) {
									aPathJSON[i].keep="false";
								}
							}
							document.getElementById("a_auton_path").innerHTML = JSON.stringify(aPathJSON);
						}
					}

					$( document ).ready( function() {
						var aWidth = 0.9*$( ".field-grid-taptap" ).width();
						document.getElementById("field-diagram").style="width:"+aWidth+"; height:"+(aWidth/2);
						aWidth = 0.9*$("#my_popup").width();
						document.getElementsByClassName("cycle-times-popup")[0].style="width:"+aWidth+"; height:"+(aWidth/2);
					});

					$( window ).resize( function() {
						var aWidth = 0.9*$( ".field-grid-taptap" ).width();
						document.getElementById("field-diagram").style="width:"+aWidth+"; height:"+(aWidth/2);
						aWidth = 0.9*$("#my_popup").width();
						document.getElementsByClassName("cycle-times-popup")[0].style="width:"+aWidth+"; height:"+(aWidth/2);
					});
					<?php /*TODO: Figure out how to split the two field diagram's resizing*/ ?>
				</script>
			@elseif($field['inputType'] == 'autonScoring')
				<script>
					function minusCubeAuton(element) {
						var curVal = parseInt(document.getElementById(element).innerHTML);
						if(curVal>0) {
							curVal--;
						}
						document.getElementById(element).innerHTML = curVal;
						var curJSON = JSON.parse(document.getElementById("{{$field['id']}}").innerHTML);
						curJSON[element] = curVal;
						document.getElementById("{{$field['id']}}").innerHTML = JSON.stringify(curJSON);
					}
					function plusCubeAuton(element) {
						var curVal = parseInt(document.getElementById(element).innerHTML);
						if(curVal<3) {
							curVal++;
						}
						document.getElementById(element).innerHTML = curVal;
						var curJSON = JSON.parse(document.getElementById("{{$field['id']}}").innerHTML);
						curJSON[element] = curVal;
						document.getElementById("{{$field['id']}}").innerHTML = JSON.stringify(curJSON);
					}
				</script>
				<style>
					.unselectable {
						-webkit-touch-callout: none;
						-webkit-user-select: none;
						-khtml-user-select: none;
						-moz-user-select: none;
						-ms-user-select: none;
						user-select: none;
					}
				</style>
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm">
							<p style="text-align: center;">XCHNG</p>
							<div style="text-align: center;">
								<button class="small-button btn btn-danger" onclick="plusCubeAuton('xch_miss');" type="button">&#10005;</button>
								<button class="small-button btn btn-success" onclick="plusCubeAuton('xch_hit');" type="button">&#10003;</button>
							</div>
							<div style="text-align: center;">
								<span class="unselectable" onclick="minusCubeAuton('xch_miss');" style="color:red; font-size: 30;">&#9660;</span>
								<span id="xch_miss" style="color:red; font-weight: bold">0</span>:<span id="xch_hit" style="color:green; font-weight: bold">0</span>
								<span class="unselectable" onclick="minusCubeAuton('xch_hit');" style="color:green; font-size: 30;">&#9660;</span>
							</div>
						</div>
						<div class="col-sm">
							<p style="text-align: center;">SWITCH</p>
							<div style="text-align: center;">
								<button class="small-button btn btn-danger" onclick="plusCubeAuton('hsw_miss');" type="button">&#10005;</button>
								<button class="small-button btn btn-success" onclick="plusCubeAuton('hsw_hit');" type="button">&#10003;</button>
							</div>
							<div style="text-align: center;">
								<span class="unselectable" onclick="minusCubeAuton('hsw_miss');" style="color:red; font-size: 30;">&#9660;</span>
								<span id="hsw_miss" style="color:red; font-weight: bold">0</span>:<span id="hsw_hit" style="color:green; font-weight: bold">0</span>
								<span class="unselectable" onclick="minusCubeAuton('hsw_hit');" style="color:green; font-size: 30;">&#9660;</span>
							</div>
						</div>
						<div class="col-sm">
							<p style="text-align: center;">SCALE</p>
							<div style="text-align: center;">
								<button class="small-button btn btn-danger" onclick="plusCubeAuton('scl_miss');" type="button">&#10005;</button>
								<button class="small-button btn btn-success" onclick="plusCubeAuton('scl_hit');" type="button">&#10003;</button>
							</div>
							<div style="text-align: center;">
								<span class="unselectable" onclick="minusCubeAuton('scl_miss');" style="color:red; font-size: 30;">&#9660;</span>
								<span id="scl_miss" style="color:red; font-weight: bold">0</span>:<span id="scl_hit" style="color:green; font-weight: bold">0</span>
								<span class="unselectable" onclick="minusCubeAuton('scl_hit');" style="color:green; font-size: 30;">&#9660;</span>
							</div>
						</div>
						<textarea id="{{$field['id']}}" name="{{$field['id']}}" hidden>{"hsw_miss":0,"hsw_hit":0,"scl_miss":0,"scl_hit":0,"xch_miss":0,"xch_hit":0}</textarea>
					</div>
				</div>
			@endif
			@if($field['notes']!="")
				<br>{{$field['notes']}}
			@endif
		@endforeach
		</div>
	</div>
	<br>
	@endforeach
	<script>
    window.setInterval(function(){
      checkform();
    }, 1000);
    function checkform()
    {
    	var filledOut = true;
    	var radioList;
    	var radioValue;
    	var radioValid = false;
    	@foreach($fields['groups'] as $section)
    	@foreach($section['fields'] as $field)
    	if (filledOut == true) {
	    	@if($field['inputType'] == 'radio')
	    		radioList = document.getElementsByName("{{$field['id']}}");
				for(var i=0; i<radioList.length; i++) {
					if(radioList[i].checked) {
						radioValid = true;
					}
				}
				if (radioValid == false) {
					filledOut = false;
				} else {
					radioValid = false;
				}
			@elseif($field['inputType'] == 'numberType')
    			var {{$field['id']}}Value = document.getElementById("{{$field['id']}}").value;
	    		if({{$field['id']}}Value == "undefined" || {{$field['id']}}Value == "" || {{$field['id']}}Value < {{$field['minValue']}} || {{$field['id']}}Value > {{$field['maxValue']}}) {
	    			filledOut = false;
	    		}
	    	@elseif($field['inputType'] == 'text')
	    		var {{$field['id']}}Value = document.getElementById("{{$field['id']}}").value;
	    		if({{$field['id']}}Value == "undefined" || {{$field['id']}}Value == "") {
	    			filledOut = false;
	    		}
	    	@endif
    	}
    	@endforeach
    	@endforeach

    	if(filledOut) {
    		document.getElementById("submitBtn").disabled=false;
    	} else {
    		document.getElementById("submitBtn").disabled=true;
    	}
    }
    </script>
	<input class="btn btn-success" style="float: right;" type="submit" id="submitBtn" onClick="clearStorage(); this.form.submit(); this.disabled=true; this.value='Sending…'; " value="Done">
</form>
@stop