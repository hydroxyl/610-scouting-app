@extends('layouts/main')
@section('content')
<h4 class="card-title">Match List</h4>
@if(count($matches) > 0)
<table class="table table-hover">
   <thead>
      <tr>
         <th>Match Number</th>
         <th>Red 1</th>
         <th>Red 2</th>
         <th>Red 3</th>
         <th>Blue 1</th>
         <th>Blue 2</th>
         <th>Blue 3</th>
      </tr>
   </thead>
   <tbody>
      @foreach($matches as $key => $match)
      <tr>
         <td><a href="{{route('admin.match', $match->id)}}">{{$match->match_number}}</a></td>
         <td>{{substr($match->red1, 3)}}</td>
         <td>{{substr($match->red2, 3)}}</td>
         <td>{{substr($match->red3, 3)}}</td>
         <td>{{substr($match->blue1, 3)}}</td>
         <td>{{substr($match->blue2, 3)}}</td>
         <td>{{substr($match->blue3, 3)}}</td>
      </tr>
      @endforeach
   </tbody>
</table>
@else
There is no match data in our database :(
@endif
@stop
