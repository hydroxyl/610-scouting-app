@extends('layouts/main')
@section('content')
<script src="{{url('js/buttons.js')}}"></script>
<script src="{{url('js/local-save.js')}}"></script>
<form action="{{ route('submit') }}" method="post" onsubmit="" id="form">
<input type="hidden" name="match_num" id="match_num" value="{{$matchNum}}">
  <!--*************************************Team Select************************************************-->
  <div class="card">
    <div class="card-header">
      Team Select **Please do this first!**
    </div>
    <div class="card-block">
      <select name="team_num" id="team_num" required>
          <option class="btn-group-vertical" value="{{substr($matchData->red1, 3)}}">Red 1: {{substr($matchData->red1, 3)}}</option>
          <option class="btn-group-vertical" value="{{substr($matchData->red2, 3)}}">Red 2: {{substr($matchData->red2, 3)}}</option>
          <option class="btn-group-vertical" value="{{substr($matchData->red3, 3)}}">Red 3: {{substr($matchData->red3, 3)}}</option>
          <option class="btn-group-vertical" value="{{substr($matchData->blue1, 3)}}">Blue 1: {{substr($matchData->blue1, 3)}}</option>
          <option class="btn-group-vertical" value="{{substr($matchData->blue2, 3)}}">Blue 2: {{substr($matchData->blue2, 3)}}</option>
          <option class="btn-group-vertical" value="{{substr($matchData->blue3, 3)}}">Blue 3: {{substr($matchData->blue3, 3)}}</option>
       </select>
    </div>
 </div>
<br>
  <!--*************************************Auton************************************************-->
<div class="card">
  <div class="card-header">
    Autonomous Period
  </div>
  <div class="card-block">
    <p>Auton starting position:</p>
    <fieldset id="starting_pos_auton">
        <label><input required type="radio" value="center" name="starting_pos_auton"> Center</label><br>
        <label><input required type="radio" value="human" name="starting_pos_auton"> Human Player</label><br>
        <label><input required type="radio" value="boiler" name="starting_pos_auton"> Boiler</label><br>
        <label><input required type="radio" value="other" name="starting_pos_auton"> Other</label><br>
    </fieldset>
    <hr>
    <p>Gears Attempted</p>
     <button type="button" class="btn btn-danger" name="minusone_gears_attempted_auton" onclick="gears_attempted_auton_inc(-1)">-1</button>
     <input type="number" name="gears_attempted_auton" id="gears_attempted_auton" value="0" min="0" max="5" readonly>
     <button type="button" class="btn btn-success" name="plusone_gears_attempted_auton" onclick="gears_attempted_auton_inc(1)">+1</button>
     <hr>
     <p>Gears Scored</p>
     <button type="button" class="btn btn-danger" name="minusone_gears_attempted_auton" onclick="gears_success_auton_inc(-1)">-1</button>
     <input type="number" name="gears_success_auton" id="gears_success_auton" value="0" min="0" max="5" readonly>
     <button type="button" class="btn btn-success" name="plusone_gears_attempted_auton" onclick="gears_success_auton_inc(1)">+1</button>
     <hr>
     <p>High Goals Scored</p>
     Tip: if there is only one robot shooting, look at the pressure after the autonomous period. <br>
      <button type="button" class="btn btn-danger" name="minusten_highgoal_auton" onclick="high_goal_shots_auton_inc(-10)">-10</button>
      <button type="button" class="btn btn-danger" name="minusfive_highgoal_auton" onclick="high_goal_shots_auton_inc(-5)">-5</button>
      <button type="button" class="btn btn-danger" name="minusone_highgoal_auton" onclick="high_goal_shots_auton_inc(-1)">-1</button>
      <input type="number" name="high_goal_shots_auton" id="high_goal_shots_auton" value="0" min="0" max="200" readonly>
      <button type="button" class="btn btn-success" name="plusone_highgoal_auton" onclick="high_goal_shots_auton_inc(1)">+1</button>
      <button type="button" class="btn btn-success" name="plusfive_highgoal_auton" onclick="high_goal_shots_auton_inc(5)">+5</button>
      <button type="button" class="btn btn-success" name="plusten_highgoal_auton" onclick="high_goal_shots_auton_inc(10)">+10</button>
      <hr>
      <p>Was the low goal used?</P>
        <fieldset id="low_goal_auton">
          <label><input required type="radio" value="1" name="low_goal_auton"> Yes</label><br>
          <label><input required type="radio" value="0" name="low_goal_auton" > No</label><br>
        </fieldset>
  </div>
</div>
<br>
  <!--*************************************Teleop************************************************-->
<div class="card">
  <div class="card-header">
    Teleoperated Period
  </div>
  <div class="card-block">
    <p>Gears Attempted</p>
    <button type="button" class="btn btn-danger" name="minusone_gears_attempted_teleop" onclick="gears_attempted_teleop_inc(-1)">-1</button>
    <input type="number" name="gears_attempted_teleop" id="gears_attempted_teleop" value="0" min="0" max="30" readonly>
    <button type="button" class="btn btn-success" name="plusone_gears_attempted_teleop" onclick="gears_attempted_teleop_inc(1)">+1</button>
    <hr>
    <p>Gears Scored</p>
    <button type="button" class="btn btn-danger" name="minusone_gears_attempted_teleop" onclick="gears_success_teleop_inc(-1)">-1</button>
    <input type="number" name="gears_success_teleop" id="gears_success_teleop" value="0" min="0" max="30" readonly>
    <button type="button" class="btn btn-success" name="plusone_gears_attempted_teleop" onclick="gears_success_teleop_inc(1)">+1</button>
    <hr>
    <p>High Goals Scored</p>
    Tip: if there is only one robot shooting, look at the amount of pressure at the end of the match, subtract the pressure at the end of auton, and multiply by three.<br>
    <button type="button" class="btn btn-danger" name="minusten_highgoal_auton" onclick="high_goal_shots_teleop_inc(-10)">-10</button>
    <button type="button" class="btn btn-danger" name="minusfive_highgoal_auton" onclick="high_goal_shots_teleop_inc(-5)">-5</button>
    <button type="button" class="btn btn-danger" name="minusone_highgoal_auton" onclick="high_goal_shots_teleop_inc(-1)">-1</button>
    <input type="number" name="high_goal_shots_teleop" id="high_goal_shots_teleop" value="0" min="0" max="200" readonly>
    <button type="button" class="btn btn-success" name="plusone_highgoal_auton" onclick="high_goal_shots_teleop_inc(1)">+1</button>
    <button type="button" class="btn btn-success" name="plusfive_highgoal_auton" onclick="high_goal_shots_teleop_inc(5)">+5</button>
    <button type="button" class="btn btn-success" name="plusten_highgoal_auton" onclick="high_goal_shots_teleop_inc(10)">+10</button>
    <hr>
    <p>Ready for take off (Hang)</p>
    <fieldset id="hang">
      <label><input required type="radio" value="1" name="hang"> Yes</label><br>
      <label><input required type="radio" value="0" name="hang" > No</label><br>
    </fieldset>
    <hr>
    <p>Hanging Position</p>
    <fieldset id="hang_pos">
      <label><input required type="radio" value="human" name="hang_pos"> Human Player</label><br>
      <label><input required type="radio" value="center" name="hang_pos"> Center</label><br>
      <label><input required type="radio" value="Boiler" name="hang_pos"> Boiler</label><br>
      <label><input required type="radio" value="dnh" name="hang_pos" > Did Not Hang</label><br>
    </fieldset>
    <hr>
    <p>Climb Time (Not including line up!)</p>
    Tip: Remember what time they started to climb and what time they completed the climb and subtract the two.<br>
    <input type="number" name="hang_time" id="hang_time" value="0" min="0" max="30" required> seconds
    <hr>
    <p>Did the robot play defense?</p>
    <fieldset id="played_defense">
      <label><input required type="radio" value="1" name="played_defense"> Yes</label><br>
      <label><input required type="radio" value="0" name="played_defense" > No</label><br>
    </fieldset>
  </div>
</div>
<br>
<!--*************************************Post************************************************-->
<div class="card">
  <div class="card-header">
    Post Game Analysis
  </div>

  <div class="card-block">
    <p>Did this robot contribute <b>most</b> of the pressure for the alliance?</p>
    <fieldset id="pressure">
      <label><input required type="radio" value="1" name="pressure"> Yes</label><br>
      <label><input required type="radio" value="0" name="pressure" > No</label><br>
    </fieldset>
    <hr>
    <p>Did this robot only shoot?</p>
    <fieldset id="only_shooting">
      <label><input required type="radio" value="1" name="only_shooting"> Yes</label><br>
      <label><input required type="radio" value="0" name="only_shooting" > No</label><br>
    </fieldset>
    <hr>
    <p>Was there substantial defense against this robot?</p>
    <fieldset id="defended">
      <label><input required type="radio" value="1" name="defended"> Yes</label><br>
      <label><input required type="radio" value="0" name="defended" > No</label><br>
    </fieldset>
    <hr>
    <p>Match Comments (These aren’t an option, they are required.)</p>
    <textarea name="comments" rows="8" cols="50" maxlength="999" placeholder="Max character count is 999" id="comment_box" oninput="countChars()" required></textarea><br>
    Characters remaining: <span id="char_count">999</span><br>
    <hr>
    <p>Was this match a replay?</p>
    <fieldset id="replay">
      <label><input required type="radio" value="1" name="replay"> Yes</label><br>
      <label><input type="radio" value="0" name="replay" > No</label><br>
    </fieldset>
    <hr>
    Please check all of the data before submitting because scouts will be held accountable for their data. Consequences for blatantly incorrect data might apply.<br>
    <script>
    window.setInterval(function(){
      checkform();
    }, 200);
    function checkform()
    {
      var filledOut = true;
      if(document.getElementById("team_num").value == "undefined" || document.getElementById("team_num").value == "") {
        filledOut = false;
      }
      if (document.getElementById("starting_pos_auton").value == "undefined" || document.getElementById("starting_pos_auton").value == "") {
        filledOut = false;
      }
      if (document.getElementById("low_goal_auton").value == "undefined" || document.getElementById("low_goal_auton").value == "") {
        filledOut = false;
      }
      if (document.getElementById("hang").value == "undefined" || document.getElementById("hang").value == "") {
        filledOut = false;
      }
      if (document.getElementById("hang_pos").value == "undefined" || document.getElementById("hang_pos").value == "") {
        filledOut = false;
      }
      if (document.getElementById("played_defense").value == "undefined" || document.getElementById("played_defense").value == "") {
        filledOut = false;
      }
      if (document.getElementById("pressure").value == "undefined" || document.getElementById("pressure").value == "") {
        filledOut = false;
      }
      if (document.getElementById("only_shooting").value == "undefined" || document.getElementById("only_shooting").value == "") {
        filledOut = false;
      }
      if (document.getElementById("defended").value == "undefined" || document.getElementById("defended").value == "") {
        filledOut = false;
      }
      if (document.getElementById("comment_box").value == "undefined" || document.getElementById("comment_box").value  == "") {
        filledOut = false;
      }
      if (document.getElementById("replay").value == "undefined" || document.getElementById("replay").value == "") {
        filledOut = false;
      }

      if(filledOut) {
        document.getElementById("submitBtn").disabled=false;
      } else {
        document.getElementById("submitBtn").disabled=true;
      }


    }
    </script>
    <input class="btn btn-success" style="float: right;" type="submit" id="submitBtn" onClick="this.form.submit(); this.disabled=true; this.value='Sending…'; " value="Done">

  </div>

</div>

</form>
@stop
