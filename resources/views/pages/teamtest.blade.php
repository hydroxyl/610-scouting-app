<html>

<head>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Team {{$pageTitle}} Page</title>
    <meta charset="UTF-8">
    <link href="{{ url('css/style2.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ url('css/team2.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ url('css/navigation2.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ url('css/data_table.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('css/font-awesome.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- <script src="js/scripts.js"></script> -->
    <script src="{{ url('js/graphs.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script>
    function toggleFixedBar() {
        var elem2 = document.getElementById('side-container');
        var elem = document.getElementById('side-content');
        if(window.pageYOffset > 61) {
            elem.classList = 'fixed-side';
        }
        else if (elem.classList.contains("fixed-side")) {
            elem.classList = "";
        }
    };

    function nextGraph(name) {
        var nums = document.getElementById(name + '-graph-number');
        var str = nums.innerHTML;
        var chartNum;
        if (Number(str.charAt(0)) < Number(str.charAt(2))) {
            chartNum = Number(str.charAt(0)) + 1;
        }
        else {
            chartNum = 1;
        }
        nums.innerHTML = chartNum + str.substring(1,3);
        var graphs = document.getElementsByClassName(name + '-graph');
        for (var i = 0; i < graphs.length; i++) {
            if (i === chartNum - 1) {
                graphs[i].classList = 'charts ' + name + '-graph';
            }
            else {
                graphs[i].classList = 'charts hidden-chart ' + name + '-graph';
            }
        }
    }

    function prevGraph(name) {
        var nums = document.getElementById(name + '-graph-number');
        var str = nums.innerHTML;
        var chartNum;
        if (Number(str.charAt(0)) > 1) {
            chartNum = Number(str.charAt(0)) - 1;
        }
        else {
            chartNum = Number(str.charAt(2));
        }
        nums.innerHTML = chartNum + str.substring(1,3);
        var graphs = document.getElementsByClassName(name + '-graph');
        for (var i = 0; i < graphs.length; i++) {
            if (i === chartNum - 1) {
                graphs[i].classList = 'charts ' + name + '-graph';
            }
            else {
                graphs[i].classList = 'charts hidden-chart ' + name + '-graph';
            }
        }
    }

    function highlightPos() {
        var card = [
            document.getElementById('overall'),
            document.getElementById('auton'),
            document.getElementById('teleop'),
            document.getElementById('switch'),
            document.getElementById('scale'),
            document.getElementById('exchange'),
            document.getElementById('hang'),
            document.getElementById('comments')
        ];
        var max = 0;
        var most = 0;
        for (var i = 0; i < 8; i++) {
            var percent = (Math.min(Math.max(card[i].getBoundingClientRect().bottom, 0), window.innerHeight) -
            Math.min(Math.max(card[i].getBoundingClientRect().top, 0), window.innerHeight)) /
            card[i].offsetHeight;
            if (percent > max) {
                most = i;
                max = percent;
            }
        }
        var links = document.getElementsByClassName('overall-stat-row');
        for (var i = 0; i < 8; i++) {
            if (i === most) {
                links[i].classList = 'overall-stat-row active-row';
            }
            else {
                links[i].classList = 'overall-stat-row'
            }
        }
    };


        window.onscroll = function() {
            toggleFixedBar();
            highlightPos();
        }
    </script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper">
        <div id="nav-bar-main" class="navigation">
            <a id="logo" href="index.html"><img src="{{ url('img/610.png') }}"></a>
            <a id="home" class="nav-bar-link" href="{{route ('index')}}">Home</a>
            <a class="icon" href="javascript:void(0);" onclick="menuDropDown()">&#9776;</a>
        </div>
        <div id="nav-bar-sub" class="navigation">
            <a class="nav-bar-link" id="active-page" href="{{route ('admin.teams')}}">Teams</a>
            <!-- <a class="nav-bar-link" href="#">Alliance Builder</a> -->
            <a class="nav-bar-link" href="{{route ('admin.overall')}}">Overall</a>
            <a class="nav-bar-link" href="{{route ('admin.sidebyside')}}">Side by Side</a>
        </div>
    </div>


@if(count($matchList)>0)
    <div class="main">
        <div class="stats-container">
            <div id="overall" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Overall</span>
                </div>
                <div class="stats">
                    <div class="stat">
                        Auton
                        <img class="stat-icon" src="{{ url('img/auton.png') }}">
                        <span class="over-stat-data">
                            <?php
                                if($analytics['auto_overall_acc'] !=-1) {
                                    $count = round($analytics['auto_overall_avg'], 1);
                                    $acc = round($analytics['auto_overall_acc'], 2)*100;
                                    echo "$count @ $acc%";
                                }
                            ?>
                        </span>
                    </div>
                    <div class="stat">
                        Exchange
                        <img class="stat-icon" src="{{ url('img/xchng.png') }}">
                        <span class="over-stat-data">
                            <?php
                            if($analytics['exchange_acc'] !=-1) {
                                $count = round($analytics['avg_num_exchange'], 2);
                                $time = round($analytics['avg_exchange_time'],2);
                                $acc = round($analytics['exchange_acc'], 2)*100;
                                echo "$count @ $time s @ $acc%";
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </span>
                    </div>
                    <div class="stat">
                        Home Switch
                        <img class="stat-icon" src="{{ url('img/switch.png') }}">
                        <span class="over-stat-data">
                            <?php
                            if($analytics['home_switch_acc'] != -1) {
                                $count = round($analytics['avg_num_home_switch'], 2);
                                $time = round($analytics['avg_home_switch_time'], 2);
                                $acc = round($analytics['home_switch_acc'], 2)*100;
                                echo "$count @ $time s @ $acc%";
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </span>
                    </div>
                    <div class="stat">
                        Away Switch
                        <img class="stat-icon" src="{{ url('img/switch.png') }}">
                        <span class="over-stat-data">
                            <?php
                            if($analytics['away_switch_acc'] != -1) {
                                $count = round($analytics['avg_num_away_switch'], 2);
                                $time = round($analytics['avg_away_switch_time'], 2);
                                $acc = round($analytics['away_switch_acc'], 2)*100;
                                echo "$count @ $time s @ $acc%";
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </span>
                    </div>
                    <div class="stat">
                        Scale
                        <img class="stat-icon" src="{{ url('img/scale.png') }}">
                        <span class="over-stat-data">
                            <?php
                            if($analytics['scale_acc']!=-1) {
    							$count = round($analytics['avg_num_scale'], 2);
    							$time = round($analytics['avg_scale_time'], 2);
                                $acc = round($analytics['scale_acc'], 2)*100;
    							echo "$count @ $time s @ $acc%";
    						} else {
    							echo "N/A";
    						}
                            ?>
                        </span>
                    </div>
                    <div class="stat">
                        Hang
                        <img class="stat-icon" src="{{ url('img/climb.png') }}">
                        <span class="over-stat-data">
                        @if(count($matchList)>0&&$analytics['active_hang_avg']>0)
                        {{round(($analytics['num_center_rung_hangs']+$analytics['num_carrier_hangs']+$analytics['num_assissted_hangs'])/count($matchList),1)*100}}% @ {{round($analytics['active_hang_avg'],2)}} s
                        @else
                        N/A
                        @endif
                        </span>
                    </div>
                </div>
            </div>

            <div id="auton" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Auton</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="nextGraph('auton')"><i class="fa fa-chevron-right"></i></a>
                    <span id="auton-graph-number" class="graph-number">1/2</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="prevGraph('auton')"><i class="fa fa-chevron-left"></i></a>
                </div>
                <div class="stat-content">
                    <div class="stat-numbers">
                        <div class="stat-data">
                            <span class="stat-number">Average Scl Scored</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['auto_scl_scored_avg'], 1) }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Scl Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['auto_scl_acc'], 1) }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average HSw Scored</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['auto_hsw_scored_avg'], 1) }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">HSw Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['auto_hsw_acc'], 1) }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Xch Scored</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['auto_xch_scored_avg'], 1) }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Xch Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['auto_xch_acc'], 1) }}</span>
                        </div>
                    </div>
                    <div class="chart-container">
                        <div id="chart_auton_1" class="charts auton-graph">
                            <?php $gridNumRows=5; $gridNumCols=5; $info=json_decode($analytics['auto_heatmap'],true); ?>
                                <style>
                                    #cropped-field-diagram {
                                        display: grid;
                                        grid-template-columns: <?php for($i=0; $i<$gridNumCols; $i++){echo (100/$gridNumCols).'% ';} ?>;
                                        grid-template-rows: <?php for($i=0; $i<$gridNumRows; $i++){echo (100/$gridNumRows).'% ';} ?>;
                                        margin:auto;
                                        background-image: url('../img/inverse-cropped-field-diagram.png');
                                        background-size: 100% 100%;
                                    }
                                </style>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        var aWidth = $('#chart_auton_1').height() * 1;
                                        document.getElementById("cropped-field-diagram").style="width:"+aWidth+"; height:"+aWidth;
                                    });
                                    $(window).resize(function() {
                                        var aWidth = $('#chart_auton_1').height() * 1;
                                        document.getElementById("cropped-field-diagram").style="width:"+aWidth+"; height:"+aWidth;
                                    });
                                </script>
                                <div id="cropped-field-diagram">
                                    @for($row=1; $row<=$gridNumRows; $row++)
                                        @for($col=1; $col<=$gridNumCols; $col++)
                                            <?php
                                                $count=0;
                                                for($i=1; $i<count($info); $i++) {
                                                    if($info[$i]['row'] == $row && $info[$i]['col'] == $col) {
                                                        $count = $info[$i]['count'];
                                                    }
                                                }
                                            ?>
                                            <!--div id="grid_r{{$row}}c{{$col}}" style="border-style: dotted; border-width: thin; background-color: rgba(255,0,0,{{$count/count($matchList)}})"></div-->
                                            @if($count!=0)
                                            <div id="grid_r{{$row}}c{{$col}}" style="border-style: dotted; border-width: thin; background-color: rgba({{round(195*$count/count($matchList)+60)}},0,0,0.8)"></div>
                                            @else
                                            <div id="grid_r{{$row}}c{{$col}}" style="border-style: dotted; border-width: thin; background-color: rgba(0,0,0,0)"></div>
                                            @endif
                                        @endfor
                                    @endfor
                                </div>
                        </div>
                        Resize the window to properly size the graph
                        <div id="chart_auton_2" class="charts auton-graph hidden-chart"></div>
                    </div>
                </div>
            </div>
            <div id="teleop" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Teleop</span>
                </div>
                <div class="stat-content">
                    <div class="stat-numbers">
                        <div class="stat-data">
                            <span class="stat-number">Average Cycles</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_num_cycles'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Cycle Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_cycle_time'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Laps</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_num_laps'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Lap Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_lap_time'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Lap Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['lap_acc'] * 100, 1) }}%</span>
                        </div>
                    </div>
                    No Graph
                </div>
            </div>
            <div id="switch" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Switch</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="nextGraph('switch')"><i class="fa fa-chevron-right"></i></a>
                    <span id="switch-graph-number" class="graph-number">1/2</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="prevGraph('switch')"><i class="fa fa-chevron-left"></i></a>
                </div>
                <div class="stat-content">
                    <div class="stat-numbers">
                        <div class="stat-data">
                            <span class="stat-number">Average Scored</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_num_home_switch'] + $analytics['avg_num_away_switch'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Scored Home</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_num_home_switch'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Scored Away</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_num_away_switch'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Home Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_home_switch_time'] , 1) }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">SD of Home Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['home_switch_time_sd'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Away Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_away_switch_time'] , 1) }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">SD of Away Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['away_switch_time_sd'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Home Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['home_switch_acc'] * 100, 1)}}%</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Away Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['away_switch_acc'] * 100, 1)}}%</span>
                        </div>
                    </div>
                    <div class="chart-container">
                        <div id="chart_switch_1" class="charts switch-graph"></div>
                        <div id="chart_switch_2" class="charts hidden-chart switch-graph"></div>
                    </div>
                </div>
            </div>
            <div id="scale" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Scale</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="nextGraph('scale')"><i class="fa fa-chevron-right"></i></a>
                    <span id="scale-graph-number" class="graph-number">1/2</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="prevGraph('scale')"><i class="fa fa-chevron-left"></i></a>
                </div>
                <div class="stat-content">
                    <div class="stat-numbers">
                        <div class="stat-data">
                            <span class="stat-number">Average Scored</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_num_scale'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_scale_time'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">SD of Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['scale_time_sd'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['scale_acc'] * 100, 1)}}%</span>
                        </div>
                    </div>
                    <div class="chart-container">
                        <div id="chart_scale_1" class="charts scale-graph"></div>
                        <div id="chart_scale_2" class="charts hidden-chart scale-graph"></div>
                    </div>
                </div>
            </div>
            <div id="exchange" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Exchange</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="nextGraph('exchange')"><i class="fa fa-chevron-right"></i></a>
                    <span id="exchange-graph-number" class="graph-number">1/2</span>
                    <a class="graph-arrow" href="javascript:void(0)" onclick="prevGraph('exchange')"><i class="fa fa-chevron-left"></i></a>
                </div>
                <div class="stat-content">
                    <div class="stat-numbers">
                        <div class="stat-data">
                            <span class="stat-number">Average Scored</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_num_exchange'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Average Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['avg_exchange_time'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">SD of Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['exchange_time_sd'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Accuracy</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['exchange_acc'] * 100, 1)}}</span>
                        </div>
                    </div>
                    <div class="chart-container">
                        <div id="chart_exchange_1" class="charts exchange-graph"></div>
                        <div id="chart_exchange_2" class="charts hidden-chart exchange-graph"></div>
                    </div>
                </div>
            </div>
            <div id="hang" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Hang</span>
                </div>
                <div class="stat-content">
                    <div class="stat-numbers">
                        <div class="stat-data">
                            <span class="stat-number">Average Time</span>
                            <span class="stat-number stat-number-data">{{ round($analytics['active_hang_avg'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Standard Deviation</span>
                            <span class="stat-number stat-number-data">{{round($analytics['active_hang_sd'], 1)}}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Accuracy</span>
                            <span class="stat-number stat-number-data">{{round(($analytics['num_center_rung_hangs']+$analytics['num_carrier_hangs']+$analytics['num_assissted_hangs'])/count($matchList),1)*100}}%</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Count of Centre</span>
                            <span class="stat-number stat-number-data">{{ $analytics['num_center_rung_hangs'] }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Count of Carrier</span>
                            <span class="stat-number stat-number-data">{{ $analytics['num_carrier_hangs'] }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Count of Assisted</span>
                            <span class="stat-number stat-number-data">{{ $analytics['num_assissted_hangs'] }}</span>
                        </div>
                        <div class="stat-data">
                            <span class="stat-number">Count of Park</span>
                            <span class="stat-number stat-number-data">{{ $analytics['num_park_hangs'] }}</span>
                        </div>
                    </div>
                    <div class="chart-container">
                        <div id="chart_hang" class="charts"></div>
                    </div>
                </div>
            </div>
            <div id="comments" class="stat-container">
                <div class="stats-title">
                    <span class="stat-title">Match Comments</span>
                    <!-- <a class="graph-arrow" href="#">►</a>
                    <span id="exchange-graph-number" class="graph-number">1/2</span>
                    <a class="graph-arrow" href="#">◄</a> -->
                </div>
                <div class="stat-content">
                    <table class="table table-hover table-responsive">
    					<thead>
    						<tr>
    							<th>Match #</th>
    							<th>Scout Name</th>
    							<th>Comments</th>
    						</tr>
    					</thead>
    					<tbody>
    						<?php
    						$info = json_decode($analytics['match_comments'], true);
    						for($i=1; $i<count($info); $i++) {
    							echo "<tr><td>".$info[$i]['matchNum']."</td><td>".$info[$i]['scout']."</td><td>".$info[$i]['comments']."</td></tr>";
    						}
    						?>
    					</tbody>
    				</table>
                </div>
            </div>
        </div>


        <div class="side-container" id="side-container">
            <div id="side-content">
                <h1 class="team-name">{{substr_replace($title, '', 0, 5)}}</h1>
                <a class="overall-stat-row active-row" href="#overall">
                    Overall
                </a>
                <a class="overall-stat-row" href="#auton">
                    Auto
                </a>
                <a class="overall-stat-row" href="#teleop">
                    Teleop
                </a>
                <a class="overall-stat-row" href="#switch">
                    Switch
                </a>
                <a class="overall-stat-row" href="#scale">
                    Scale
                </a>
                <a class="overall-stat-row" href="#exchange">
                    Exchange
                </a>
                <a class="overall-stat-row" href="#hang">
                    Hang
                </a>
                <a class="overall-stat-row" href="#comments">
                    Comments
                </a>
            </div>
        </div>
    </div>

<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);

window.onresize = function(){
    drawMultSeries();
}

function drawMultSeries() {
        drawScaleCharts();
        drawHangCharts();
        drawAutonCharts();
        drawExchangeCharts();
        drawSwitchCharts();
    }

    function drawScaleCharts() {
        var data = new google.visualization.arrayToDataTable([
            ['Time', 'Count'],
            <?php $info = json_decode($analytics['scale_time_histo'], true); ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$i-1}}', {{$info[$i][$i-1]}}],
            @endfor
        ]);

        var options = {
            isStacked: true,
            legend: {
                position: 'none'
            },
            chartArea: {
                width: "85%",
                height: "70%"
            },
            hAxis: {
                title: 'Cycle Time (s)'
            },
            vAxis: {
                title: 'Count'
            }
        };

        var chart = new google.visualization.ColumnChart(
            document.getElementById('chart_scale_1'));

        chart.draw(data, options);


        ////////////////////////////////


        data = new google.visualization.arrayToDataTable([
            ['Match Number', 'Cubes Scored'],
            <?php $info = json_decode($analytics['scale_per_match'], true); ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}', {{$info[$i]['hits']}}],
            @endfor
        ]);

        options = {
            width: document.getElementById('chart_scale_1').offsetWidth,
            height: document.getElementById('chart_scale_1').offsetHeight,
            legend: {
                position: 'none'
            },
            chartArea: {
                width: "85%",
                height: "70%"
            },
            hAxis: {
                title: 'Match Number'
            },
            vAxis: {
                title: 'Cubes Scored'
            }
        };

        chart = new google.visualization.ColumnChart(
            document.getElementById('chart_scale_2'));

        chart.draw(data, options);
    }

    function drawSwitchCharts() {
        var data = new google.visualization.arrayToDataTable([
            ['Time', 'Home Count', 'Away Count'],
            <?php
            $info  = json_decode($analytics['home_switch_time_histo'], true);
            $info2 = json_decode($analytics['away_switch_time_histo'], true);
            ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$i-1}}',
                @if(count($info)>$i)
                    {{$info[$i][$i-1]}},
                @else
                    0,
                @endif
                @if(count($info2)>$i)
                    {{$info2[$i][$i-1]}}],
                @else
                    null],
                @endif
            @endfor
        ]);

        var options = {
            isStacked: true,
            // legend: {
            //     position: 'none'
            // },
            chartArea: {
                // width: "85%",
                height: "70%"
            },
            hAxis: {
                title: 'Cycle Time (s)'
            },
            vAxis: {
                title: 'Count'
            }
        };

        var chart = new google.visualization.ColumnChart(
          document.getElementById('chart_switch_1'));

        chart.draw(data, options);

        data = new google.visualization.arrayToDataTable([
            ['Match Number', 'Home Scored', 'Away Scored'],
            <?php $info  = json_decode($analytics['home_switch_per_match'], true); ?>
            <?php $info2 = json_decode($analytics['away_switch_per_match'], true); ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}', {{$info[$i]['hits']}}, {{$info2[$i]['hits']}}],
            @endfor
        ]);

        options = {
            isStacked: true,
            width: document.getElementById('chart_switch_1').offsetWidth,
            height: document.getElementById('chart_switch_1').offsetHeight,
            chartArea: {
                // width: "85%",
                height: "70%"
            },
            hAxis: {
                title: 'Match Number'
            },
            vAxis: {
                title: 'Cubes Scored'
            }
        };

        chart = new google.visualization.ColumnChart(
            document.getElementById('chart_switch_2'));

        chart.draw(data, options);
    }

    function drawExchangeCharts() {
        var data = new google.visualization.arrayToDataTable([
            ['Time', 'Count'],
            <?php $info = json_decode($analytics['exchange_time_histo'], true); ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$i-1}}', {{$info[$i][$i-1]}}],
            @endfor
        ]);

        var options = {
            isStacked: true,
            legend: {
                position: 'none'
            },
            chartArea: {
                width: "85%",
                height: "70%"
            },
            hAxis: {
                title: 'Cycle Time (s)'
            },
            vAxis: {
                title: 'Count'
            }
        };

        var chart = new google.visualization.ColumnChart(
          document.getElementById('chart_exchange_1'));

        chart.draw(data, options);


        data = new google.visualization.arrayToDataTable([
            ['Match Number', 'Cubes Scored'],
            <?php $info = json_decode($analytics['exchange_per_match'], true); ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}', {{$info[$i]['hits']}}],
            @endfor
        ]);

        options = {
            width: document.getElementById('chart_exchange_1').offsetWidth,
            height: document.getElementById('chart_exchange_1').offsetHeight,
            legend: {
                position: 'none'
            },
            chartArea: {
                width: "85%",
                height: "70%"
            },
            hAxis: {
                title: 'Match Number'
            },
            vAxis: {
                title: 'Cubes Scored'
            }
        };

        chart = new google.visualization.ColumnChart(
            document.getElementById('chart_exchange_2'));

        chart.draw(data, options);
    }

    function drawAutonCharts() {
        var data = new google.visualization.arrayToDataTable([
            ['Match Number', 'Scale', 'Switch', 'Exchange'],
            <?php $info = json_decode($analytics['auto_cubes_per_match'], true); ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}',{{$info[$i]["scl"]}},{{$info[$i]["hsw"]}},{{$info[$i]["xch"]}}],
            @endfor
        ]);

        var options = {
            isStacked: true,
            chartArea: {
                // width: "85%",
                height: "70%"
            },
            hAxis: {
                title: 'Match Number'
            },
            vAxis: {
                title: 'Cubes Scored'
            }
        };

        var chart = new google.visualization.ColumnChart(
          document.getElementById('chart_auton_2'));

        chart.draw(data, options);
    }

    function drawHangCharts() {
        var data = new google.visualization.arrayToDataTable([
            ['Match Number', 'Centre Rung', 'Carried/Assisted', 'Carrier'],
            <?php $info  = json_decode($analytics['hang_per_match'], true); ?>
            @for($i=1; $i<count($info); $i++)
                ['{{$info[$i]["matchNum"]}}',
                @if($info[$i]['hangPos'] == 'Center Rung')
                    {{$info[$i]['hangTime']}}, 0, 0],
                @elseif($info[$i]['hangPos'] == 'Carried/Assisted')
                    0, {{$info[$i]['hangTime']}}, 0],
                @elseif($info[$i]['hangPos'] == 'Carrier')
                    0, 0, {{$info[$i]['hangTime']}}],
                @else
                    0, 0, 0],
                @endif
            @endfor
        ]);

        var options = {
            isStacked: true,
            chartArea: {
                height: "70%"
            },
            hAxis: {
                title: 'Match Number',
            },
            vAxis: {
                title: 'Time (s)'
            }
        };

        var chart = new google.visualization.ColumnChart(
          document.getElementById('chart_hang'));

        chart.draw(data, options);
    }
</script>

</body>
@else
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Sorry, no matches found for this team.
</body>
@endif

</html>
