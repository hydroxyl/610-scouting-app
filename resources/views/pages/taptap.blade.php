@extends('pages/inputform')
@section('tap-tap')
<style>
	.big-button {
		height: 50px;
		margin-bottom: 5px;
	}
	.left-button-column {
		display: inline-block;
		width: 33%;
	}
	.right-button-column {
		display: inline-block;
		right: 3%;
		width: 33%;
	}
</style>

<div class="left-button-column" id="start-buttons">
	<button class="big-button btn btn-success" type="button" onclick="portalBtn()">Portal</button><br>
	<button class="big-button btn btn-success" type="button" onclick="pyramidBtn()">Cube Zone/Pyramid</button><br>
	<button class="big-button btn btn-success" type="button" onclick="hPlatformBtn()">Home Platform Zone</button><br>
	<button class="big-button btn btn-success" type="button" onclick="aPlatformBtn()">Away Platform Zone</button><br>
	<button class="big-button btn btn-success" type="button" onclick="groundBtn()">Preload/Ground</button>
</div>
<div class="left-button-column" id="end-buttons" hidden>
	<button class="big-button btn btn-danger" type="button" onclick="homeSwitchBtn()">Home Switch</button><br>
	<button class="big-button btn btn-danger" type="button" onclick="scaleBtn()">Scale</button><br>
	<button class="big-button btn btn-danger" type="button" onclick="awaySwitchBtn()">Away Switch</button><br>
	<button class="big-button btn btn-danger" type="button" onclick="exchangeBtn()">Exchange</button><br>
	<button class="big-button btn btn-danger" type="button" onclick="droppedBtn()">Dropped</button>
</div>
<div class="right-button-column" id="hit-miss" align="right" hidden>
	<button class="big-button btn btn-danger" type="button" onclick="yesBtn()">Hit</button><br>
	<button class="big-button btn btn-danger" type="button" onclick="noBtn()">Miss</button>
</div>
<br><br><span id="timer"><b>0.0 seconds</b></span><br><br>

<table class="table">
	<thead>
		<tr>
			<th>Cycle #</th>
			<th>Start Pos</th>
			<th>End Pos</th>
			<th>Duration</th>
			<th>Hit/Miss</th>
			<th>Remove</th>
		</tr>
	</thead>
	<tbody id="tableBody"></tbody>
</table>
<textarea name="cycle_times" id="cycle_times" style="font-family:monospace;" wrap="soft" hidden readonly>[{}]</textarea>

<script type="text/javascript">
	var cycleCnt = 0;
	var startTime;
	var startPos;
	var endPos;
	var endTime;
	var inCycle = false;
	var timer;

	function removeRow(cycleNum) {
		var row = document.getElementById('cycleRow'+cycleNum);
		row.parentNode.removeChild(row);
		var cycleText = document.getElementById('cycle_times').innerHTML;
		var cycleJSON = JSON.parse(cycleText);
		for (var i=1; i<cycleJSON.length; i++) {
			if (cycleJSON[i]!==null && cycleJSON[i].cycleNum == cycleNum) {
				delete cycleJSON[i];
			}
		}
		var result;
		// result.substring(1,result.length)
		document.getElementById('cycle_times').innerHTML = JSON.stringify(cycleJSON);
	}

	function portalBtn() {
		startPos = "Ptl";
		start();
	}
	function pyramidBtn() {
		startPos = "Pyr";
		start();
	}
	function groundBtn() {
		startPos = "Grd";
		start();
	}
	function hPlatformBtn() {
		startPos = "HPl";
		start();
	}
	function aPlatformBtn() {
		startPos = "APl";
		start();
	}
	function start() {
		if(inCycle == false) {
			var d = new Date();
			startTime = d.getTime();
			document.getElementById("start-buttons").hidden = true;
			document.getElementById("hit-miss").hidden = true;
			document.getElementById("end-buttons").hidden = false;
			startTimer();
			inCycle=true;
		}
	}

	function homeSwitchBtn() {
		endPos = "HSw";
		end();
	}
	function scaleBtn() {
		endPos = "Scl";
		end();
	}
	function awaySwitchBtn() {
		endPos = "ASw";
		end();
	}
	function exchangeBtn() {
		endPos = "Xch";
		end();
	}
	function droppedBtn() {
		endPos = "Drp";
		end();
	}
	function end() {
		if (inCycle==true) {
			var d = new Date();
			endTime = d.getTime();
			cycleCnt++;
			document.getElementById("hit-miss").hidden = false;
			endTimer();
			inCycle=false;
		}
	}

	function yesBtn() {
		tellHitMiss("H");
	}
	function noBtn() {
		tellHitMiss("M");
	}
	function tellHitMiss(hitMiss) {
		if(inCycle==false) {
			var text = document.getElementById('cycle_times').innerHTML;
			var duration = (endTime-startTime)/1000;
			document.getElementById('cycle_times').innerHTML = text.substring(0, text.length-1)+',{"cycleNum":'+cycleCnt+',"startPos":"'+startPos+'","endPos":"'+endPos+'","duration":'+duration+',"startTime":'+startTime+',"hitMiss":"'+hitMiss+'"}]';
			if(hitMiss=="H") {
				document.getElementById("tableBody").innerHTML = document.getElementById("tableBody").innerHTML+"<tr id='cycleRow"+cycleCnt+"'><td>"+cycleCnt+"</td><td>"+startPos+"</td><td>"+endPos+"</td><td>"+duration+"</td><td>Hit</td><td><button type='button' onclick='removeRow("+cycleCnt+")'>Remove</button></td></tr>";
			} else {
				document.getElementById("tableBody").innerHTML = document.getElementById("tableBody").innerHTML+"<tr id='cycleRow"+cycleCnt+"'><td>"+cycleCnt+"</td><td>"+startPos+"</td><td>"+endPos+"</td><td>"+duration+"</td><td>Miss</td><td><button type='button' onclick='removeRow("+cycleCnt+")'>Remove</button></td></tr>";
			}
			document.getElementById("start-buttons").hidden = false;
			document.getElementById("end-buttons").hidden = true;
			document.getElementById("hit-miss").hidden = true;
		}
	}

	function endTimer() {
		window.clearInterval(timer);
		document.getElementById('timer').innerHTML = "<b>0.0 seconds</b>";
	}
	function startTimer() {
		timer = window.setInterval(function() {
			var d = new Date();
			var curTime = d.getTime();
			var duration = (Math.round((curTime-startTime)/100)/10).toFixed(1);
			document.getElementById('timer').innerHTML = "<b>"+duration+" seconds</b>";
		}, 100);
	}
</script>

@stop