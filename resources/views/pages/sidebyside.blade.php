@extends('layouts/main')
@section('content')
<form action="{{ route('admin.sidebyside.callback') }}" method="post">
   <div class="input-group">
      <span class="input-group-addon" id="basic-addon3">Team 1</span>
      <input type="text" class="form-control" name="team1" aria-describedby="basic-addon3">
   </div>
   <br>
   <div class="input-group">
      <span class="input-group-addon" id="basic-addon3">Team 2</span>
      <input type="text" class="form-control" name="team2" aria-describedby="basic-addon3">
   </div>
   <br>
   <button class="btn btn-primary btn-lg" type="submit">Next</button>
</form>
@stop
