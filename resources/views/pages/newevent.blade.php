@extends('layouts/main')
@section('content')

<form action="{{ route('admin.events.new.callback') }}" method="post">

  <div class="form-group row">
   <label for="example-text-input" class="col-2 col-form-label">Event Code</label>
   <div class="col-10">
     <input class="form-control" type="text" value="" name="event_code">
   </div>
 </div>

 <input class="btn btn-outline-success" style="float: right;" type="submit" id="submitBtn" value="Save">
</form>

@stop
