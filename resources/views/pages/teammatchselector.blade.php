@extends('layouts/main')
@section('content')
<h3>
Enter the team number:
<h3>
<form action="{{ route('admin.teammatches.callback') }}" method="post">
   <div class="input-group">
      <input type="text" name="team" class="form-control form-control-lg" placeholder="Team number">
      <span class="input-group-btn">
      <button class="btn btn-primary btn-lg" type="submit">Next</button>
      </span>
   </div>
</form>
@stop
