@extends('layouts/main')
@section('content')
<br>
<h5>Expert Scouting</h5>
<br>
@if(isset($expertScoutingSettings[Auth::User()->id]) && !empty($expertScoutingSettings[Auth::User()->id]))

<table class="table table-hover">
   <thead>
      <tr>
         <th>Team Number</th>
         <th>Photo Status</th>
         <th>Photo Upload</th>
         <th>Robot Stats*</th>
      </tr>
   </thead>
   <tbody>
   <!-- TODO: Check if this works -->
      @foreach($expertScoutingSettings[Auth::User()->id] as $teamnum)
      <tr>
         <td>{{$teamnum}}</td>
         @if(App\RobotPhotos::where('team_id', 'frc'. $teamnum)->count() >= 1)
         <td><i class="fa fa-check" aria-hidden="true"></i></td>
         @else
         <td><i class="fa fa-times" aria-hidden="true"></i></td>
         @endif
         <td><a href="{{route('team.upload', 'frc' . $teamnum)}}">Upload</a></td>
         <td><a href="{{route('admin.team', 'frc' . $teamnum)}}">Here</a></td>
      </tr>
      @endforeach
   
   </tbody>
</table>
<p>*Please be careful with this, and dont show this page to anyone outside of Team 610 under any circumstance!</p>
@else
<p>Sorry, you havent been assigned any expert scouting teams yet.</p>
@endif
<br>
<h3>Matches Scouted</h3>
<br>
<table class="table table-hover">
   <thead>
      <tr>
         <th>Scouting ID</th>
         <th>Team Number</th>
         <th>Replayed Match</th>
         <th>Time</th>
         <th>Edit</th>
      </tr>
      <!--TODO: insert all of scout's scouted matches onto the page -->
   </thead>
</table>
<hr>
<!-- TODO: pull all of the current matches into the page
<h3>Our Upcoming Matches</h3>
<table class="table table-hover">
   <thead>
      <tr>
         <th>Match Number</th>
         <th>Red 1</th>
         <th>Red 2</th>
         <th>Red 3</th>
         <th>Blue 1</th>
         <th>Blue 2</th>
         <th>Blue 3</th>
      </tr>
   </thead>
   <tbody>
      @foreach($ourEvents as $match)
      <tr>
         <td>{{$match->match_number}}</td>
         <td>{{substr($match->red1, 3)}}</td>
         <td>{{substr($match->red2, 3)}}</td>
         <td>{{substr($match->red3, 3)}}</td>
         <td>{{substr($match->blue1, 3)}}</td>
         <td>{{substr($match->blue2, 3)}}</td>
         <td>{{substr($match->blue3, 3)}}</td>
      </tr>
      @endforeach
   </tbody>
</table>-->
@stop
