@extends('layouts/main')
@section('content')
<!--TODO: refactor the edit page to include the correct version -->
<script src="{{url('js/buttons.js')}}"></script>
<form action="{{ route('edit.callback') }}" method="post">
<input type="hidden" name="id" value="{{ $data->id }}">
  <!--*************************************Auton************************************************-->
<div class="card">
  <div class="card-header">
    Autonomous Period
  </div>
  <div class="card-block">
    <p>Auton starting position:</p>
    <fieldset id="starting_pos_auton">
        @if($data->starting_pos_auton == "center")
        <input type="radio" value="center" name="starting_pos_auton" checked> Center<br>
        @else
        <input type="radio" value="center" name="starting_pos_auton"> Center<br>
        @endif

        @if($data->starting_pos_auton == "human")
        <input type="radio" value="human" name="starting_pos_auton" checked> Human Player<br>
        @else
        <input type="radio" value="human" name="starting_pos_auton"> Human Player<br>
        @endif

        @if($data->starting_pos_auton == "boiler")
        <input type="radio" value="boiler" name="starting_pos_auton" checked> Boiler<br>
        @else
        <input type="radio" value="boiler" name="starting_pos_auton"> Boiler<br>
        @endif

        @if($data->starting_pos_auton == "other")
        <input type="radio" value="other" name="starting_pos_auton" checked> Other<br>
        @else
        <input type="radio" value="other" name="starting_pos_auton"> Other<br>
        @endif

    </fieldset>
    <hr>
    <p>Gears Attempted</p>
     <button type="button" class="btn btn-danger" name="minusone_gears_attempted_auton" onclick="gears_attempted_auton_inc(-1)">-1</button>
     <input type="number" name="gears_attempted_auton" id="gears_attempted_auton" value="{{$data->gears_attempted_auton}}" min="0" max="5" readonly="true">
     <button type="button" class="btn btn-success" name="plusone_gears_attempted_auton" onclick="gears_attempted_auton_inc(1)">+1</button>
     <hr>
     <p>Gears Scored</p>
     <button type="button" class="btn btn-danger" name="minusone_gears_attempted_auton" onclick="gears_success_auton_inc(-1)">-1</button>
     <input type="number" name="gears_success_auton" id="gears_success_auton" value="{{$data->gears_success_auton}}" min="0" max="5" >
     <button type="button" class="btn btn-success" name="plusone_gears_attempted_auton" onclick="gears_success_auton_inc(1)">+1</button>
     <hr>
     <p>High Goals Scored</p>
      <button type="button" class="btn btn-danger" name="minusten_highgoal_auton" onclick="high_goal_shots_auton_inc(-10)">-10</button>
      <button type="button" class="btn btn-danger" name="minusfive_highgoal_auton" onclick="high_goal_shots_auton_inc(-5)">-5</button>
      <button type="button" class="btn btn-danger" name="minusone_highgoal_auton" onclick="high_goal_shots_auton_inc(-1)">-1</button>
      <input type="number" name="high_goal_shots_auton" id="high_goal_shots_auton" value="{{$data->high_goal_shots_auton}}" min="0" max="200" >
      <button type="button" class="btn btn-success" name="plusone_highgoal_auton" onclick="high_goal_shots_auton_inc(1)">+1</button>
      <button type="button" class="btn btn-success" name="plusfive_highgoal_auton" onclick="high_goal_shots_auton_inc(5)">+5</button>
      <button type="button" class="btn btn-success" name="plusten_highgoal_auton" onclick="high_goal_shots_auton_inc(10)">+10</button>
      <hr>
      <p>Was the low goal Used?</P>
        <fieldset id="low_goal_auton">
          <input type="radio" value="1" name="low_goal_auton" @if($data->low_goal_auton == 1) checked @endif> Yes<br>
          <input type="radio" value="0" name="low_goal_auton" @if($data->low_goal_auton == 0) checked @endif> No<br>
        </fieldset>
  </div>
</div>
<br>
  <!--*************************************Teleop************************************************-->
<div class="card">
  <div class="card-header">
    Teleoperated Period
  </div>
  <div class="card-block">
    <p>Gears Attempted</p>
    <button type="button" class="btn btn-danger" name="minusone_gears_attempted_teleop" onclick="gears_attempted_teleop_inc(-1)">-1</button>
    <input type="number" name="gears_attempted_teleop" id="gears_attempted_teleop" value="{{$data->gears_attempted_teleop}}" min="0" max="30" >
    <button type="button" class="btn btn-success" name="plusone_gears_attempted_teleop" onclick="gears_attempted_teleop_inc(1)">+1</button>
    <hr>
    <p>Gears Scored</p>
    <button type="button" class="btn btn-danger" name="minusone_gears_attempted_teleop" onclick="gears_success_teleop_inc(-1)">-1</button>
    <input type="number" name="gears_success_teleop" id="gears_success_teleop" value="{{$data->gears_success_teleop}}" min="0" max="30" >
    <button type="button" class="btn btn-success" name="plusone_gears_attempted_teleop" onclick="gears_success_teleop_inc(1)">+1</button>
    <hr>
    <p>High Goals Scored</p>
    <button type="button" class="btn btn-danger" name="minusten_highgoal_auton" onclick="high_goal_shots_teleop_inc(-10)">-10</button>
    <button type="button" class="btn btn-danger" name="minusfive_highgoal_auton" onclick="high_goal_shots_teleop_inc(-5)">-5</button>
    <button type="button" class="btn btn-danger" name="minusone_highgoal_auton" onclick="high_goal_shots_teleop_inc(-1)">-1</button>
    <input type="number" name="high_goal_shots_teleop" id="high_goal_shots_teleop" value="{{$data->high_goal_shots_teleop}}" min="0" max="200" >
    <button type="button" class="btn btn-success" name="plusone_highgoal_auton" onclick="high_goal_shots_teleop_inc(1)">+1</button>
    <button type="button" class="btn btn-success" name="plusfive_highgoal_auton" onclick="high_goal_shots_teleop_inc(5)">+5</button>
    <button type="button" class="btn btn-success" name="plusten_highgoal_auton" onclick="high_goal_shots_teleop_inc(10)">+10</button>
    <hr>
    <p>Ready for take off (Hang)</p>
    <fieldset id="hang">
      <input type="radio" value="1" name="hang" @if($data->hang == 1) checked @endif> Yes<br>
      <input type="radio" value="0" name="hang" @if($data->hang == 0) checked @endif> No<br>
    </fieldset>
    <hr>
    <p>Hanging Position</p>
    <fieldset id="hang_pos">
      <input type="radio" value="human" name="hang_pos" @if($data->hang_pos == "human") checked @endif> Human Player<br>
      <input type="radio" value="center" name="hang_pos" @if($data->hang_pos == "center") checked @endif> Center<br>
      <input type="radio" value="hopper" name="hang_pos" @if($data->hang_pos == "hopper") checked @endif> Hopper<br>
      <input type="radio" value="dnh" name="hang_pos" @if($data->hang_pos == "dnh") checked @endif> Did Not Hang<br>
    </fieldset>
    <hr>
    <p>Climb Time (Not including line up!)</p>
    Tip: Remember what time they started to climb and what time they completed the climb and subtract the two.<br>
    <input type="number" name="hang_time" id="hang_time" value="{{$data->hang_time}}" min="0" max="30"> seconds
    <hr>
    <p>Did the robot play defense?</p>
    <fieldset id="played_defense">
      <input type="radio" value="1" name="played_defense" @if($data->played_defense == 1) checked @endif> Yes<br>
      <input type="radio" value="0" name="played_defense" @if($data->played_defense == 0) checked @endif> No<br>
    </fieldset>
  </div>
</div>
<br>
<!--*************************************Post************************************************-->
<div class="card">
  <div class="card-header">
    Post Game Analysis
  </div>

  <div class="card-block">
    <p>Did this robot contribute <b>most</b> of the pressure for the alliance?</p>
    <fieldset id="pressure">
      <input type="radio" value="1" name="pressure" @if($data->pressure == 1) checked @endif> Yes<br>
      <input type="radio" value="0" name="pressure" @if($data->pressure == 0) checked @endif> No<br>
    </fieldset>
    <hr>
    <p>Did this robot only shoot?</p>
    <fieldset id="only_shooting">
      <input type="radio" value="1" name="only_shooting" @if($data->only_shooting == 1) checked @endif> Yes<br>
      <input type="radio" value="0" name="only_shooting" @if($data->only_shooting == 0) checked @endif> No<br>
    </fieldset>
    <hr>
    <p>Was there substantial defence against this robot?</p>
    <fieldset id="defended">
      <input type="radio" value="1" name="defended" @if($data->defended == 1) checked @endif> Yes<br>
      <input type="radio" value="0" name="defended" @if($data->defended == 0) checked @endif> No<br>
    </fieldset>
    <hr>
    <p>Match Comments (These aren’t an option, they are required.)</p>
    <textarea name="comments" rows="8" cols="50" maxlength="999" placeholder="Max character count is 999" id="comment_box" oninput="countChars()" required>{{$data->comments}}</textarea>
    <hr>
    Please check all of the data before submitting because scouts will be held accountable for their data. Consequences for blatantly incorrect data might apply.<br>
    <input class="btn btn-success" style="float: right;" type="submit" id="submitBtn" value="Save">

  </div>

</div>

</form>
@stop
