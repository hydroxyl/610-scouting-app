@extends('layouts/main')
@section('content')
<form action="{{ route('user.callback') }}" method="post">
   <input type="hidden" name="id" value="{{$user->id}}">

   <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">Display Name</label>
    <div class="col-10">
      <input class="form-control" type="text" value="{{$user->display_name}}" name="display_name">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">First Name</label>
    <div class="col-10">
      <input class="form-control" type="text" value="{{$user->first_name}}" name="first_name">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">Last Name</label>
    <div class="col-10">
      <input class="form-control" type="text" value="{{$user->last_name}}" name="last_name">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-email-input" class="col-2 col-form-label">Email</label>
    <div class="col-10">
      <input class="form-control" type="email" value="{{$user->email}}" name="email">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-email-input" class="col-2 col-form-label">Role</label>
    <div class="col-10">
      <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="role">
        <option value="student">Student</option>
        <option value="admin" @if($user->role == 'admin') selected @endif>Admin</option>
      </select>
    </div>
  </div>

  <div class="form-group row">
    <label for="example-email-input" class="col-2 col-form-label">Enabled</label>
    <div class="col-10">
      <div class="form-check">
          <label class="form-check-label">
            <input name="enabled" class="form-check-input" type="checkbox" @if($user->enabled == true) checked @endif>
          </label>
        </div>
    </div>
  </div>


   <hr>
   <input class="btn btn-outline-success" style="float: right;" type="submit" id="submitBtn" value="Save">
</form>
</div>@stop
