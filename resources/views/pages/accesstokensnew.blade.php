@extends('layouts/main')
@section('content')
<form action="{{ route('admin.accesstokens.callback') }}" method="post">

  <div class="form-group row">
   <label for="example-text-input" class="col-2 col-form-label">Display Name</label>
   <div class="col-10">
     <input class="form-control" type="text" value="" name="display_name">
   </div>
 </div>

  <div class="form-group row">
   <label for="example-text-input" class="col-2 col-form-label">First Name</label>
   <div class="col-10">
     <input class="form-control" type="text" value="" name="first_name">
   </div>
 </div>

 <div class="form-group row">
  <label for="example-text-input" class="col-2 col-form-label">Last Name</label>
  <div class="col-10">
    <input class="form-control" type="text" value="" name="last_name">
  </div>
</div>

<div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Role</label>
  <div class="col-10">
    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="role">
      <option value="student">Student</option>
      <option value="admin">Admin</option>
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Duration</label>
  <div class="col-10">
    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="duration">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select>
  </div>
</div>

<hr>
   <input class="btn btn-outline-success" style="float: right;" type="submit" id="submitBtn" value="Save">
</form>
@stop
