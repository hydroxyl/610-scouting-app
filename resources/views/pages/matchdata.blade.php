@extends('layouts/main')
@section('content')
<h3>Match Data</h3>
<a href="{{route('edit', $match['id'])}}">
   <p style="float: right;">Edit</p>
</a>
<table class="table">
   <tbody>
      <tr>
         <td>Scout ID</td>
         <td>{{$name}}</td>
      </tr>
      @foreach($data as $key => $value)
      <tr>
         @if($key=="cycle_times")
            <td>{{$key}}<br>
               <a href="{{route('admin.exportCSV', $match['id'])}}">Export to csv</a>
            </td>
            <td>{{$value}}</td>
         @else
            <td>{{$key}}</td>
            <td>{{$value}}</td>
         @endif
      </tr>
      @endforeach
   </tbody>
</table>
@stop
