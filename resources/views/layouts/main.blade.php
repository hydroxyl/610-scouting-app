<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <link rel="icon" href="{{ url('favicon.ico') }}">
      <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
      <link rel="stylesheet" href="{{ url('css/custom.css') }}">
      <link rel="stylesheet" href="{{ url('css/font-awesome.css') }}">
      <script src="{{url('js/wipe.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/js/tether.min.js"></script>
      <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
      <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
      <script src="{{ url('js/bootstrap.js') }}"></script>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{$pageTitle}} | Team 610 Scouting</title>
   </head>
   <body>
     <style>
     @media (min-width: 768px) { /*Should be 992px*/
        .container {
          width: 970px;
        }
      }
      @media (min-width: 992px) { /*Should be 1200px*/
        .container {
          width: 1170px;
        }
      }
      @media (min-width: 1600px) {
        .container {
          width: 1570px;
        }
      }
     </style>
      <nav class="navbar navbar-light navbar-toggleable-md bg-faded"> <!--navbar-inverse-->
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="{{route ('index')}}">
            <img src="{{ url('img/logo128.png') }}" height="30" alt="">
            </a>
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
               <li class="nav-item @if(Route::is('index')) active @endif">
                  <a class="nav-link" href="{{route ('index')}}"><i class="fa fa-home"></i> Home<span class="sr-only"></span></a>
               </li>
               <li class="nav-item @if(Route::is('form')) active @endif">
                  <a class="nav-link" href="{{route ('form')}}"><i class="fa fa-plus-square-o"></i> Form<span class="sr-only"></span></a>
               </li>
               <li class="nav-item @if(Route::is('robotphotos.select')) active @endif">
                  <a class="nav-link" href="{{route ('robotphotos.select')}}"><i class="fa fa-camera"></i> Robot Photos<span class="sr-only"></span></a>
               </li>
               @can('view-admin-controls', Auth::User())

               <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Info</a>
                 <div class="dropdown-menu">
                   <a class="dropdown-item @if(Route::is('admin.matches')) active @endif" href="{{route ('admin.matches')}}"><i class="fa fa-calendar"></i> Matches<span class="sr-only"></span></a>
                   <a class="dropdown-item @if(Route::is('admin.teams')) active @endif" href="{{route ('admin.teams')}}"><i class="fa fa-users"></i> Teams<span class="sr-only"></span></a>
                   <!--a class="dropdown-item @if(Route::is('admin.teammatches')) active @endif" href="{{route ('admin.teammatches')}}"><i class="fa fa-binoculars"></i> Team Matches Lookup<span class="sr-only"></span></a-->
                 </div>
               </li>

               <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Analytics</a>
                 <div class="dropdown-menu">
                   <a class="dropdown-item @if(Route::is('admin.sidebyside')) active @endif" href="{{route ('admin.sidebyside')}}"><i class="fa fa-compress"></i> Side by Side<span class="sr-only"></span></a>
                   <a class="dropdown-item @if(Route::is('admin.overall')) active @endif" href="{{route ('admin.overall')}}"><i class="fa fa-globe"></i> Overall<span class="sr-only"></span></a>
                   <a class="dropdown-item @if(Route::is('admin.raw')) active @endif" href="{{route ('admin.raw')}}"><i class="fa fa-database"></i> Raw<span class="sr-only"></span></a>
                 </div>
               </li>

               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Exports</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item @if(Route::is('admin.rawExportCSV')) active @endif" href="{{route ('admin.rawExportCSV')}}"> Raw CSV</a>
                  <a class="dropdown-item @if(Route::is('admin.overallExportCSV')) active @endif" href="{{route ('admin.overallExportCSV')}}"> Overall CSV</a>
                  <a class="dropdown-item @if(Route::is('admin.overallExportJSON')) active @endif" href="{{route ('admin.overallExportJSON')}}"> Overall JSON</a>
                </div>
               </li>

               <li class="nav-item dropdown">
                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Settings</a>
                 <div class="dropdown-menu">
                   <a class="dropdown-item @if(Route::is('users'))  active @endif" href="{{route ('users')}}"><i class="fa fa-id-card-o"></i> Users<span class="sr-only"></span></a>
                   <a class="dropdown-item @if(Route::is('admin.accesstokens'))  active @endif" href="{{route ('admin.accesstokens')}}"><i class="fa fa-key"></i> Access Tokens<span class="sr-only"></span></a>
                   <a class="dropdown-item @if(Route::is('admin.expertscoutingsettings'))  active @endif" href="{{route ('admin.expertscoutingsettings')}}"><i class="fa fa-list"></i> Expert Scout<span class="sr-only"></span></a>
                   <a class="dropdown-item @if(Route::is('admin.events'))  active @endif" href="{{route ('admin.events')}}"><i class="fa fa-cog"></i> Event<span class="sr-only"></span></a>

                 </div>
               </li>
            </ul>

            <!--ul class="navbar-nav justify-content-end">
              <li class="nav-item @if(Route::is('admin.events')) active @endif">
                 <a class="nav-link" href="{{route ('admin.recalculate')}}">Force Recalculate Analytics <i class="fa fa-calculator"></i> <span class="sr-only"></span></a>
              </li>
            </ul-->
            &nbsp
            &nbsp
            @endcan
            <span class="navbar-text">
            {{ Auth::User()->display_name }}
            </span>
            &nbsp &nbsp
            <a href="{{ route('logout') }}">
            <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
            </a>
         </div>
      </nav>
      <div class="container">
         @if (count($errors) > 0)
         <br>
         <div class="alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         <br>
         @if(Session::has('success'))
         <div class="alert alert-success">
            {{Session::get('success') }}
         </div>
         <br>
         @endif
         @if(Session::has('error'))
         <div class="alert alert-danger">
            {{Session::get('error') }}
         </div>
         <br>
         @endif
         @if(isset($error_custom))
         <div class="alert alert-danger" role="alert">
            {{$error}}
         </div>
         <br>
         @endif
         @if(isset($success))
         <div class="alert alert-success" role="alert">
            {{$success}}
         </div>
         <br>
         @endif
         <div>
           @if(Route::is('new.callback') || Route::is('edit') || Route::is('admin.team'))
              @yield('content')
           @else
            <div class="card" id="main">
               <div class="card-header">
                  <strong>{{$title}}</strong>
               </div>
               <div class="card-block">
                  @yield('content')
               </div>
            </div>
            @endif
         </div>
         <br>
         <footer>
            <center>
               <p>Version <span class="text-warning">3.2.5</span></p>
            </center>
         </footer>
      </div>
   </body>
</html>
