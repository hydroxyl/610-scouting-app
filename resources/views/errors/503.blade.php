<!DOCTYPE html>
<html>
    <head>
        <title>Error 503</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

          <style>
              html, body {
                  height: 100%;
              }

              body {
                  margin: 0;
                  padding: 0;
                  width: 100%;
                  color: #8B0000;
                  display: table;
                  font-weight: 100;
                  font-family: 'Roboto', cursive;
              }

              .container {
                background: url("{{ url('img/Parth-Patel.jpg')}}") no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                text-align: center;
                display: table-cell;
                vertical-align: middle;
              }

              .content {
                  text-align: center;
                  display: inline-block;
              }

              .title {
                  font-size: 68px;
                  margin-bottom: 40px;
              }
              .footer {
                color: #000000;
                display: table;
                font-weight: 100;
                font-family: 'Roboto', sans-serif;
                font-size: 28px;
                background-color: #FFFF00
              }
          </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Error 503: Unavailable</div>
                <center>
                <div class="footer">If this issue persists please contact: felixyu@crescentschool.org</div>
              </center>
            </div>
        </div>
    </body>
</html>
