<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Events extends Model {

  protected $table = 'events';

  use SoftDeletes;

  protected $fillable = ['event', 'teams', 'expert_scouting', 'match_schedule', 'analytics', 'robot_photos', 'is_current_event'];

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];


}
