<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchSchedule extends Model {

  protected $table = 'match_schedule';

  public $incrementing=false;

  protected $fillable = [
      'id', 'event', 'match_id', 'comp_level', 'red1', 'red2', 'red3', 'blue1', 'blue2', 'blue3', 'match_number',
  ];


}
