<?php
/*
- Created by: Grant Chesney
- Date: October 2'nd 2016
- Version: 1.0
*/
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchData extends Model
{
    protected $table = 'match_data';

    public $incrementing=false;

    use SoftDeletes;

    protected $fillable = ['id', 'scout_id', 'data', 'layout', 'event_id', 'team_id'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
