<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-admin-controls', function ($user) {
            return $user->role == 'admin';
        });

        Gate::define('view-team-info', function ($user, $team) {
            if(!$user->role == 'admin') {
              $expertScoutingSettings = json_decode(App\Settings::where('key', 'expertScoutingSettings')->first()->value, true);
              if(isset($expertScoutingSettings[Auth::User()->id])){ //if the user is in the data
                if(!in_array(substr($team, 3), $expertScoutingSettings[Auth::User()->id])) { // if the user can see the team
                  return false;
                } else {
                  return true;
                }
              } else {
                return false;
              }
            } else {
              return true;
            }
        });




    }
}
