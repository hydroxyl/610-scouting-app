<?php
/*
- Created by: Grant Chesney
- Date: October 2'nd 2016
- Version: 1.0
*/
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RobotPhotos extends Model
{
    protected $table = 'robot_photos';

    use SoftDeletes;

    protected $fillable = ['id', 'scout_id', 'path', 'team_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
