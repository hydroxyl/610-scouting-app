<?php
/*
- Created by: Grant Chesney
- Date: October 2'nd 2016
- Version: 1.0
*/
namespace App;
use Illuminate\Database\Eloquent\Model;
class Analytics extends Model
{
    protected $table = 'analytics';

    protected $fillable = ['id', 'match_id', 'team_id', 'scout_id', 'layout', 'data', 'event_id'];
    
    protected $dates = ['created_at', 'updated_at'];
}
