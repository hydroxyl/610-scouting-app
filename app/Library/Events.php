<?php
/*
- Created by: Grant Chesney
- Date: March 16'th 2016
- Version: 1.0
*/
namespace App\Library;
use DB;
use App;
class Events
{
    public static function getEventMatches($event)
    {
        $client       = new \GuzzleHttp\Client();

        $matches = json_decode($client->get('https://www.thebluealliance.com/api/v3/event/' . $event . '/matches?X-TBA-Auth-Key=' . env('TBA_API_KEY'))->getBody());

        return $matches;

      }

    public static function validateEvents($event)
    {
        $client = new \GuzzleHttp\Client();

            try {
                $client->get('https://www.thebluealliance.com/api/v3/event/' . $event . '?X-TBA-Auth-Key=' . env('TBA_API_KEY'))->getBody();
            }
            catch (\Exception $e) {
                return false;
            }

            return true;
    }

    public static function isCurrentEventSet() {
      $events = App\Events::all();
      $set = false;
      foreach($events as $event){
        if($event->is_current_event == true) {
          $set = true;
          break;
        }
      }
      return $set;
    }

    public static function getCurrentEventID() {
      $events = App\Events::all();
      $currentEventID = null;
      foreach($events as $event){
        if($event->is_current_event == true) {
          $currentEvent = $event->id;
          break;
        }
      }
      return $currentEvent;
    }

}
