<?php
/*
- Created by: Grant Chesney
- Date: March 16'th 2016
- Version: 1.0
*/
namespace App\Library;
use DB;
use App;
class Teams
{
    public static function getTeams($event)
    {
        $client       = new \GuzzleHttp\Client();

        $teams = json_decode($client->get('https://www.thebluealliance.com/api/v3/event/' . $event . '/teams?X-TBA-Auth-Key=' .  env('TBA_API_KEY'))->getBody());

        return $teams;
    }

    public static function getTeamsFromCurEvent() {
      $teams1 = json_decode(App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail()->teams, true);
      //Fix this stupid line ^
      $teams = array();
      foreach ($teams1 as $key => $value) {
        $teams[$teams1[$key]['key']] = $teams1[$key];
      }
      return $teams;
    }

    public static function checkTeamNumber($id, $teamchk) {
      $event = App\Events::where('id', $id)->firstOrFail();
      $teams = json_decode($event->teams);
      foreach($teams as $team) {
        if($team->key == $teamchk) {
          return true;
        }
      }
      return false;
    }
}
