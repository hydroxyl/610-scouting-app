<?php
namespace App\Http\Middleware;
use Closure;
use Auth;
use App;
use Carbon\Carbon;
class TokenCheck
{

  protected $auth;

  public function __construct()
  {
    $this->auth = Auth::User();
  }
 public function handle($request, Closure $next)
 {
   if($this->auth == null){
     auth('web')->logout();
     return redirect(route('login'));
   }
   if ($this->auth->is_access_token == true) {
       if(Carbon::now()->lt(Carbon::parse($this->auth->expiry))) {
         return $next($request);
       } else {
         auth('web')->logout();
         $request->session()->flash('error', 'Sorry, your token is expired.');
         $user = App\User::where('id', $this->auth->id)->firstOrFail();
         $user->delete();
         return redirect(route('login'));
       }
   } else {
      return $next($request);
   }
 }

}
