<?php
namespace App\Http\Middleware;
use Closure;
use Auth;
use App;
use Carbon\Carbon;
class UserCheck
{
  protected $auth;

  public function __construct()
  {
    $this->auth = Auth::User();
  }

 public function handle($request, Closure $next)
 {
   if($this->auth == null){
     auth('web')->logout();
     return redirect(route('login'));
   }
   if ($this->auth->enabled == true) {
      return $next($request);
   } else {
    auth('web')->logout();
    $request->session()->flash('error', 'Sorry, your user account has been disabled by an administrator.');
    return redirect(route('login'));
   }
 }

}
