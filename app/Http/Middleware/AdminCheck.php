<?php
namespace App\Http\Middleware;
use Closure;
use Auth;
class AdminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     protected $auth;
     public function __construct()
     {
       $this->auth = Auth::User();
     }
    public function handle($request, Closure $next)
    {
      if (is_null ($this->auth)) {
          return redirect(route('login'));
      } else {
        	if (strpos($this->auth->role, 'admin') !== false) {
            	return $next($request);
          } else {
              return redirect(route('error.401'));
          }
      }
    }
}
