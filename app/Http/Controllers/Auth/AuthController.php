<?php
namespace App\Http\Controllers\Auth;
use Auth;
use App;
use Socialite;
use Log;
use Event;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/';
    protected $loginPath = '/login';

    public function login() {
      if(auth('web')->user() != null) {
          return redirect(route('index'));
      }
      return view('pages.auth.login');
    }

    public function loginRedirect() {
      return Socialite::driver('google')->with(['hd' => 'crescentschool.org'])->redirect();
    }

    public function loginCallback(Request $request) {
      $returnedUser = Socialite::driver('google')->stateless()->user();

      $user = App\User::firstOrNew(['google_id' => $returnedUser->id]);
      if ($user->exists) {
        if($user->enabled != true) {
          $request->session()->flash('error', 'Sorry, your user account has been disabled by an administrator.');
          return redirect(route('login'));
        }
        $user->login_count++;
        $user->save();
      } else {
        $user->email = $returnedUser->email;
        $user->avatar = $returnedUser->avatar;
        $user->first_name = $returnedUser->user['name']['givenName'];
        $user->last_name = $returnedUser->user['name']['familyName'];
        $user->display_name = $returnedUser->user['displayName'];
        $user->token = '0000000000';
        $user->is_access_token = false;
        $user->expiry = '';
        $user->enabled = true;
        $user->save();
      }

      auth('web')->login($user, true);

      return redirect(route('index'));

    }

    public function loginWithAccessToken() {
      return view('pages.auth.authtokenlogin');
    }

    public function logout() {
        auth('web')->logout();
        return redirect()->away('https://accounts.google.ca/logout');
    }

    public function loginWithAccessTokenCallback(Request $request) {
      $this->validate($request, [
       'token' => 'required|max:10|string|alpha_num',
      ]);
      $user = App\User::where('token', $request->token)->firstOrFail();
      auth('web')->login($user, true);
      return redirect(route('index'));
    }

}
