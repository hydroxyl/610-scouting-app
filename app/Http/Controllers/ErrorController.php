<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App;

class ErrorController extends Controller
{
    public function error401() {
      return view('errors.401');
    }
    public function errorte() {
      return view('errors.te');
    }

}
