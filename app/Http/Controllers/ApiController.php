<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;

class ApiController extends Controller
{
  public function status() {
    return true;
  }

  public function submit(Request $request) {
    if(filter_var($request->key, FILTER_SANITIZE_STRING) == "dacraxucrudruch7fuxAFruga") {
      try {
        $data = json_decode($request->data, true);
      }catch (\Exception $e) {
        return "error";
      }
      foreach($data as $element) {
        $currentEvent      = App\Settings::where('key', 'currentEvent')->first()->value;
        //dd($element);
        //Filter input
        $element["match_number"] = filter_var ($element["match_number"], FILTER_SANITIZE_NUMBER_INT);
        $element["team_number"] = filter_var ($element["team_number"], FILTER_SANITIZE_NUMBER_INT);
        $element["starting_pos_auton"] = filter_var ($element["starting_pos_auton"], FILTER_SANITIZE_STRING);
        $element["gears_attempted_auton"] = filter_var ($element["gears_attempted_auton"], FILTER_SANITIZE_NUMBER_INT);
        $element["gears_success_auton"] = filter_var ($element["gears_success_auton"], FILTER_SANITIZE_NUMBER_INT);
        $element["high_goal_shots_auton"] = filter_var ($element["high_goal_shots_auton"], FILTER_SANITIZE_NUMBER_INT);
        $element["low_goal_auton"] = filter_var ($element["low_goal_auton"], FILTER_SANITIZE_NUMBER_INT);
        $element["gears_attempted_teleop"] = filter_var ($element["gears_attempted_teleop"], FILTER_SANITIZE_NUMBER_INT);
        $element["gears_success_teleop"] = filter_var ($element["gears_success_teleop"], FILTER_SANITIZE_NUMBER_INT);
        $element["high_goal_shots_teleop"] = filter_var ($element["high_goal_shots_teleop"], FILTER_SANITIZE_NUMBER_INT);
        $element["hang_time"] = filter_var ($element["hang_time"], FILTER_SANITIZE_NUMBER_INT);
        $element["played_defense"] = filter_var ($element["played_defense"], FILTER_SANITIZE_NUMBER_INT);
        $element["pressure"] = filter_var ($element["most_pressure"], FILTER_SANITIZE_NUMBER_INT);
        $element["only_shooting"] = filter_var ($element["only_shooting"], FILTER_SANITIZE_NUMBER_INT);
        $element["defended"] = filter_var ($element["defended"], FILTER_SANITIZE_NUMBER_INT);
        $element["comments"] = filter_var ($element["comments"], FILTER_SANITIZE_STRING);
        $element["hang_pos"] = filter_var ($element["hang_pos"], FILTER_SANITIZE_STRING);
        $element["replay"] = filter_var ($element["is_replay"], FILTER_SANITIZE_NUMBER_INT);
        $element["email"] = filter_var ($element["email"], FILTER_SANITIZE_STRING);

        if($element["hang_time"] > 0 && $element["hang_pos"] != "dnh") {
          $element["hang"] = 1;
        } else {
          $element["hang"] = 0;
        }

        $game = new App\MatchData();

        $game->scout_email = $element["email"] . "(api request)";
        $game->event = $currentEvent;
        $game->team_id = "frc" . $element["team_number"];
        $game->team_number = $element["team_number"];
        $game->match_number = $element["match_number"];
        if($element["replay"] == 1) {
          $game->replay = 1;
        } else {
          $game->replay = 0;
        }
        if($element["replay"] == 1) {
          $game->match_id = "qm" . $element["match_number"] . "_1";
        } else {
          $game->match_id = "qm" . $element["match_number"];
        }
        $game->id = $currentEvent . "_" . $game->team_id . "_" . "qm" . $element["match_number"] . "_" . $game->replay . "_" . time();

        $game->starting_pos_auton = $element["starting_pos_auton"];
        $game->gears_attempted_auton = (int)$element["gears_attempted_auton"];
        $game->gears_success_auton = (int)$element["gears_success_auton"];
        $game->high_goal_shots_auton = (int) $element["high_goal_shots_auton"];
        if($element["low_goal_auton"] == 1) {
          $game->low_goal_auton = true;
        } else {
          $game->low_goal_auton = false;
        }

        $game->gears_attempted_teleop = (int) $element["gears_attempted_teleop"];
        $game->gears_success_teleop = (int) $element["gears_success_teleop"];
        $game->high_goal_shots_teleop = (int) $element["high_goal_shots_teleop"];
        if($element["hang"] == 1) {
            $game->hang = true;
        } else {
            $game->hang = false;
        }
        $game->hang_time = (int)$element["hang_time"];
        if($element["played_defense"] == 1) {
            $game->played_defense = true;
        } else {
            $game->played_defense = false;
        }

        if($element["pressure"] == 1) {
            $game->pressure = true;
        } else {
            $game->pressure = false;
        }
        if($element["only_shooting"] == 1) {
            $game->only_shooting = true;
        } else {
            $game->only_shooting = false;
        }
        if($element["defended"] == 1) {
            $game->defended = true;
        } else {
            $game->defended = false;
        }
        $game->comments = $element["comments"];
        $game->hang_pos = $element["hang_pos"];
        $running = true;

        DB::beginTransaction();
          $game->save();
        DB::commit();

        $runner = new App\Http\Controllers\webController();
      }
      $runner->calAnalytics();
      return "success";
    } else {
      return "Wrong api key, or none provided.";
    }
  }
}
