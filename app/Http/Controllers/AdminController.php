<?php
namespace App\Http\Controllers;

use Input;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Validator;
use Auth;
use Storage;
use DB;
use Log;

class AdminController extends Controller
{
    //TODO Fix the database default user role

    public function implodeJSON($jsonString) {
      $jsonArray = json_decode($jsonString, true);
      $resultText = '';
      for($i=0;$i<count($jsonArray);$i++) {
        foreach($jsonArray[$i] as $key=>$value) {
          $resultText = $resultText.' '.$key.' '.$value;
        }
      }

      return $resultText;
    }

    public function implodeKeyValue($array) {
      $output = '';
      foreach($array as $key=>$value) {
        $output = $output.$key.'=>'.$value;
      }
      return $output;
    }

    public static function getAnalytics() {
      //TODO: replace with get from database
      return App\Http\Controllers\AdminController::calculateAnalytics();
    }

    public static function calculateAnalytics() {
      //TODO: vv use these to replace JSON start positions
      $AUTON_PATH_START_POS = 1;
      $CYCLE_TIMES_START_POS = 1;

      $matchRows = json_decode($matchList = App\MatchData::where('event_id', App\Library\Events::getCurrentEventID())->whereNull('deleted_at')->get(), true);
      $teamList = App\Library\Teams::getTeamsFromCurEvent();
      $analyticsConfig = json_decode(Storage::get('analyticsConfig.json'), true);
      $formConfig = json_decode(Storage::get('formConfig.json'), true);
      $formMetaData = $formConfig['game'].$formConfig['timestamp'];
      $analyticsMetaData = $analyticsConfig['game'].$analyticsConfig['timestamp'];
      if ($analyticsMetaData != $formMetaData) {
        $request->session()->flash('error', 'Sorry, the analytics config is not properly set. Please contact felixyu@crescentschool.org to fix it.');
        return back();
      }
      $analyticsFields = $analyticsConfig['calculatables'];

      $analyzedDataOverall = [];

      foreach($teamList as $team=>$teamInfo) {
        $matchList = [];
        $matchNumList = [];
        $i=0;
        foreach($matchRows as $match) {
          $matchData = json_decode($match['data'], true);
          if ($matchData['team_num'].'' == substr($team, 3)) {
            $matchList[$matchData['match_num']]=$matchData;
            $matchNumList[$matchData['match_num']]=$match['id'];
            $i++;
          }
        }

        $operandsCollection = [];
        $analyzedData = [];
        foreach($analyticsFields as $fieldName=>$field) {
          //Auton stuff
          if($field['id']=='auto_start_row_avg') {
            $minRow = -1;
            $rowSum=0;
            foreach($matchList as $matchNum => $matchInfo) {
              $autonPathArray = json_decode($matchInfo['a_auton_path'],true);
              $min = 1000;
              for($i=1; $i<count($autonPathArray); $i++) {
                $curTapNum = $autonPathArray[$i]['tapNum'];
                if($curTapNum < $min && $autonPathArray[$i]['keep']=="true") {
                  $minRow=$autonPathArray[$i]['row'];
                }
              }
              $rowSum+=$minRow;
            }
            if(count($matchList)>0) {
              $analyzedData[$field['id']] = $rowSum/count($matchList);
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif ($field['id'] == 'auto_scl_scored_avg') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAutonAvg($matchList, 'scl');
          } elseif ($field['id'] == 'auto_hsw_scored_avg') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAutonAvg($matchList, 'hsw');
          } elseif ($field['id'] == 'auto_xch_scored_avg') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAutonAvg($matchList, 'xch');
          } elseif ($field['id'] == 'auto_overall_avg') {
            $numScored=0;
            $numMatches=0;
            foreach($matchList as $matchNum=>$matchInfo) {
              $cubes = json_decode($matchInfo['a_scoring'], true);
              $cubeAttempted = false;
              foreach($cubes as $key=>$value) {
                if($value>0) {
                  $cubeAttempted = true;
                  break;
                }
              }
              if($cubeAttempted) {
                $numScored+=$cubes['hsw_hit']+$cubes['scl_hit']+$cubes['xch_hit'];
                $numMatches++;
              }
            }
            if($numMatches>0) {
              $analyzedData[$field['id']] = $numScored/$numMatches;
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif ($field['id'] == 'auto_scl_acc') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAutonAcc($matchList, 'scl');
          } elseif ($field['id'] == 'auto_hsw_acc') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAutonAcc($matchList, 'hsw');
          } elseif ($field['id'] == 'auto_xch_acc') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAutonAcc($matchList, 'xch');
          } elseif ($field['id'] == 'auto_overall_acc') {
            $numMissed=0;
            $numScored=0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cubes = json_decode($matchInfo['a_scoring'], true);
              $numMissed += $cubes['hsw_miss']+$cubes['scl_miss']+$cubes['xch_miss'];
              $numScored += $cubes['hsw_hit']+$cubes['scl_hit']+$cubes['xch_hit'];
            }
            if($numMissed+$numScored>0) {
              $analyzedData[$field['id']] = $numScored/($numScored+$numMissed);
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif ($field['id'] == 'auto_cubes_per_match') {
            $perMatchString = "[{}]";
            foreach($matchList as $matchNum => $matchInfo) {
              $cubes = json_decode($matchInfo['a_scoring'], true);
              $perMatchString = substr($perMatchString, 0, -1) . ',{"matchNum":'.$matchInfo['match_num'].',"scl":'.$cubes['scl_hit'].',"hsw":'.$cubes['hsw_hit'].',"xch":'.$cubes['xch_hit'].'}]';
            }
            $analyzedData[$field['id']] = $perMatchString;
          } elseif ($field['id'] == 'auto_mobility') {
            $cumulative = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              $autonPathArray = json_decode($matchInfo['a_auton_path'], true);
              for($i=1; $i<count($autonPathArray); $i++) {
                if($autonPathArray[$i]['keep']==true && $autonPathArray[$i]['col']>2 && $autonPathArray[$i]['col']<9) {
                  $cumulative++;
                  break;
                }
              }
            }
            $analyzedData[$field['id']] = $cumulative;
          } elseif ($field['id'] == 'auto_heatmap') {
            $cumulativeHeatmap = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]];
            foreach($matchList as $matchNum => $matchInfo) {
              $autonPathArray = json_decode($matchInfo['a_auton_path'], true);
              for($i=1; $i<count($autonPathArray); $i++) {
                if($autonPathArray[$i]['keep'] === 'true') {
                  $col = $autonPathArray[$i]['col'];
                  $row = $autonPathArray[$i]['row'];
                  if($autonPathArray[$i]['col']>5) {
                    $col = 11-$col;
                    $row = 6-$row;
                  }
                  $cumulativeHeatmap[$row-1][$col-1]++;
                }
              }
            }

            $heatmapString='[{}]';
            for($i=0; $i<5; $i++) {
              for($j=0;$j<5;$j++) {
                if($cumulativeHeatmap[$i][$j]>0) {
                  $heatmapString = substr($heatmapString, 0, -1) . ',{"row":'.($i+1).',"col":'.($j+1).',"count":'.$cumulativeHeatmap[$i][$j].'}]';
                }
              }
            }
            $analyzedData[$field['id']] = $heatmapString;
          }

          //Cycle times stuff
          elseif ($field['id'] == 'avg_num_cycles') {
            $cumulative = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              if(count($cyclesArray)>1) {
                $cumulative += $cyclesArray[count($cyclesArray)-1]['cycleNum'];
              }
            }
            if(count($matchList)>0) {
              $analyzedData[$field['id']] = $cumulative/count($matchList);
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif ($field['id'] == 'avg_lap_time') {
            $totalTime = 0;
            $numLaps = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              for($i=1; $i<count($cyclesArray); $i++) {
                $numLaps++;
                $totalTime += $cyclesArray[$i]['duration'];
              }
            }
            if($numLaps>0) {
              $analyzedData[$field['id']] = $totalTime/$numLaps;
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif ($field['id'] == 'avg_num_laps') {
            $cumulative = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              $cumulative += count($cyclesArray)-1;
            }
            if(count($matchList)>0) {
              $analyzedData[$field['id']] = $cumulative/count($matchList);
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif ($field['id'] == 'avg_cycle_time') {
            $numCycles = 0;
            $totalTime = 0;
            foreach($matchList as $matchNum => $matchInfo ) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              for($i=1; $i<count($cyclesArray); $i++) {
                if($cyclesArray[$i]['score']=='true') {
                  if($cyclesArray[$i]['cycleNum'] == 1) {
                    $totalTime += ($cyclesArray[$i]['endTime']-$cyclesArray[1]['startTime'])/1000;
                  } else {
                    $preTime = $cyclesArray[$i-$cyclesArray[$i]['lapNum']]['endTime'];
                    $totalTime += ($cyclesArray[$i]['endTime']-$preTime)/1000;
                  }
                  $numCycles++;
                }
              }
            }
            if($numCycles>0) {
              $analyzedData[$field['id']] = $totalTime/$numCycles;
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif ($field['id'] == 'lap_acc') {
            $cumulativeLaps = 0;
            $cumulativeCycles = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              if(count($cyclesArray)>1) {
                $cumulativeCycles += $cyclesArray[count($cyclesArray)-1]['cycleNum'];
                $cumulativeLaps += count($cyclesArray)-1;
              }
            }
            if($cumulativeLaps>0) {
              $analyzedData[$field['id']] = $cumulativeCycles/$cumulativeLaps;
            } else {
              $analyzedData[$field['id']] = -1;
            }
          }

          //Standard deviations of cycles and laps
          elseif($field['id'] == 'cycle_time_sd') {
            $cmlSqSum = 0;
            $count = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              for($i=1; $i<count($cyclesArray); $i++) {
                if($cyclesArray[$i]['score']=='true') {
                  $time;
                  if($cyclesArray[$i]['cycleNum'] == 1) {
                    $time = ($cyclesArray[$i]['endTime']-$cyclesArray[1]['startTime'])/1000;
                  } else {
                    $preEndTime = $cyclesArray[$i-$cyclesArray[$i]['lapNum']]['endTime'];
                    $time = ($cyclesArray[$i]['endTime']-$preEndTime)/1000;
                  }
                  $cmlSqSum += pow($time-$analyzedData[$field['averageValue']],2);
                  $count++;
                }
              }
            }
            if($count>0) {
              $analyzedData[$field['id']] = sqrt($cmlSqSum/$count);
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif($field['id'] == 'lap_time_sd') {
            $cmlSqSum = 0;
            $count = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              for($i=1; $i<count($cyclesArray); $i++) {
                $count++;
                $cmlSqSum += pow($cyclesArray[$i]['duration']-$analyzedData[$field['averageValue']],2);
              }
            }
            if($count>0) {
              $analyzedData[$field['id']] = sqrt($cmlSqSum/$count);
            } else {
              $analyzedData[$field['id']] = -1;
            }
          }

          //Scale first 3 cubes
          elseif($field['id'] == 'first_three_scl_avg') {
            $numMatches=0;
            $cumulativeTime=0;
            foreach($matchList as $matchNum => $matchInfo) {
              $cyclesArray = json_decode($matchInfo['cycle_times'], true);
              if(count($cyclesArray)>3) {
                $tempTime=0;
                $sclCount=0;
                for($i=1; $i<count($cyclesArray); $i++) {
                  if($cyclesArray[$i]['score']=='true' && $cyclesArray[$i]['scoreZone'] == 'Scl') {
                    $sclCount++;
                    if($cyclesArray[$i]['cycleNum'] == 1) {
                      $tempTime += ($cyclesArray[$i]['endTime']-$cyclesArray[1]['startTime'])/1000;
                    } else {
                      $preEndTime = $cyclesArray[$i-$cyclesArray[$i]['lapNum']]['endTime'];
                      $tempTime += ($cyclesArray[$i]['endTime']-$preEndTime)/1000;
                    }
                    if($sclCount==3) {
                      break;
                    }
                  }
                }
                if($sclCount==3) {
                  $cumulativeTime+=$tempTime;
                  $numMatches++;
                }
              }
            }
            if($numMatches>0) {
              $analyzedData[$field['id']] = $cumulativeTime/$numMatches;
            } else {
              $analyzedData[$field['id']] = -1;
            }
          }

          //Specific scoring element average num cycles
          elseif($field['id'] == 'avg_num_scale') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgNum($matchList, 'Scl');
          } elseif($field['id'] == 'avg_num_home_switch') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgNum($matchList, 'HSw');
          } elseif($field['id'] == 'avg_num_away_switch') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgNum($matchList, 'ASw');
          } elseif($field['id'] == 'avg_num_exchange') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgNum($matchList, 'Xch');
          }

          //Specific scoring element average cycle time
          elseif($field['id'] == 'avg_scale_time') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgTime($matchList, 'Scl');
          } elseif($field['id'] == 'avg_home_switch_time') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgTime($matchList, 'HSw');
          } elseif($field['id'] == 'avg_away_switch_time') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgTime($matchList, 'ASw');
          } elseif($field['id'] == 'avg_exchange_time') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAvgTime($matchList, 'Xch');
          }

          elseif($field['id'] == 'scale_time_sd') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementTimeSd($matchList, 'Scl', $analyzedData[$field['averageValue']]);
          } elseif($field['id'] == 'home_switch_time_sd') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementTimeSd($matchList, 'HSw', $analyzedData[$field['averageValue']]);
          } elseif($field['id'] == 'away_switch_time_sd') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementTimeSd($matchList, 'ASw', $analyzedData[$field['averageValue']]);
          } elseif($field['id'] == 'exchange_time_sd') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementTimeSd($matchList, 'Xch', $analyzedData[$field['averageValue']]);
          }

          //Specific scoring element accuracy
          elseif($field['id'] == 'scale_acc') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAcc($matchList, 'Scl');
          } elseif($field['id'] == 'home_switch_acc') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAcc($matchList, 'HSw');
          } elseif($field['id'] == 'away_switch_acc') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAcc($matchList, 'ASw');
          } elseif($field['id'] == 'exchange_acc') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::specificElementAcc($matchList, 'Xch');
          }

          //Specific scoring element per match
          elseif($field['id'] == 'scale_per_match') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::perMatchSpecificElement($matchList, 'Scl');
          } elseif($field['id'] == 'home_switch_per_match') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::perMatchSpecificElement($matchList, 'HSw');
          } elseif($field['id'] == 'away_switch_per_match') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::perMatchSpecificElement($matchList, 'ASw');
          } elseif($field['id'] == 'exchange_per_match') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::perMatchSpecificElement($matchList, 'Xch');
          }

          //Specific scoring element time histogram
          elseif($field['id'] == 'scale_time_histo') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::timeHisto($matchList, 'Scl');
          } elseif($field['id'] == 'home_switch_time_histo') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::timeHisto($matchList, 'HSw');
          } elseif($field['id'] == 'away_switch_time_histo') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::timeHisto($matchList, 'ASw');
          } elseif($field['id'] == 'exchange_time_histo') {
            $analyzedData[$field['id']] = App\Http\Controllers\AdminController::timeHisto($matchList, 'Xch');
          }

          //Hang stuff
          elseif ($field['id'] == 'num_center_rung_hangs') {
            $total=0;
            foreach($matchList as $matchNum => $matchInfo) {
              if($matchInfo['t_hang_pos']=="Center Rung") {
                $total++;
              }
            }
            $analyzedData[$field['id']] = $total;
          } elseif ($field['id'] == 'num_carrier_hangs') {
            $total=0;
            foreach($matchList as $matchNum => $matchInfo) {
              if($matchInfo['t_hang_pos']=="Carrier") {
                $total++;
              }
            }
            $analyzedData[$field['id']] = $total;
          } elseif ($field['id'] == 'num_assissted_hangs') {
            $total=0;
            foreach($matchList as $matchNum => $matchInfo) {
              if($matchInfo['t_hang_pos']=="Carried/Assisted") {
                $total++;
              }
            }
            $analyzedData[$field['id']] = $total;
          } elseif ($field['id'] == 'num_park_hangs') {
            $total=0;
            foreach($matchList as $matchNum => $matchInfo) {
              if($matchInfo['t_hang_pos']=="Park Only") {
                $total++;
              }
            }
            $analyzedData[$field['id']] = $total;
          } elseif ($field['id'] == 'hang_per_match') {
            $hangString = '[{}]';
            foreach($matchList as $matchNum => $matchInfo) {
              $hangString = substr($hangString, 0, -1).',{"matchNum":'.$matchInfo['match_num'].',"hangPos":"'.$matchInfo['t_hang_pos'].'","hangTime":'.$matchInfo['t_active_hang'].'}]';
            }
            $analyzedData[$field['id']] = $hangString;
          } elseif ($field['id'] == 'active_hang_avg') {
            $cmlTime = 0;
            $count=0;
            foreach($matchList as $matchNum => $matchInfo) {
              if($matchInfo['t_active_hang']>0) {
                $cmlTime += $matchInfo['t_active_hang'];
                $count++;
              }
            }
            if($count>0) {
              $analyzedData[$field['id']] = $cmlTime/$count;
            } else {
              $analyzedData[$field['id']] = -1;
            }
          } elseif($field['id'] == 'active_hang_sd') {
            $cmlSqSum = 0;
            $count = 0;
            foreach($matchList as $matchNum => $matchInfo) {
              if($matchInfo['t_active_hang']>0) {
                $cmlSqSum += pow($matchInfo['t_active_hang']-$analyzedData[$field['averageValue']],2);
                $count++;
              }
            }
            if($count>0) {
              $analyzedData[$field['id']] = sqrt($cmlSqSum/$count);
            } else {
              $analyzedData[$field['id']] = -1;
            }
          }

          elseif($field['id']== 'match_comments'){
            $commentsString='[{}]';
            foreach($matchList as $matchNum => $matchInfo){
              $matchID = $matchNumList[$matchNum];
              $correctMatch;
              foreach($matchRows as $match) {
                if($match['id']==$matchID) {
                  $correctMatch = $match;
                }
              }
              $scout = App\User::where('id', $correctMatch['scout_id'])->firstOrFail();
              $scoutName = $scout->first_name.' '.$scout->last_name;
              $commentsString=  substr($commentsString,0,-1).',{"matchNum":'.$matchInfo['match_num'].',"scout":"'.$scoutName.'","comments":"'.$matchInfo['p_comments'].'"}]';
            }
            $analyzedData[$field['id']] = $commentsString;
          }

          //Everything else
          else {
            $operandsCollection[$field['id']] = [];
            foreach($field['operands'] as $operand) {
              foreach($matchList as $matchNum => $matchInfo) {
                $operandsCollection[$field['id']][''.$matchNum] = $matchInfo[$operand];
              }
            }
            $rawInfo = $operandsCollection[$field['id']];
            $operatorName = $field['operator'];

            $analyzedData[$field['id']] = App\Http\Controllers\WebController::$operatorName($rawInfo);
          }
        }
        $analyzedDataOverall[$team] = $analyzedData;
      }

      return $analyzedDataOverall;
    }

    //Function for specific scoring element average #s in auton
    public static function specificElementAutonAvg($matchList, $scoreElement) {
      $numMatches = 0;
      $cumulativeScored = 0;
      foreach($matchList as $matchNum => $matchInfo) {
        $cubes = json_decode($matchInfo['a_scoring'], true);
        $cumulativeScored += $cubes[$scoreElement.'_hit'];
        if($cubes[$scoreElement.'_hit']>0 || $cubes[$scoreElement.'_miss']>0) {
          $numMatches++;
        }
      }
      if($numMatches>0) {
        return $cumulativeScored/$numMatches;
      } else {
        return -1;
      }
    }

    public static function specificElementAutonAcc($matchList, $scoreElement) {
      $numMissed = 0;
      $numScored = 0;
      foreach($matchList as $matchNum => $matchInfo) {
        $cubes = json_decode($matchInfo['a_scoring'], true);
        $numMissed += $cubes[$scoreElement.'_miss'];
        $numScored += $cubes[$scoreElement.'_hit'];
      }
      if($numMissed+$numScored>0) {
        return $numScored/($numMissed+$numScored);
      } else {
        return -1;
      }
    }

    //Function for specific scoring element average num cycles
    public static function specificElementAvgNum($matchList, $scoreElement) {
      $cumulativeScored = 0;
      foreach($matchList as $matchNum => $matchInfo) {
        $cyclesArray = json_decode($matchInfo['cycle_times'], true);
        for($i=1; $i<count($cyclesArray); $i++) {
          if ($cyclesArray[$i]['score'] == 'true' && $cyclesArray[$i]['scoreZone'] == $scoreElement) {
            $cumulativeScored++;
          }
        }
      }
      if(count($matchList)>0) {
        return $cumulativeScored/count($matchList);
      } else {
        return -1;
      }
    }

    //Function for specific scoring element average cycle time
    public static function specificElementAvgTime($matchList, $scoreElement) {
      $cumulativeTime=0;
      $cycleCount=0;
      foreach($matchList as $matchNum => $matchInfo) {
        $cyclesArray = json_decode($matchInfo['cycle_times'], true);
        for($i=1; $i<count($cyclesArray); $i++) {
          if($cyclesArray[$i]['score'] == 'true' && $cyclesArray[$i]['scoreZone'] == $scoreElement) {
            $cycleCount++;
            if($cyclesArray[$i]['cycleNum'] == 1) {
              $cumulativeTime += ($cyclesArray[$i]['endTime']-$cyclesArray[1]['startTime'])/1000;
            } else {
              $preEndTime = $cyclesArray[$i-$cyclesArray[$i]['lapNum']]['endTime'];
              $cumulativeTime += ($cyclesArray[$i]['endTime']-$preEndTime)/1000;
              //Debug thingy
              // Log::debug('--- teamNum: '.$matchInfo['team_num'].' matchNum: '.$matchInfo['match_num'].' i: '.$i.' j: '.$j.' cycleCount: '.$cycleCount.' cumulativeTime: '.$cumulativeTime.' $cyclesArray[$j]["cycleNum"]: '.$cyclesArray[$j]['cycleNum'].' $cyclesArray[$i]["cycleNum"]: '.$cyclesArray[$i]['cycleNum']);
            }
          }
        }
      }
      if($cycleCount>0) {
        return $cumulativeTime/$cycleCount;
      } else {
        return -1;
      }
    }

    public static function specificElementTimeSd($matchList, $scoreElement, $avgValue) {
      $cmlSqSum = 0;
      $count = 0;
      foreach($matchList as $matchNum => $matchInfo) {
        $cyclesArray = json_decode($matchInfo['cycle_times'], true);
        for($i=1; $i<count($cyclesArray); $i++) {
          if($cyclesArray[$i]['score'] == 'true' && $cyclesArray[$i]['scoreZone'] == $scoreElement) {
            $time;
            if($cyclesArray[$i]['cycleNum'] == 1) {
              $time = ($cyclesArray[$i]['endTime']-$cyclesArray[1]['startTime'])/1000;
            } else {
              $preEndTime = $cyclesArray[$i-$cyclesArray[$i]['lapNum']]['endTime'];
              $time = ($cyclesArray[$i]['endTime']-$preEndTime)/1000;
            }
            $cmlSqSum += pow($time-$avgValue, 2);
            $count++;
          }
        }
      }
      if($count>0) {
        return sqrt($cmlSqSum/$count);
      } else {
        return -1;
      }
    }

    //Function for specific scoring element accuracy
    public static function specificElementAcc($matchList, $scoreElement) {
      $cumulativeScored = 0;
      $cumulativeMissed = 0;
      foreach($matchList as $matchNum => $matchInfo) {
        $cyclesArray = json_decode($matchInfo['cycle_times'], true);
        for($i=1; $i<count($cyclesArray); $i++) {
          if($cyclesArray[$i]['scoreZone'] == $scoreElement) {
            if($cyclesArray[$i]['score']=='true') {
              $cumulativeScored++;
            } else {
              $cumulativeMissed++;
            }
          }
        }
      }
      if($cumulativeScored+$cumulativeMissed>0) {
        return $cumulativeScored/($cumulativeScored+$cumulativeMissed);
      } else {
        return -1;
      }
    }

    //Function for specific scoring element per match
    public static function perMatchSpecificElement($matchList, $scoreElement) {
      $perMatchString = "[{}]";
      foreach($matchList as $matchNum => $matchInfo) {
        $cyclesArray = json_decode($matchInfo['cycle_times'], true);
        $curMatchNum = $matchInfo['match_num'];
        $hits = 0;
        $misses = 0;
        $isReplay = $matchInfo['p_replay'];

        for($i=1; $i<count($cyclesArray); $i++) {
          if($cyclesArray[$i]['scoreZone'] == $scoreElement) {
            if($cyclesArray[$i]['score'] == 'true') {
              $hits++;
            } else {
              $misses++;
            }
          }
        }

        $perMatchString = substr($perMatchString, 0, -1) . ',{"matchNum":'.$curMatchNum.',"isReplay":"'.$isReplay.'","hits":'.$hits.',"misses":'.$misses.'}]';
      }
      return $perMatchString;
    }

    //Function for specific scoring element time histogram
    public static function timeHisto($matchList, $scoreElement) {
      $timeHistoString = "[{}]";
      $times = [];
      foreach($matchList as $matchNum => $matchInfo) {
        $cyclesArray = json_decode($matchInfo['cycle_times'], true);
        for($i=1; $i<count($cyclesArray); $i++) {
          if($cyclesArray[$i]['score'] == 'true' && $cyclesArray[$i]['scoreZone'] == $scoreElement) {
            if($cyclesArray[$i]['cycleNum'] == 1) {
              $time = ($cyclesArray[$i]['endTime']-$cyclesArray[1]['startTime'])/1000;
            } else {
              $preEndTime = $cyclesArray[$i-$cyclesArray[$i]['lapNum']]['endTime'];
              $time = ($cyclesArray[$i]['endTime']-$preEndTime)/1000;
            }
            $time = (int)floor($time);
            if(array_key_exists($time,$times)) {
              $times[$time]++;
            } else {
              $times[$time] = 1;
            }
          }
        }
      }
      ksort($times);
      if(count($times)>0) {
        end($times);
        $lastTime = key($times);
        reset($times);
        for($i=0; $i<=$lastTime+1; $i++) {
          if(!array_key_exists($i,$times)) {
            $timeHistoString = substr($timeHistoString, 0, -1) . ',{"'.$i.'":0}]';
          } else {
            $timeHistoString = substr($timeHistoString, 0, -1) . ',{"'.$i.'":'.$times[$i].'}]';
          }
        }
      } else {
        $timeHistoString = '[{},{"0":0}]';
      }

      return $timeHistoString;
    }

    public static function calculateAnalyticsTeam($teamNum) {
      //TODO: Replace with getting one row from database
      $analytics = App\Http\Controllers\AdminController::calculateAnalytics();
      if(array_key_exists($teamNum, $analytics)) {
        return $analytics[$teamNum];
      } else {
        return false;
      }
    }

    public function exportOverallJSON(Request $request) {
      $analyzedDataOverall = $this->getAnalytics();

      $output = json_encode($analyzedDataOverall);
      $fileName = 'overall'.time().'.json';
      header('Content-Type: application/json');
      header('Content-Disposition: attachment; filename="'.$fileName.'"');
      echo $output;
    }

    public function exportOverallCSV(Request $request) {
      $analyzedDataOverall = $this->getAnalytics();

      $analyticsConfig = json_decode(Storage::get('analyticsConfig.json'), true);
      $analyticsFields = $analyticsConfig['calculatables'];

      $csvName = 'analytics'.time().'.csv';
      header('Content-Type: application/excel');
      header('Content-Disposition: attachment; filename="'.$csvName.'"');
      $fp = fopen('php://output', 'w');

      $csvHeader = array('team_num');
      foreach($analyticsFields as $fieldName=>$field) {
        array_push($csvHeader, $field['id']);
      }
      fputcsv($fp, $csvHeader);

      foreach($analyzedDataOverall as $teamNum => $data) {
        $row = array('team_num'=>substr($teamNum, 3));
        foreach($data as $key=>$value) {
          if(getType($value)=="array") {
            // Log::error('key:'.$key.'- value:'.implode(' ',$value));
            continue;
            // $row[$key]=$this->implodeKeyValue($value);
            // $row = array_push($row, $this->implodeKeyValue($value));
          } else {
            // Log::error('key:'.$key.'- value:'.$value);
            $row[$key] = $value;
            // $row = array_push($row, $value);
          }
        }
        fputcsv($fp, $row);
      }

      fclose($fp);
    }

    public function exportRawDataCSV() {
      $rows = DB::table('match_data')->where('event_id', App\Library\Events::getCurrentEventID())->orWhereNotNull('deleted_at')->get();
      $csvName = 'rawData'.time().'.csv';
      header('Content-Type: application/excel');
      header('Content-Disposition: attachment; filename="'.$csvName.'"');

      $csvHeader = array('id', 'scout_id', 'created_at', 'updated_at', 'team_num', 'match_num', 'p_replay', 'hsw_miss', 'hsw_hit', 'scl_miss', 'scl_hit', 'xch_miss', 'xch_hit', 'a_auton_path', 't_hang_pos', 't_active_hang', 'p_defense_rating', 'p_defended', 'p_comments', 'cycleNum', 'lapNum','startZone','scoreZone','startTime','endTime','duration','score','mark');

      $fp = fopen('php://output', 'w');
      fputcsv($fp, $csvHeader);

      $tmpRows=$rows->toArray();
      // Log::error("rows type:" . gettype($tmpRows));
      //$tmpRow=$tmpRows[0].toArray();
      //Log::error('row type:' . gettype($tmpRow));
      //Log::error('row :' . $tmpRow);

      foreach($rows as $row) {
        if($row->deleted_at != null) {
          continue;
        }
        $dataArray = json_decode($row->data, true);
        $cubes = json_decode($dataArray['a_scoring'], true);
        $cycles = json_decode($dataArray['cycle_times'], true);
        if(count($cycles)>1) {
          for ($i=1; $i<count($cycles); $i++) {
            $rowInfo = array();
            $rowInfo[0] = $row->id;
            $rowInfo[1] = $row->scout_id;
            $rowInfo[2] = $row->created_at;
            $rowInfo[3] = $row->updated_at;

            $rowInfo[4] = $dataArray['team_num'];
            $rowInfo[5] = $dataArray['match_num'];
            $rowInfo[6] = $dataArray['p_replay'];
            $rowInfo[7] = $cubes['hsw_miss'];
            $rowInfo[8] = $cubes['hsw_hit'];
            $rowInfo[9] = $cubes['scl_miss'];
            $rowInfo[10] = $cubes['scl_hit'];
            $rowInfo[11] = $cubes['xch_miss'];
            $rowInfo[12] = $cubes['xch_hit'];
            $rowInfo[14] = $dataArray['a_auton_path'];
            $rowInfo[15] = $dataArray['t_hang_pos'];
            $rowInfo[16] = $dataArray['t_active_hang'];
            $rowInfo[17] = $dataArray['p_defense_rating'];
            $rowInfo[18] = $dataArray['p_defended'];
            $rowInfo[19] = $dataArray['p_comments'];

            $rowInfo = $rowInfo+$cycles[$i];

            fputcsv($fp, $rowInfo);
          }
        } else {
          $rowInfo = array();
          $rowInfo[0] = $row->id;
          $rowInfo[1] = $row->scout_id;
          $rowInfo[2] = $row->created_at;
          $rowInfo[3] = $row->updated_at;

          $rowInfo[4] = $dataArray['team_num'];
          $rowInfo[5] = $dataArray['match_num'];
          $rowInfo[6] = $dataArray['p_replay'];
          $rowInfo[7] = $cubes['hsw_miss'];
          $rowInfo[8] = $cubes['hsw_hit'];
          $rowInfo[9] = $cubes['scl_miss'];
          $rowInfo[10] = $cubes['scl_hit'];
          $rowInfo[11] = $cubes['xch_miss'];
          $rowInfo[12] = $cubes['xch_hit'];
          $rowInfo[14] = $dataArray['a_auton_path'];
          $rowInfo[15] = $dataArray['t_hang_pos'];
          $rowInfo[16] = $dataArray['t_active_hang'];
          $rowInfo[17] = $dataArray['p_defense_rating'];
          $rowInfo[18] = $dataArray['p_defended'];
          $rowInfo[19] = $dataArray['p_comments'];

          $rowInfo[15] = 'No cycles';

          fputcsv($fp, $rowInfo);
        }
      }
      // $this->putTableCSV($csvHeader, $rows->toArray(), $fp);

      fclose($fp);
    }

    public function putTableCSV($header, $data, $filePointer) {
      foreach($data as $row) {
        $rowInfo = array();
        for($col=0; $col<count($header); $col++) {
          $rowInfo[$col] = $row[$header[$col]];
        }
        fputcsv($filePointer, $rowInfo);
      }
    }

    public function exportCSV(Request $request) {
      //Get data
      $id = $request->id;
      $match = json_decode(App\MatchData::where('id', $id)->firstOrFail(), true);
      $userID = $match['scout_id'];
      $data = json_decode($match['data'], true);
      $cycleTimes = json_decode($data['cycle_times'], true);
      $createTime = $match['created_at'];

      //Set up php download headers
      $csvName = 'u'.$userID.'_t'.$createTime.'_cycle-times.csv';
      header('Content-Type: application/excel');
      header('Content-Disposition: attachment; filename="'.$csvName.'"');

      //File pointer, file header
      $fp = fopen('php://output', 'w');
      fputcsv($fp, array('lapNum', 'startZone', 'scoreZone', 'startTime', 'endTime', 'duration', 'score', 'mark'));

      for($i=1; $i<count($cycleTimes); $i++) {
        //Write to csv
        fputcsv($fp, $cycleTimes[$i]);
      }

      fclose($fp);
    }

    public function showUsers() {
      $users = App\User::where('is_access_token', false)->get();
      return view('pages.users', ['pageTitle' => 'Users','title' => 'Users', 'users' => $users]);
    }

    public function showUserEdit($id) {
        $user = App\User::where([['id', '=', $id], ['is_access_token', '=', false]])->firstOrFail();
        return view('pages.user', ['pageTitle' => 'User '.$id.' Edit','title' => "Edit User Account", 'user' => $user]);
    }

    public function userEditCallback(Request $request) {
      //dd($request);
      $this->validate($request, [
       'display_name' => 'required|max:255|string',
       'first_name' => 'required|max:255|string|alpha_num',
       'last_name' => 'required|max:255|string|alpha_num',
       'email' => 'required|email',
       'role' => 'required|max:7|string|alpha_num',
       'id' => 'integer|required'
      ]);

      $user = App\User::where('id', $request->id)->firstOrFail();
      if($user->email == "grantchesney@crescentschool.org" || strlen($user->google_id) == 10) {
          return view('nope');
      }
      $user->display_name = $request->display_name;
      $user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->email = $request->email;

      $request->role == 'admin' ? $user->role = 'admin' : $user->role = 'student';
      isset($request->enabled) ? $user->enabled = true : $user->enabled = false;


      $user->save();

      $request->session()->flash('success', 'Update successful!');
      return redirect(route('users'));
    }

    public function showUserInfo($id) {
      $user = App\User::where([['is_access_token', '=', false], ['id', '=', $id]])->firstOrFail();
      $matchData = App\MatchData::where('scout_id', $user->id)->orderBy('created_at','asc')->get();
      $robotPhotos = App\RobotPhotos::where('scout_id', $user->id)->orderBy('created_at','asc')->get();

      return view('pages.userinfo', ['pageTitle' => 'User '.$id.' Info','title'=> 'User Info', 'userData' => $user, 'matchData' => $matchData, 'robotPhotos' => $robotPhotos]);
    }

    public function showAccessTokens() {
      $tokens =  App\User::where('is_access_token', true)->get();
      return view('pages.accesstokens', ['pageTitle' => 'Access Tokens','title' => 'Access Tokens', 'tokens' => $tokens]);
    }

    public function showAccessTokensNew() {
      return view('pages.accesstokensnew', ['pageTitle'=>'Access Token New','title' => 'Create An Access Token']);
    }

    public function accessTokensCallback(Request $request) {
      $this->validate($request, [
       'display_name' => 'required|max:255|string',
       'first_name' => 'required|max:255|string|alpha_num',
       'last_name' => 'required|max:255|string|alpha_num',
       'role' => 'required|max:7|string',
       'duration' => 'required|numeric|max:5|min:1',
      ]);

      $tokenString = $this->generateRandomString();

      $user = new App\User();

      $user->google_id = Auth::User()->id;
      $user->email = $tokenString;
      $user->is_access_token = true;
      $user->token = $tokenString;
      $user->expiry = Carbon::now()->addDays($request->duration )->toDateTimeString();
      $user->enabled = true;
      $user->avatar = "https://eliaslealblog.files.wordpress.com/2014/03/user-200.png";


      $user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->display_name = $request->display_name;
      $request->role == 'admin' ? $user->role = 'admin' : $user->role = 'student';

      $user->save();

      $request->session()->flash('success', 'You have successfuly generated a token: ' . $tokenString);
      return redirect(route('admin.accesstokens'));
    }

    public function accessTokensDelete(Request $request, $id) {
      try{
        $tokenUser = App\User::where([['id', '=', $id], ['is_access_token', '=', true]])->firstOrFail();
      } catch (Exception $e) {
        $request->session()->flash('error', 'Sorry, we cant find that token.');
        return back();
      }

      $tokenUser->delete();
      $request->session()->flash('success', 'You have successfuly deleted token ' . $id);
      return redirect(route('admin.accesstokens'));
    }

    public function showEventSettings() {
      $events = App\Events::all();
      return view('pages.events', ['pageTitle' => 'Events','title' => 'Events', 'events' => $events]);
    }

    public function showEventsNew() {
      return view('pages.newevent', ['pageTitle'=>'Add Event','title' => 'Add A New Event']);
    }

    public function showEventsNewCallback(Request $request) {
      $this->validate($request, [
       'event_code' => 'max:10|string',
      ]);

      if(App\Events::where('event', $request->event_code)->count() > 0){
        $request->session()->flash('error', 'Sorry, this event is already in the database.');
        return back();
      }

      if(!App\Library\Events::validateEvents($request->event_code)) {
        $request->session()->flash('error', 'Sorry, this seems to be an invalid event code.');
        return back();
      }

      $event = new App\Events();
      $event->event = $request->event_code;
      $event->teams = json_encode(App\Library\Teams::getTeams($request->event_code));
      $event->match_schedule = json_encode(App\Library\Events::getEventMatches($request->event_code));
      $event->expert_scouting = json_encode([]);
      $event->analytics = json_encode([]);
      $event->robot_photos = json_encode([]);
      $event->is_current_event = false;

      $event->save();

      $request->session()->flash('success', 'You have successfuly added an event to the event list.');
      return redirect(route('admin.events'));
    }

    public function setCurrentEvent(Request $request, $id) {
      $events = App\Events::all();
      foreach($events as $event){
        if($id == $event->id) {
          $event->is_current_event = true;
        } else {
          $event->is_current_event = false;
        }
        $event->save();
      }
      $request->session()->flash('success', 'You have successfuly set event "' . App\Events::where('id', $id)->firstOrFail()->event . '" as the current event.');
      return redirect(route('admin.events'));
    }

    public function updateEvent($id, Request $request) {
        $event = App\Events::where('id', $id)->firstOrFail();
        $event->teams = json_encode(App\Library\Teams::getTeams($event->event));
        $event->match_schedule = json_encode(App\Library\Events::getEventMatches($event->event));
        $event->save();
        $request->session()->flash('success', 'You have successfuly updated event "' . App\Events::where('id', $id)->firstOrFail()->event . '" to the latest information.');
        return redirect(route('admin.events'));
    }

    public function deleteEvent($id, Request $request) {
      $event = App\Events::where('id', $id)->firstOrFail();
      $eventName = $event->event;
      $event->delete();
      $request->session()->flash('success', 'You have successfuly deleted event "' . $eventName . '".');
      return redirect(route('admin.events'));
    }

    public function showTeams() {
        $teams = App\Library\Teams::getTeamsFromCurEvent();
        if (count($teams) > 0) {
          usort($teams, function ($a, $b) {
            return $a['team_number'] > $b['team_number'];
          });
        }
        $teams1 = array();
        $teams2 = array();
        $teams3 = array();
        $count = 1;
        foreach ($teams as $key => $value) {
          if (($count) % 3 == 0) {
            array_push($teams3, $teams[$key]);
          } elseif(($count)%3==1) {
            array_push($teams1, $teams[$key]);
          } else {
            array_push($teams2, $teams[$key]);
          }
          $count++;
        }
        return view('pages.teams', ['pageTitle'=>'Team List','title' => 'Teams List', 'teams1' => $teams1, 'teams2' => $teams2, 'teams3' => $teams3]);
    }

    public function showMatches() {
      if(!App\Library\Events::isCurrentEventSet()) {
        return view('pages.matches', ['pageTitle'=>'Matches','title'=> 'Matches', 'error' => 'Sorry, the there is no current event set.']);
      }

      $matches = json_decode(App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail()->match_schedule);
      //TODO: Fix this stupid line ^

      foreach($matches as $key => $match) {
        if($match->comp_level != 'qm') {
          unset($matches[$key]);
        }
      }

      usort($matches, function ($a, $b) {
          return $a->match_number - $b->match_number;
      });

      return view('pages.matches', ['pageTitle'=>'Matches','matches' => $matches, 'title' => 'Matches']);
    }

    public function showMatch($id) {
      $matchTeams = App\Http\Controllers\WebController::getMatch($id);
      if($matchTeams==false) {
        $request->session()->flash('error', 'Sorry, there is something wrong with this match number');
        return back();
      }

      $analyticsTable = $this->getAnalytics();
      $analyticsTable2 = [];

      $x = 0;
      $dataNames = [
          'auto_scl_scored_avg',//0
          'auto_hsw_scored_avg',
          'auto_xch_scored_avg',
          'avg_num_cycles',
          'avg_num_scale',
          'avg_num_home_switch',//5
          'avg_num_away_switch',
          'avg_num_exchange',
          'num_center_rung_hangs',//8
          'num_carrier_hangs',
          'num_assissted_hangs'
      ];
      foreach($matchTeams as $teamNum) {

          $matchRows = json_decode(DB::table('match_data')->where('event_id', App\Library\Events::getCurrentEventID())->where('team_id', $teamNum)->whereNull('deleted_at')->get(), true);
          $matchList = [];
          foreach($matchRows as $matchNum => $matchData) {
            $matchList[$matchNum] = json_decode($matchData['data'], true)['match_num'];
          }

          // $analyticsTable2[substr($teamNum, 3)] = $analyticsTable[$teamNum];
          $analyticsTable2[substr($teamNum, 3)]['avg_num_auto'] = round(max($analyticsTable[$teamNum][$dataNames[0]], 0) + max($analyticsTable[$teamNum][$dataNames[1]], 0) + max($analyticsTable[$teamNum][$dataNames[2]], 0), 2);
          for ($i = 3; $i < 8; $i++) {
              $analyticsTable2[substr($teamNum, 3)][$dataNames[$i]] = round(max($analyticsTable[$teamNum][$dataNames[$i]], 0), 2);
          }
          $analyticsTable2[substr($teamNum, 3)]['hang_acc'] =
            round( ($analyticsTable[$teamNum][$dataNames[8]] +
            $analyticsTable[$teamNum][$dataNames[9]] +
            $analyticsTable[$teamNum][$dataNames[10]]) /
            max(count($matchList), 1), 2);

        if ($x > 2) {
            $matchTeams['red'.($x%3+1)] = substr($matchTeams['red'.($x%3+1)], 3);
        }
        else {
            $matchTeams['blue'.($x%3+1)] = substr($matchTeams['blue'.($x%3+1)], 3);
        }

        $x = $x + 1;
      }

      // dd($analyticsTable2);
      // $matchTeams = array_keys($analyticsTable2);
      // dd($matchTeams);

      //TODO Finish off showMatch(), send all of the data correctly.
      return view('pages.teamsmatch', ['pageTitle' => 'Match '.$id,'title' => 'Match '.$id, 'teamList' => $matchTeams, 'dataTable' => $analyticsTable2]);
    }

    public function showRawData(Request $request) {
      $currentEventID = App\Library\Events::getCurrentEventID();
      $matches = App\MatchData::where('event_id', $currentEventID)->orderBy('created_at','asc')->get();
      return view('pages.raw', ['pageTitle'=>'Raw','title' => 'Raw Match Data', 'matches' => $matches]);
    }

    public function deleteMatch($id, Request $request) {
      $match = App\MatchData::where('id', $id)->firstOrFail();
      $match->delete();
      $request->session()->flash('success', 'You have successfuly deleted match "' . $id .'"');
      return redirect(route('admin.raw'));
    }

    public function showSideBySide() {
      return view('pages.sidebyside', ['pageTitle'=>'Side-by-side','title' => 'Side by Side']);
    }

    //Obsolete and broken
    public function sideBySideCallback(Request $request) {
      $analytics = $this->calculateAnalytics();
      //TODO change to get from database
      $team1 = filter_var($request->team1, FILTER_SANITIZE_STRING);
      $team2 = filter_var($request->team2, FILTER_SANITIZE_STRING);
      if(!array_key_exists('frc'.$team1, $analytics)) {
        $request->session()->flash('error', 'No data found for team '.$team1.'.');
        return back();
      }
      if(!array_key_exists('frc'.$team2, $analytics)) {
        $request->session()->flash('error', 'No data found for team '.$team2.'.');
        return back();
      }
      $analytics1 = $analytics['frc'.$team1];
      $analytics2 = $analytics['frc'.$team2];

      $analyticsConfig = json_decode(Storage::get('analyticsConfig.json'), true);
      $groups = $analyticsConfig['sideBySide'];

      return view('pages.sidebysidecallback', ['pageTitle'=>'Compare '.$team1.', '.$team2,'title' => 'Side by Side', 'team1Data' => $analytics1, 'team2Data' => $analytics2, 'team1Num' => $team1, 'team2Num' => $team2, 'displayGroups' => $groups]);
    }

    //Obsolete
    public function showOverall(Request $request) {
        $analytics = App\Analytics::get();
        $auton_gears = filter_var($request->auton_gears, FILTER_SANITIZE_NUMBER_INT);
        $teleop_gears = filter_var($request->teleop_gears, FILTER_SANITIZE_NUMBER_INT);
        $auton_high_goal = filter_var($request->auton_high_goal, FILTER_SANITIZE_NUMBER_INT);
        $teleop_high_goal = filter_var($request->teleop_high_goal, FILTER_SANITIZE_NUMBER_INT);
        $hang = filter_var($request->hang, FILTER_SANITIZE_NUMBER_INT);
        if ($auton_gears == null) {
            $auton_gears = 1;
        }
        if ($teleop_gears == null) {
            $teleop_gears = 1;
        }
        if ($auton_high_goal == null) {
            $auton_high_goal = 1;
        }
        if ($teleop_high_goal == null) {
            $teleop_high_goal = 1;
        }
        if ($hang == null) {
            $hang = 1;
        }
        $rankingNumbers = array();
        foreach ($analytics as $analytic) {
                $rankingNumbers[$analytic->id] =  $this->calTeamRankingNumber($analytic, $auton_gears, $teleop_gears, $auton_high_goal, $teleop_high_goal, $hang);
        }
          foreach ($analytics as $key => $value) {
          $analytics[$analytics[$key]->id] = $analytics[$key];
          unset($analytics[$key]);
        }
        arsort($rankingNumbers);
        return view('pages.overall', ['title' => 'Overall'], compact('analytics', 'rankingNumbers', 'request'));
    }

    public function showOverallPage(Request $request) {
      $analyticsTable = $this->getAnalytics();
      $teams=array_keys($analyticsTable);
      for($i=0; $i<count($teams); $i++) {
        $teams[$i] = (int)(substr($teams[$i],3));
      }
      sort($teams);
      $analyticsTable2 = [];
      $number610 = [];
      foreach($teams as $teamNum) {
        $analyticsTable2[$teamNum] = $analyticsTable['frc'.$teamNum];

        $number610[$teamNum] =
        $analyticsTable2[$teamNum]['auto_hsw_scored_avg'] * 7  +
        $analyticsTable2[$teamNum]['auto_xch_scored_avg'] * 5  +
        $analyticsTable2[$teamNum]['auto_scl_scored_avg'] * 10 +

        $analyticsTable2[$teamNum]['avg_num_scale'] * 15 +
        $analyticsTable2[$teamNum]['avg_num_home_switch'] * 7 +
        $analyticsTable2[$teamNum]['avg_num_away_switch'] * 7 +
        $analyticsTable2[$teamNum]['avg_num_exchange'] * 10 +
         max(($analyticsTable2[$teamNum]['num_center_rung_hangs'] + $analyticsTable2[$teamNum]['num_carrier_hangs'] + $analyticsTable2[$teamNum]['num_assissted_hangs']) / 12, 0) * 30;
      }

      $analyticsConfig = json_decode(Storage::get('analyticsConfig.json'), true);
      $headers = $analyticsConfig["overall"];

      return view('pages.overalltable', ['pageTitle'=>'Overall','title' => 'Overall', 'headers' => $headers, 'dataTable' => $analyticsTable2, 'number610' => $number610]);
    }

    public function showMatchData($id) {
          $match = json_decode(App\MatchData::where('id', $id)->firstOrFail(), true);
          $data = json_decode($match['data'], true);
          $scout = App\User::where('id', $match['scout_id'])->firstOrFail();
          $scoutName = $scout->first_name.' '.$scout->last_name;
          return view('pages.matchdata', ['pageTitle'=>'Match'.$id.'Data','title' => 'MatchData', 'match' => $match, 'data' => $data, 'name' => $scoutName]);
    }

    public function showTeamMatches(Request $request) {
        return view('pages.teammatchselector', ['pageTitle'=>'Team Matches','title' => 'Team Match Lookup']);
    }

    public function teamMatchesCallback(Request $request) {
      $requestTeam = filter_var($request->team, FILTER_SANITIZE_NUMBER_INT);
      $count = 0;
      $teamList = App\Library\Teams::getTeamsFromCurEvent();
      foreach($teamList as $team) {
        if ($team['team_number']==$requestTeam) {
          $count++;
        }
      }
      if($count == 1) {
        return redirect(route('admin.teammatches.id', 'frc' . $requestTeam));
      } else {
        $request->session()->flash('error', 'Sorry, we could not find team ' . $requestTeam);
        return redirect(route('admin.teammatches'));
      }
    }

    public function showTeamMatchesByID($id, Request $request) {
      $teamList = App\Library\Teams::getTeamsFromCurEvent();
      $count = 0;
      foreach($teamList as $team) {
        if ($team['id'] == $id) {
          $count++;
        }
      }
      if($count == 1) {
        $matches = App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail();
        $teamMatches = array();
        foreach($matches as $match) {
          if($match->blue1 == $id){
            array_push($teamMatches, $allMatches[$key]);
          }
          if($match->blue2 == $id){
            array_push($teamMatches, $allMatches[$key]);
          }
          if($match->blue3 == $id){
            array_push($teamMatches, $allMatches[$key]);
          }
          if($match->red1 == $id){
            array_push($teamMatches, $allMatches[$key]);
          }
          if($match->red2 == $id){
            array_push($teamMatches, $allMatches[$key]);
          }
          if($match->red3 == $id){
            array_push($teamMatches, $allMatches[$key]);
          }
        }
        usort($teamMatches, function ($a, $b) {
            return $a['match_number'] > $b['match_number'];
        });
        return view('pages.showteammatches', ['pageTitle'=>'Team Matches','title' => 'Team Matches', 'matches' => $teamMatches]);
      } else {
        $request->session()->flash('error', 'Sorry, we could not find team "' . $id . '"');
        return redirect(route('admin.teammatches'));
      }

    }

    public function deletePhoto($id, Request $request) {
        $photo = App\RobotPhotos::where('id',  $id)->firstOrFail();
        Storage::delete($photo->path);
        $photo->delete();
        $request->session()->flash('success', 'You have successfully deleted a team photo.');
        return back();
    }

    public function recalculate(Request $request) {
      $runner = new App\Http\Controllers\webController();
      $runner->calAnalytics();
      $request->session()->flash('success', 'You have successfully recalculated the analytics.');
      return back();
    }



    public function showExpertScoutingSettings() {

      if(!App\Library\Events::isCurrentEventSet()) {
        return view('pages.expertscoutingsettings', ['title'=> 'Expert Scout Settings', 'error' => 'Sorry, the there is no current event set.']);
      }

      $data = json_decode(App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail()->expert_scouting);
      $users = App\User::where('is_access_token', false)->get();

      return view('pages.expertscoutingsettings', ['pageTitle'=> 'Expert Scout Settings','title'=> 'Expert Scout Settings', 'users' => $users, 'data' => $data]);
    }

    public function expertScoutingSettingsCallback(Request $request) {
        if(!App\Library\Events::isCurrentEventSet()) {
          return view('pages.expertscoutingsettings', ['title'=> 'Expert Scout Settings', 'error' => 'Sorry, the there is no current event set.']);
        }

        $this->validate($request, [
         'id.*' => 'max:255|string',
        ]);

        $data = [];
        $users = App\User::where('is_access_token', false)->get();

        foreach($users as $user) {
          if(isset($request->{"id_".$user->id})) {
            $teams = explode(", ", $request->{"id_".$user->id});
            foreach ($teams as $key => $team) {
              if(!App\Library\Teams::checkTeamNumber(App\Library\Events::getCurrentEventID(), "frc".$team)) {
                unset($teams[$key]);
              }
            }
            $data[$user->id] = $teams;
          } else {
            $data[$user->id] = [];
          }
        }

        $event = App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail();
        //Fix this stupid line ^
        $event->expert_scouting = json_encode($data);
        $event->save();

        $request->session()->flash('success', 'You have successfully updated the expert scouting list!');
        return back();
    }

    public function showLayoutBuilderHome() {

    }

    public function showExportToCSV() {

    }

//_____________________________________________**********************************___________________________________________________________________________________________________________

    function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    static function calTeamRankingNumber($analytics, $auton_gears, $teleop_gears, $auton_high_goal, $teleop_high_goal, $hang)
    {
      return (
                (($auton_gears*$analytics->gears_success_auton_avg) * 60)
              + ($teleop_gears * ((-1/3)* pow($analytics->gears_success_teleop_avg, 2) + ((38/3) * $analytics->gears_success_teleop_avg) + 16))
              + ($auton_high_goal * $analytics->high_goal_shots_auton_avg)
              + (($teleop_high_goal * $analytics->high_goal_shots_teleop_avg) / 3)
              + (40 * ($hang * $analytics->hang_avg)));
    }

}
