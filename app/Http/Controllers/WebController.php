<?php
namespace App\Http\Controllers;

use Input;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Validator;
use Auth;
use Storage;
use DB;
use Log;

class WebController extends Controller
{
    //TODO: write an automated database backup script that takes a snapshot every hour

    public function showIndex()
    {
      //TODO: to be deleted, this variable is not used
      // $currentEvent = App\Settings::where('key', 'currentEvent')->first()->value;
      $matchesScouted = null;
      //TODO: below is legacy logic for 610's upcoming matches, temporarily commmented
      // this should be refactored
      // $matches = App\MatchData::get();
      // $highest_num = 1;
      // foreach ($matches as $match) {
      //   if(substr($match->match_id, 2) > $highest_num) {
      //     $highest_num = substr($match->match_id, 2);
      //   }
      // }
      // $events = App\CurrentEvent::where('match_number', '>', $highest_num)->get();
      $ourevents = array();
      // foreach($events as $event) {
      //   if($event->red1 == 'frc610') {
      //     array_push($ourevents, $event);
      //   }
      //   if($event->red2 == 'frc610') {
      //     array_push($ourevents, $event);
      //   }
      //   if($event->red3 == 'frc610') {
      //     array_push($ourevents, $event);
      //   }
      //   if($event->blue1 == 'frc610') {
      //     array_push($ourevents, $event);
      //   }
      //   if($event->blue2 == 'frc610') {
      //     array_push($ourevents, $event);
      //   }
      //   if($event->blue3 == 'frc610') {
      //     array_push($ourevents, $event);
      //   }
      // }
      // usort($ourevents, function ($a, $b) {
      //     return $a->match_number > $b->match_number;
      // });

      $curEvent = App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail()->event;

      //TODO: legacy code for getting the teams that the expert scout is responsible for
      $expertScoutingSettings = json_decode(App\Settings::where('key', 'expertScoutingSettings')->first()->value, true);
      return view('pages.home', ['pageTitle' => 'Home','title' => 'Home - '.$curEvent, 'matchesScouted' => $matchesScouted, 'ourEvents' => $ourevents, 'expertScoutingSettings' => $expertScoutingSettings]);
    }

    public function showNew() {
      $currentEvent = App\Settings::where('key', 'currentEvent')->first();
      return view('pages.new', ['title' => 'New Match', 'currentEvent' => $currentEvent->value]);
    }

    public function newCallback(Request $request) {
      $matchData = App\CurrentEvent::where('match_number', filter_var($request->matchNum, FILTER_SANITIZE_STRING))->get();
      if(count($matchData)  != 1) {
        $request->session()->flash('error', 'Sorry, we cant seem to find that match.');
        return redirect(route('new'));
      }
      $matchData = $matchData[0];
      return view('pages.datainput', ['title' => 'Match Data', 'matchData' => $matchData, 'matchNum' => filter_var($request->matchNum, FILTER_SANITIZE_STRING)]);
    }

    //TODO Get rid of old stuff
    public function submitCallback(Request $request) {
      $currentEvent      = App\Settings::where('key', 'currentEvent')->first()->value;
      //Filter input
      $request->match_num = filter_var ($request->match_num, FILTER_SANITIZE_NUMBER_INT);
      $request->team_num = filter_var ($request->team_num, FILTER_SANITIZE_NUMBER_INT);
      $request->starting_pos_auton = filter_var ($request->starting_pos_auton, FILTER_SANITIZE_STRING);
      $request->gears_attempted_auton = filter_var ($request->gears_attempted_auton, FILTER_SANITIZE_NUMBER_INT);
      $request->gears_success_auton = filter_var ($request->gears_success_auton, FILTER_SANITIZE_NUMBER_INT);
      $request->high_goal_shots_auton = filter_var ($request->high_goal_shots_auton, FILTER_SANITIZE_NUMBER_INT);
      $request->low_goal_auton = filter_var ($request->low_goal_auton, FILTER_SANITIZE_NUMBER_INT);
      $request->gears_attempted_teleop = filter_var ($request->gears_attempted_teleop, FILTER_SANITIZE_NUMBER_INT);
      $request->gears_success_teleop = filter_var ($request->gears_success_teleop, FILTER_SANITIZE_NUMBER_INT);
      $request->high_goal_shots_teleop = filter_var ($request->high_goal_shots_teleop, FILTER_SANITIZE_NUMBER_INT);
      $request->hang = filter_var ($request->hang, FILTER_SANITIZE_NUMBER_INT);
      $request->hang_time = filter_var ($request->hang_time, FILTER_SANITIZE_NUMBER_INT);
      $request->played_defense = filter_var ($request->played_defense, FILTER_SANITIZE_NUMBER_INT);
      $request->pressure = filter_var ($request->pressure, FILTER_SANITIZE_NUMBER_INT);
      $request->only_shooting = filter_var ($request->only_shooting, FILTER_SANITIZE_NUMBER_INT);
      $request->defended = filter_var ($request->defended, FILTER_SANITIZE_NUMBER_INT);
      $request->comments = filter_var ($request->comments, FILTER_SANITIZE_STRING);
      $request->hang_pos = filter_var ($request->hang_pos, FILTER_SANITIZE_STRING);
      $request->replay = filter_var ($request->replay, FILTER_SANITIZE_NUMBER_INT);

      $game = new App\MatchData();

      $game->scout_email = Auth::User()->email;
      $game->event = $currentEvent;
      $game->team_id = "frc" . $request->team_num;
      $game->team_number = $request->team_num;
      $game->match_number = $request->match_num;
      if($request->replay == 1) {
        $game->replay = 1;
      } else {
        $game->replay = 0;
      }
      if($request->replay == 1) {
        $game->match_id = "qm" . $request->match_num . "_1";
      } else {
        $game->match_id = "qm" . $request->match_num;
      }
      $game->id = $currentEvent . "_" . $game->team_id . "_" . "qm" . $request->match_num . "_" . $game->replay . "_" . time();

      $game->starting_pos_auton = $request->starting_pos_auton;
      $game->gears_attempted_auton = (int)$request->gears_attempted_auton;
      $game->gears_success_auton = (int)$request->gears_success_auton;
      $game->high_goal_shots_auton = (int) $request->high_goal_shots_auton;
      if($request->low_goal_auton == 1) {
        $game->low_goal_auton = true;
      } else {
        $game->low_goal_auton = false;
      }

      $game->gears_attempted_teleop = (int) $request->gears_attempted_teleop;
      $game->gears_success_teleop = (int) $request->gears_success_teleop;
      $game->high_goal_shots_teleop = (int) $request->high_goal_shots_teleop;
      if($request->hang == 1) {
          $game->hang = true;
      } else {
          $game->hang = false;
      }
      $game->hang_time = (int)$request->hang_time;
      if($request->played_defense == 1) {
          $game->played_defense = true;
      } else {
          $game->played_defense = false;
      }

      if($request->pressure == 1) {
          $game->pressure = true;
      } else {
          $game->pressure = false;
      }
      if($request->only_shooting == 1) {
          $game->only_shooting = true;
      } else {
          $game->only_shooting = false;
      }
      if($request->defended == 1) {
          $game->defended = true;
      } else {
          $game->defended = false;
      }
      $game->comments = $request->comments;
      $game->hang_pos = $request->hang_pos;
      $running = true;

      DB::beginTransaction();
        $game->save();
      DB::commit();

      $this->calAnalytics();
      $request->session()->flash('success', 'You have successfuly submited match data!');
      return redirect(route('index'));
    }

    private function getNewKeyPath ($keyPath, $key) {
        return $keyPath . '[' . $key . ']';
    }

    private function createLine($name, $longName, $value, $type, $level) {
        $line=array();
        $line['name']=$name;
        $line['long_name']=$longName;
        $line['value']=$value;
        $line['type']=$type;
        $line['level']=$level;
        return $line;

    }

    private function convertKeyValuesIntoLines($key, $value, $level,$keyPath) {
        $lines=array();
        if (!is_array($value)){
            $decoded_value = json_decode($value, true);

            if ($decoded_value != null && is_array($decoded_value)) {
                $value = $decoded_value;
            }
        }

        if (is_array($value)) {
            $line=$this->createLine($key,$this->getNewKeyPath($keyPath,$key), $key, 'display' , $level);


            array_push($lines,$line);
            $newkeyPath=$this->getNewKeyPath($keyPath,$key);
            foreach ($value as $newkey => $newvalue) {
                if (!empty($newvalue)||$newvalue==0) {
                    $new_lines = $this->convertKeyValuesIntoLines($newkey, $newvalue, $level + 1, $newkeyPath);
                    $lines=array_merge($lines, $new_lines);
                }
                else {
                    $line=$this->createLine($newkey,$this->getNewKeyPath($newkeyPath,$newkey).'[]', $newkey, 'hidden' , $level);
                    array_push($lines, $line);
                }
            }
        }
        else {
            $line=$this->createLine($key,$this->getNewKeyPath($keyPath,$key), $value, 'edit' , $level);
            array_push($lines, $line);
        }
        return $lines;
    }

    private function convertArrayIntoLines($input)
    {
        $output = array();
        if (is_array($input)) {
            foreach ($input as $key => $value) {
                $lines = $this->convertKeyValuesIntoLines($key, $value,1,'match');
                $output=array_merge($output, $lines);
            }
        }
        else {
            $line=$this->createLine('line','match', $input, 'edit' , 1);
            array_push($output,$line);
        }
        return $output;
    }


    public function showEdit($id) {
//      $match = App\MatchData::where('id', $id)->firstOrFail();
//      return view('pages.edit', ['data' => $match, 'title' => 'Edit Match']);

        $match_json = App\MatchData::where('id', $id)->firstOrFail();
        $match_decoded=json_decode($match_json, true);
        $match_data=json_decode($match_decoded['data'],true);
        if ($match_data ==null) {
            return view('pages.json_edit', ['error' => json_last_error(), 'title' => 'Edit Match']);

        }
        $lines=$this->convertArrayIntoLines($match_data);
        return view('pages.json_edit', ['pageTitle' => 'Edit', 'oldID' => $match_decoded['id'], 'scoutID' => $match_decoded['scout_id'], 'eventID' => $match_decoded['event_id'], 'teamID' => $match_decoded['team_id'], 'lines' => $lines, 'error_message'=>null, 'title' => 'Edit Match']);
    }

    //TODO Get rid of old stuff
    public function editCallback(Request $request) {
        $currentEvent = App\Settings::where('key', 'currentEvent')->first()->value;
        //Filter input

        $match_data = $request->match;
        $match_data['cycle_times']=json_encode($match_data['cycle_times']);
        $match_data['a_auton_path']=json_encode($match_data['a_auton_path']);
        $match_data['a_scoring']=json_encode($match_data['a_scoring']);
        $match_data_json=json_encode($match_data);

        $data = new App\MatchData();
        $data->scout_id = $request->scout_id;
        $data->event_id = $request->event_id;
        $data->team_id = $request->team_id;
        $data->layout = "";
        $data->data = $match_data_json;
        $data->save();

        $oldMatch = App\MatchData::where('id', $request->old_id)->firstOrFail();
        $oldMatch->delete();

        $request->session()->flash('success', 'You have successfuly edited match '.$request->old_id.'.');
        return redirect(route('admin.raw'));
    }

    public function showUpload($id) {
      $teams = App\Library\Teams::getTeamsFromCurEvent();
      $teamData = $teams[$id];
      return view('pages.teamphotoupload', ['pageTitle' => 'Robot Photos','title' => 'Photo upload for team ' . $teamData['team_number'], 'teamdata' => $teamData]);
    }

    public function showUploadCallback($id, Request $request){
      $teams = App\Library\Teams::getTeamsFromCurEvent();
      $teamData = $teams[$id];
      $this->validate($request, [
          'photo' => 'required | mimes:jpeg,jpg,png | max:50000',
      ]);
      $file = $request->file('photo')->store('robot_photos/' . $id);
      $db = new App\RobotPhotos();
      $db->team_id = $id;
      $db->scout_id = Auth::User()->id;
      $db->path = $file;
      $db->save();

      $header = 'robot_photos/' . $id;
      shell_exec("cd ../storage/app/robot_photos/". $id . "&& jpegoptim " . substr($file, strlen($header) + 1));
      return "success";
    }

    public function showRobotPhotoSelect() {
      $teams = App\Library\Teams::getTeamsFromCurEvent();
      $status = array();
      foreach($teams as $team) {
        if(App\RobotPhotos::where('team_id', $team['key'])->count() >= 1) {
          $status[$team['key']] = 1;
        } else {
          $status[$team['key']] = 0;
        }
      }
      if (count($teams) > 0) {
          usort($teams, function ($a, $b) {
            return $a['team_number'] > $b['team_number'];
          });
      }
      return view('pages.robotphototeamselector', ['pageTitle' => 'Robot Photos','title' => 'Robot Photo Team Selector', 'teams' => $teams, 'status' => $status]);
    }

    public function showRobotPhotoSelectCallback(Request $request) {
      $this->validate($request, [
       'team' => 'required|integer',
      ]);
      $teams = App\Library\Teams::getTeamsFromCurEvent();

      if(!array_key_exists('frc'.$request->team, $teams)) {
        $request->session()->flash('error', 'Sorry, we cant find that team :(');
        return back();
      }
      return redirect(route('team.upload', 'frc' . $request->team));
    }


    public function showTeamPhotos($id) {
      $this->authorize('view-team-info', Auth::User(), $id);
      $team = App\Library\Teams::getTeamsFromCurEvent();
      $photos = App\RobotPhotos::where('team_id', $id)->get();
      return view('pages.photos', ['pageTitle' => 'Photos for '.substr($id,3),'title' => 'Robot Photos for Team ' . substr($id, 3), 'photos' => $photos, 'id' => $id]);
    }

    public static function calSD($data, $avg, $prop) {
      $total = 0;
      if(count($data) > 1){
        if ($prop) {
            $total = sqrt($avg * (1 - $avg) / count($data));
        }
        else {
            foreach($data as $element) {
              $total += pow(($element - $avg), 2);
            }

            $total = $total / (count($data) - 1);
            $total = sqrt($total);
        }

      return $total;
      } else {
        return 0;
      }
    }

    public static function calAVG($data) {
      $total = 0;
      foreach ($data as $element) {
        $total += $element;
      }
      if(count($data) > 1) {
        $total = $total / count($data);
      }
      return $total;
    }

    /*
     *  Previous to this comment: legacy code
     *  From this comment onwards: new code for 2018
     */

    public function viewForm($matchNum, $teamList) {
      $formArray = json_decode(Storage::get('formConfig.json'), true);

      return view('pages.taptap2', [
          'pageTitle' => 'Form for match '.$matchNum,
          'matchNum' => $matchNum,
          'title' => "Match Scouting Form",
          'fields' => $formArray,
          'red1' => $teamList['red1'],
          'red2' => $teamList['red2'],
          'red3' => $teamList['red3'],
          'blue1' => $teamList['blue1'],
          'blue2' => $teamList['blue2'],
          'blue3' => $teamList['blue3'],
        ]);
    }

    public function selectMatchNum() {
      $currentEvent = App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail();
      //TODO: Fix this stupid line ^
      return view('pages.new', ['pageTitle' => 'New Match','title' => 'New Match', 'currentEvent' => $currentEvent->event]);
    }

    public function selectMatchNumCallback(Request $request) {
      $matchNum = filter_var($request->matchNum, FILTER_SANITIZE_STRING);
      $teamList = $this->getMatch($matchNum);
      if ($teamList == false) {
        $request->session()->flash('error', 'Please input a valid match number.');
        return back();
      }
      return $this->viewForm($matchNum, $teamList);
    }

    public function submitFormCallback(Request $request) {
      $formConfig = json_decode(Storage::get('formConfig.json'), true);
      $groups = $formConfig['groups'];
      $values = [];
      foreach($groups as $group) {
      foreach($group['fields'] as $field) {
        $field_id = $field['id'];
        $values["".$field_id]=$request["".$field_id];
      }
      }
      $values['match_num'] = $request['match_num'];
      $values['team_num'] = $request['team_num'];

      $value_JSON = json_encode($values);
      $data = new App\MatchData();
      $data->scout_id = Auth::User()->id;
      $data->data = $value_JSON;
      $data->layout = ""; //TODO: find out what layout does
      $data->event_id = App\Library\Events::getCurrentEventID();
      $data->team_id = "frc".$values['team_num'];
      // $data->id = App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail()->event . "_" . $data->team_id . "_" . "qm" . $values['match_num'] . "_" . $values['p_replay'] . "replay" . "_" . time();
      $data->save();

      $request->session()->flash('success', 'You have successfuly submited match data!');
      return redirect(route('index'));
      // This is just some test text
      //  $testText = $request["match_num"];
      //  $testText2 = $values["p_shooting"];
      // return view('pages.test', [
      //     'testText' => $values['match_num'],
      //     'testText2' => $values['team_num'],
      //     'title' => "Test",
      //   ]);

      //TODO: below is the legacy version of how to submit the data to the database

      // $currentEvent      = App\Settings::where('key', 'currentEvent')->first()->value;
      // //Filter input
      // $request->match_num = filter_var ($request->match_num, FILTER_SANITIZE_NUMBER_INT);
      // $request->team_num = filter_var ($request->team_num, FILTER_SANITIZE_NUMBER_INT);
      // $request->starting_pos_auton = filter_var ($request->starting_pos_auton, FILTER_SANITIZE_STRING);
      // $request->gears_attempted_auton = filter_var ($request->gears_attempted_auton, FILTER_SANITIZE_NUMBER_INT);
      // $request->gears_success_auton = filter_var ($request->gears_success_auton, FILTER_SANITIZE_NUMBER_INT);
      // $request->high_goal_shots_auton = filter_var ($request->high_goal_shots_auton, FILTER_SANITIZE_NUMBER_INT);
      // $request->low_goal_auton = filter_var ($request->low_goal_auton, FILTER_SANITIZE_NUMBER_INT);
      // $request->gears_attempted_teleop = filter_var ($request->gears_attempted_teleop, FILTER_SANITIZE_NUMBER_INT);
      // $request->gears_success_teleop = filter_var ($request->gears_success_teleop, FILTER_SANITIZE_NUMBER_INT);
      // $request->high_goal_shots_teleop = filter_var ($request->high_goal_shots_teleop, FILTER_SANITIZE_NUMBER_INT);
      // $request->hang = filter_var ($request->hang, FILTER_SANITIZE_NUMBER_INT);
      // $request->hang_time = filter_var ($request->hang_time, FILTER_SANITIZE_NUMBER_INT);
      // $request->played_defense = filter_var ($request->played_defense, FILTER_SANITIZE_NUMBER_INT);
      // $request->pressure = filter_var ($request->pressure, FILTER_SANITIZE_NUMBER_INT);
      // $request->only_shooting = filter_var ($request->only_shooting, FILTER_SANITIZE_NUMBER_INT);
      // $request->defended = filter_var ($request->defended, FILTER_SANITIZE_NUMBER_INT);
      // $request->comments = filter_var ($request->comments, FILTER_SANITIZE_STRING);
      // $request->hang_pos = filter_var ($request->hang_pos, FILTER_SANITIZE_STRING);
      // $request->replay = filter_var ($request->replay, FILTER_SANITIZE_NUMBER_INT);

      // $game = new App\MatchData();

      // $game->scout_email = Auth::User()->email;
      // $game->event = $currentEvent;
      // $game->team_id = "frc" . $request->team_num;
      // $game->team_number = $request->team_num;
      // $game->match_number = $request->match_num;
      // if($request->replay == 1) {
      //   $game->replay = 1;
      // } else {
      //   $game->replay = 0;
      // }
      // if($request->replay == 1) {
      //   $game->match_id = "qm" . $request->match_num . "_1";
      // } else {
      //   $game->match_id = "qm" . $request->match_num;
      // }
      // $game->id = $currentEvent . "_" . $game->team_id . "_" . "qm" . $request->match_num . "_" . $game->replay . "_" . time();

      // $game->starting_pos_auton = $request->starting_pos_auton;
      // $game->gears_attempted_auton = (int)$request->gears_attempted_auton;
      // $game->gears_success_auton = (int)$request->gears_success_auton;
      // $game->high_goal_shots_auton = (int) $request->high_goal_shots_auton;
      // if($request->low_goal_auton == 1) {
      //   $game->low_goal_auton = true;
      // } else {
      //   $game->low_goal_auton = false;
      // }

      // $game->gears_attempted_teleop = (int) $request->gears_attempted_teleop;
      // $game->gears_success_teleop = (int) $request->gears_success_teleop;
      // $game->high_goal_shots_teleop = (int) $request->high_goal_shots_teleop;
      // if($request->hang == 1) {
      //     $game->hang = true;
      // } else {
      //     $game->hang = false;
      // }
      // $game->hang_time = (int)$request->hang_time;
      // if($request->played_defense == 1) {
      //     $game->played_defense = true;
      // } else {
      //     $game->played_defense = false;
      // }

      // if($request->pressure == 1) {
      //     $game->pressure = true;
      // } else {
      //     $game->pressure = false;
      // }
      // if($request->only_shooting == 1) {
      //     $game->only_shooting = true;
      // } else {
      //     $game->only_shooting = false;
      // }
      // if($request->defended == 1) {
      //     $game->defended = true;
      // } else {
      //     $game->defended = false;
      // }
      // $game->comments = $request->comments;
      // $game->hang_pos = $request->hang_pos;
      // $running = true;

      // DB::beginTransaction();
      //   $game->save();
      // DB::commit();

      // $this->calAnalytics();
      // $request->session()->flash('success', 'You have successfuly submited match data!');
      // return redirect(route('index'));
    }

    public static function getMatch ($matchNum) {
        $matches = json_decode(App\Events::where('id', App\Library\Events::getCurrentEventID())->firstOrFail()->match_schedule);
        //TODO: Fix this stupid line ^
        $matchData=[];
        $matchExists = false;
        foreach ($matches as $match) {
          if ($match->match_number == $matchNum && $match->comp_level == "qm") {
            $matchData = [
              'blue1' => $match->alliances->blue->team_keys[0],
              'blue2' => $match->alliances->blue->team_keys[1],
              'blue3' => $match->alliances->blue->team_keys[2],
              'red1' => $match->alliances->red->team_keys[0],
              'red2' => $match->alliances->red->team_keys[1],
              'red3' => $match->alliances->red->team_keys[2],
            ];
            $matchExists = true;
          }
        }
        if ($matchExists) {
          return $matchData;
        } else {
          return false;
        }
    }

    public static function median($elements) {
      $list = $elements;
      $count = count($list);
      asort($list);
      if ($count > 0) {
        if ($count % 2 == 0) {
          return ($list[$count/2]+$list[$count/2+1]-1)/2;
        } else {
          return $list[PHP_ROUND_HALF_DOWN($count/2)];
        }
      } else {
        return -1;
      }
    }

    public static function mean($elements) {
      $count = count($elements);
      if ($count > 0) {
        $sum = App\Http\Controllers\WebController::total($elements);
        return $sum/$count;
      } else {
        return -1;
      }
    }

    public static function countMatchesYN($elements) {
      $count = 0;
      foreach($elements as $element) {
        if ($element == "Yes") {
          $count++;
        }
      }
      return $count;
    }

    public static function total($elements) {
      $sum = 0;
      foreach($elements as $element) {
        $sum += $element;
      }
      return $sum;
    }

    public static function accuracy($elements) {
      //Will produce a lot of errors!
      $failCount = total($elements[0]);
      $successCount = total($elements[1]);
      $total = $failCount+$successCount;
      if($total > 0) {
        return $successCount/$total;
      } else {
        return 0;
      }
    }

    public static function perMatch($elements) {
      if (count($elements)>0) {
       return $elements;
      } else {
        return 0;
      }
    }

    public static function perMatchYN($elements) {
      if (count($elements) < 1) {
        return 0;
      }
      $numberForm = [];
      foreach($elements as $matchNum => $match) {
        if ($match == 'Yes') {
          $numberForm[$matchNum] = 1;
        } else {
          $numberForm[$matchNum] = 0;
        }
      }
      return $numberForm;
    }

    public function showTeam($id) {
      //TODO Put a better error catch page
      $this->authorize('view-team-info', Auth::User(), $id);

      $matchRows = json_decode(DB::table('match_data')->where('event_id', App\Library\Events::getCurrentEventID())->where('team_id', 'frc'.$id)->whereNull('deleted_at')->get(), true);
      $matchList = [];
      foreach($matchRows as $matchNum => $matchData) {
        $matchList[$matchNum] = json_decode($matchData['data'], true)['match_num'];
      }

      $analytics = App\Http\Controllers\AdminController::calculateAnalyticsTeam('frc'.$id);
      $analyticsConfig = json_decode(Storage::get('analyticsConfig.json'),true);
      //TODO: Figure out how to seperate multiple matches with same match #

      return view('pages.teamstats', ['pageTitle' => substr($id, 3),
          'title' => 'Team '.$id,
          'analytics' => $analytics,
          'displayGroups' => $analyticsConfig['teamStats'],
          'matchList' => $matchList]);
    }

    public function showTeamTest(Request $request, $id) {
      //TODO Put a better error catch page
      $this->authorize('view-team-info', Auth::User(), $id);

      $matchRows = json_decode(DB::table('match_data')->where('event_id', App\Library\Events::getCurrentEventID())->where('team_id', 'frc'.$id)->whereNull('deleted_at')->get(), true);
      $matchList = [];
      foreach($matchRows as $matchNum => $matchData) {
        $matchList[$matchNum] = json_decode($matchData['data'], true)['match_num'];
      }

      $analytics = App\Http\Controllers\AdminController::calculateAnalyticsTeam('frc'.$id);
      if($analytics == false) {
        $request->session()->flash('error', 'Sorry, no data found for Team '.$id.'.');
        return back();
      }
      $stripChars = array("\r\n", "\n", "\r");
      $commentStringStripped = str_replace($stripChars, '  ', $analytics['match_comments']);
      $analytics['match_comments'] = $commentStringStripped;
      $analyticsConfig = json_decode(Storage::get('analyticsConfig.json'),true);
      //TODO: Figure out how to seperate multiple matches with same match #

      return view('pages.teamtest', ['pageTitle' => $id,
          'title' => 'Team '.$id,
          'analytics' => $analytics,
          'displayGroups' => $analyticsConfig['teamStats'],
          'matchList' => $matchList]);
    }

    public function cycleTimes($cycleTimeList) {
      foreach($cycleTimeList as $oneMatchCycleTimes) {

      }
    }

    public function calAnalytics() {
      //Get team list
      //For each team, call team analytics
      //Store numbers in database
    }
}
