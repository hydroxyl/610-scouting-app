<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model {

  protected $table = 'teams';

  public $incrementing=false;

  protected $fillable = [
      'id', 'team_name', 'team_number', 'team_location', 'team_nickname', 'events',
  ];

}
