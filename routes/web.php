<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@login']);
Route::get('login/redirect', ['as' => 'login.redirect', 'uses' => 'Auth\AuthController@loginRedirect']);
Route::get('login/callback', ['as' => 'login.callback', 'uses' => 'Auth\AuthController@loginCallback']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
Route::get('login/accesstoken', ['as' => 'login.withaccesstoken', 'uses' => 'Auth\AuthController@loginWithAccessToken']);
Route::post('login/accesstokens', ['as' => 'accesstokens.login', 'uses' => 'Auth\AuthController@loginWithAccessTokenCallback']);

Route::group(['middleware' => 'usercheck'], function () {
Route::group(['middleware' => 'token'], function () {
Route::group(['middleware' => 'auth:web'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'WebController@showIndex']);
    Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'WebController@showEdit']);
    Route::post('edit', ['as' => 'edit.callback', 'uses' => 'WebController@editCallback']);

    Route::get('team/{id}/upload', ['as' => 'team.upload', 'uses' => 'WebController@showUpload']);
    Route::post('team/{id}/upload', ['as' => 'team.upload.callback', 'uses' => 'WebController@showUploadCallback']);
    Route::get('robotphotos/select', ['as' => 'robotphotos.select', 'uses' => 'WebController@showRobotPhotoSelect']);
    Route::post('robotphotos/select', ['as' => 'robotphotos.select.callback', 'uses' => 'WebController@showRobotPhotoSelectCallback']);
    Route::get('team/{id}', ['as' => 'admin.team', 'uses' => 'WebController@showTeamTest']);
    Route::get('teamtest/{id}', ['as' => 'admin.teamtest', 'uses' => 'WebController@showTeam']);
    Route::get('team/{id}/photos', ['as' => 'admin.team.photos', 'uses' => 'WebController@showTeamPhotos']);

    Route::get('form', ['as' => 'form', 'uses' => 'WebController@selectMatchNum']);
    Route::post('form', ['as' => 'form.callback', 'uses' => 'WebController@selectMatchNumCallback']);
    Route::post('submitForm', ['as' => 'submitForm', 'uses' => 'WebController@submitFormCallback']);
});

Route::group(['middleware' => 'admin'], function () {
    Route::get('users', ['as' => 'users', 'uses' => 'AdminController@showUsers']);
    Route::get('user/{id}', ['as' => 'user', 'uses' => 'AdminController@showUserEdit']);
    Route::post('user', ['as' => 'user.callback', 'uses' => 'AdminController@userEditCallback']);
    Route::get('user/{id}/info', ['as' => 'admin.userinfo', 'uses' => 'AdminController@showUserInfo']);
    Route::get('accesstokens', ['as' => 'admin.accesstokens', 'uses' => 'AdminController@showAccessTokens']);
    Route::get('accesstokens/new', ['as' => 'admin.accesstokens.new', 'uses' => 'AdminController@showAccessTokensNew']);
    Route::post('accesstokens', ['as' => 'admin.accesstokens.callback', 'uses' => 'AdminController@accessTokensCallback']);
    Route::get('accesstokens/delete/{id}', ['as' => 'admin.accesstokens.delete', 'uses' => 'AdminController@accessTokensDelete']);

    Route::get('events', ['as' => 'admin.events', 'uses' => 'AdminController@showEventSettings']);
    Route::get('events/new', ['as' => 'admin.events.new', 'uses' => 'AdminController@showEventsNew']);
    Route::post('events/new', ['as' => 'admin.events.new.callback', 'uses' => 'AdminController@showEventsNewCallback']);
    Route::get('events/{id}/setcurrent', ['as' => 'admin.events.setcurrent', 'uses' => 'AdminController@setCurrentEvent']);
    Route::get('events/{id}/update', ['as' => 'admin.events.update', 'uses' => 'AdminController@updateEvent']);
    Route::get('events/{id}/delete', ['as' => 'admin.events.delete', 'uses' => 'AdminController@deleteEvent']);


    Route::get('teams', ['as' => 'admin.teams', 'uses' => 'AdminController@showTeams']);

    Route::get('matches', ['as' => 'admin.matches', 'uses' => 'AdminController@showMatches']);
    Route::get('match/{id}', ['as' => 'admin.match', 'uses' => 'AdminController@showMatch']);

    Route::get('raw', ['as' => 'admin.raw', 'uses' => 'AdminController@showRawData']);
    Route::get('delete/{id}', ['as' => 'admin.deletematch', 'uses' => 'AdminController@deleteMatch']);

    Route::get('sidebyside', ['as' => 'admin.sidebyside', 'uses' => 'AdminController@showSideBySide']);
    Route::post('sidebyside', ['as' => 'admin.sidebyside.callback', 'uses' => 'AdminController@sideBySideCallback']);

    Route::get('overall', ['as' => 'admin.overall', 'uses' => 'AdminController@showOverallPage']);

    Route::get('matchdata/{id}', ['as' => 'admin.matchdata', 'uses' => 'AdminController@showMatchData']);
    Route::get('exportCSV/{id}', ['as' => 'admin.exportCSV', 'uses' => 'AdminController@exportCSV']);

    Route::get('teammatches', ['as' => 'admin.teammatches', 'uses' => 'AdminController@showTeamMatches']);
    Route::post('teammatches', ['as' => 'admin.teammatches.callback', 'uses' => 'AdminController@teamMatchesCallback']);
    Route::get('teammatches/{id}', ['as' => 'admin.teammatches.id', 'uses' => 'AdminController@showTeamMatchesByID']);

    Route::get('team/delete/{id}', ['as' => 'admin.team.delete', 'uses' => 'AdminController@deletePhoto']);


    Route::get('recalculate', ['as' => 'admin.recalculate', 'uses' => 'AdminController@recalculate']);

    Route::get('expertscouting', ['as' => 'admin.expertscoutingsettings', 'uses' => 'AdminController@showExpertScoutingSettings']);
    Route::post('expertscouting', ['as' => 'admin.expertscoutingsettings.callback', 'uses' => 'AdminController@expertScoutingSettingsCallback']);

    Route::get('layoutbuilder', ['as' => 'admin.layoutbuilder.home', 'uses' => 'AdminController@showLayoutBuilder']);

    Route::get('rawExportCSV', ['as' => 'admin.rawExportCSV', 'uses' => 'AdminController@exportRawDataCSV']);
    Route::get('overallExportCSV', ['as' => 'admin.overallExportCSV', 'uses' => 'AdminController@exportOverallCSV']);
    Route::get('overallExportJSON', ['as' => 'admin.overallExportJSON', 'uses' => 'AdminController@exportOverallJSON']);
});
});
});

Route::get('401', ['as' => 'error.401', 'uses' => 'ErrorController@error401']);
Route::get('tokenexplired', ['as' => 'error.te', 'uses' => 'ErrorController@errorte']);
