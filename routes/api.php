<?php

//Api routes
Route::post('status', ['as' => 'api.status', 'uses' => 'APIController@status']);
Route::post('submit', ['as' => 'api.submit', 'uses' => 'APIController@submit']);
