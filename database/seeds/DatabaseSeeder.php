<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Model::unguard();

      $this->call('seed_Global_Settings');
      $this->call('seed_Users');
      DB::table('users')->insert([
        'google_id' => '106734768425516723863', 'first_name' => 'Felix', 'last_name' => 'Yu', 'display_name' => 'Felix Yu', 'email' => 'felixyu@crescentschool.org', 'login_count' => '1', 'role' => 'admin', 'avatar' => 'https://lh6.googleusercontent.com/-Fo1c2p-N_5k/AAAAAAAAAAI/AAAAAAAAALg/cNT2HywF2-g/photo.jpg?sz=50', 'is_access_token' => '0', 'token' => '0000000000', 'expiry' => '', 'enable' => '1'
      ]);

      Model::reguard();
    }
}
