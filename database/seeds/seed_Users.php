<?php

use Illuminate\Database\Seeder;

class seed_Users extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
      DB::table('users')->insert(
      ['google_id' => '113267146575703289897', 'first_name' => 'Grant', 'last_name'=> 'Chesney', 'display_name' => 'Grant Chesney', 'email' => 'grantchesney@crescentschool.org', 'login_count' => '1', 'role' => 'admin', 'avatar' => 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'is_access_token' => '0', 'token' => '0000000000', 'expiry' => '', 'enabled' => '1']
      );

      DB::table('users')->insert(
      ['google_id' => '109773637879087537668', 'first_name' => 'Bret', 'last_name'=> 'Hodgkinson', 'display_name' => 'Bret Hodgkinson', 'email' => 'brethodgkinson@crescentschool.org', 'login_count' => '1', 'role' => 'admin', 'avatar' => 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'is_access_token' => '0', 'token' => '0000000000', 'expiry' => '', 'enabled' => '1']
      );

      DB:table('users')->insert(
        ['google_id' => '106734768425516723863', 'first_name' => 'Felix', 'last_name' => 'Yu', 'display_name' => 'Felix Yu', 'email' => 'felixyu@crescentschool.org', 'login_count' => '1', 'role' => 'admin', 'avatar' => 'https://lh6.googleusercontent.com/-Fo1c2p-N_5k/AAAAAAAAAAI/AAAAAAAAALg/cNT2HywF2-g/photo.jpg?sz=50', 'is_access_token' => '0', 'token' => '0000000000', 'expiry' => '', 'enable' => '1']
      );
    //  DB:table('users')->insert(
      //  ['google_id' => '111113948702491201847', 'first_name' => 'Scott', 'last_name' => 'Williamson', 'display_name' => 'Scott Williamson', 'email' => 'scottwilliamson@crescentschool.org', 'login_count' => '1', 'role' => 'admin', 'avatar' => 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'is_access_token' => '0', 'token' => '0000000000', 'expiry' => '', 'enable' => '1']
      //);
  }
}
