<?php

use Illuminate\Database\Seeder;

class seed_Global_Settings extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('settings')->insert(
      ['key' => 'events', 'value' => '[]']
      );

        DB::table('settings')->insert(
      ['key' => 'currentEvent', 'value' => '[]']
      );

      DB::table('settings')->insert(
      ['key' => 'expertScoutingSettings', 'value' => '[]']
      );

    }
}
