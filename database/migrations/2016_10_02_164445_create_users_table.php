<?php
/*
- Created by: Grant Chesney
- Date: October 2'nd 2016
- Version: 1.0
*/

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {


            $table->increments('id');

            $table->string('google_id');

            $table->string('first_name');

            $table->string('last_name');

            $table->string('display_name');

            $table->string('email')->unique();

            $table->integer('login_count')->default('1');

            $table->enum('role', ['admin', 'student'])->default('student');

            $table->string('avatar');

            $table->string('token')->default('0000000000');

            $table->string('remember_token')->nullable();

            $table->boolean('is_access_token');

            $table->string('expiry')->nullable();

            $table->boolean('enabled');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
