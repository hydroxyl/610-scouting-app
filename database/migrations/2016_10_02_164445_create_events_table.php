<?php
/*
- Created by: Grant Chesney
- Date: October 2'nd 2016
- Version: 1.0
*/

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {


            $table->increments('id');

            $table->string('event');

            $table->json('teams');

            $table->json('expert_scouting');

            $table->json('match_schedule');

            $table->json('analytics');

            $table->json('robot_photos');

            $table->boolean('is_current_event');

            $table->timestamps();

            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
