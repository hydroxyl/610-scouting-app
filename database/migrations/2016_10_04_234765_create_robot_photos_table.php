<?php
/*
- Created by: Grant Chesney
- Date: October 2'nd 2016
- Version: 1.0
*/
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateRobotPhotosTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('robot_photos', function (Blueprint $table) {

            $table->increments('id');

            $table->string('team_id');

            $table->string('scout_id');

            $table->string('path');

            $table->softDeletes();

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('robot_photos');
    }
}
