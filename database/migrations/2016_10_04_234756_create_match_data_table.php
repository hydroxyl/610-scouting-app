<?php
/*
- Created by: Grant Chesney
- Date: October 2'nd 2016
- Version: 1.0
*/
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateMatchdataTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('match_data', function (Blueprint $table) {

            $table->increments('id');

            $table->string('scout_id');

            $table->string('event_id');

            $table->string('team_id');

            $table->json('data');

            $table->string('layout');

            $table->softDeletes();

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('match_data');
    }
}
